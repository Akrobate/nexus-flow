'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    UserRepository,
    OrganizationRepository,
} = require('../../src/repositories');

const {
    USER_1, USER_1_PASSWORD,
} = require('../seeds/user_seed');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);
const user_repository = UserRepository.getInstance();
const organization_repository = OrganizationRepository.getInstance();

describe('User functional', () => {

    beforeEach(async () => {
        await user_repository.removeAll();
        await organization_repository.removeAll();
        await user_repository.create(USER_1);
    });


    describe('Login', () => {
        it('Login from seed - success', async () => {
            await superApp
                .post('/api/v1/login')
                .send({
                    login: USER_1.login,
                    password: USER_1_PASSWORD,
                })
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body.token).to.be.a('String');
                });
        });


        it('Login from seed - bad password', async () => {
            await superApp
                .post('/api/v1/login')
                .send({
                    login: USER_1.login,
                    password: 'Bad password',
                })
                .expect(HTTP_CODE.UNAUTHORIZED);
        });


        it('Login from seed - bad Login', async () => {
            await superApp
                .post('/api/v1/login')
                .send({
                    login: 'badlogin@login.fr',
                    password: USER_1_PASSWORD,
                })
                .expect(HTTP_CODE.UNAUTHORIZED);
        });
    });

    describe('Register', () => {
        it('Register and Login', async () => {
            const input = {
                email: 'test.test@test.js',
                login: 'test.test@test.js',
                password: 'Mypassword',
                organization_name: 'Organization name',
            };

            await superApp
                .post('/api/v1/register')
                .send(input)
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body.email).to.equal(input.email);
                });

            await superApp
                .post('/api/v1/login')
                .send({
                    login: input.login,
                    password: input.password,
                })
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body.token).to.be.a('String');
                });
        });


        it('Register without organization name', async () => {
            const input = {
                email: 'test.test@test.js',
                login: 'test.test@test.js',
                password: 'Mypassword',
                organization_name: 'Organization name',
            };

            await superApp
                .post('/api/v1/register')
                .send({
                    email: input.email,
                    login: input.login,
                    password: input.password,
                })
                .expect(HTTP_CODE.BAD_REQUEST);
        });


        it('Register should fail with existing email', async () => {
            const input = {
                email: USER_1.email,
                login: 'test.test@test.js',
                password: 'Mypassword',
                organization_name: 'Organization name',
            };

            await superApp
                .post('/api/v1/register')
                .send(input)
                .expect(HTTP_CODE.BAD_REQUEST);
        });
    });

});

