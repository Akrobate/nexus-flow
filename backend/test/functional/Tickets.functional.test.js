'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    USER_1,
    USER_2,
    SPACE_1,
    SPACE_1_TICKET_STATUS_1,
    SPACE_1_TICKET_STATUS_2,
    SPACE_1_MILESTONE_1,
} = require('../seeds');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);

let token_USER_1 = null;
let token_USER_2 = null;

describe('Ticket functional', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        token_USER_1 = DataLoader.generateToken(USER_1);
        token_USER_2 = DataLoader.generateToken(USER_2);
    });

    it('Ticket create', async () => {
        const input = {
            summary: 'Summary test payload',
            description: 'description test payload',
            priority: 3,
            milestone_id: SPACE_1_MILESTONE_1.id,
            space_id: SPACE_1.id,
            status_id: SPACE_1_TICKET_STATUS_1.id,
            estimate: 0,
        };

        let created_id = null;

        await superApp
            .post('/api/v1/tickets')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                created_id = response.body.id;
            });

        await superApp
            .get(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.deep.equal({
                    ...input,
                    id: created_id,
                });
            });
    });


    it('Ticket create with invalid input', async () => {
        // Missing description
        const input = {
            summary: 'Summary test payload',
            priority: 3,
            milestone_id: SPACE_1_MILESTONE_1.id,
            space_id: SPACE_1.id,
            status_id: SPACE_1_TICKET_STATUS_1.id,
            estimate: 0,
        };
        await superApp
            .post('/api/v1/tickets')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.BAD_REQUEST);
    });

    it('Ticket create with invalid input (space_id missing)', async () => {
        // Missing space_id
        const input = {
            summary: 'Summary test payload',
            description: 'description test payload',
            priority: 3,
            milestone_id: SPACE_1_MILESTONE_1.id,
            status_id: SPACE_1_TICKET_STATUS_1.id,
            estimate: 0,
        };
        await superApp
            .post('/api/v1/tickets')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.BAD_REQUEST);
    });


    it('Ticket update', async () => {
        const input = {
            summary: 'Summary test payload',
            description: 'description test payload',
            priority: 3,
            milestone_id: SPACE_1_MILESTONE_1.id,
            space_id: SPACE_1.id,
            status_id: SPACE_1_TICKET_STATUS_1.id,
            estimate: 0,
        };

        let created_id = null;

        await superApp
            .post('/api/v1/tickets')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                created_id = response.body.id;
            });

        await superApp
            .get(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.description).to.equal(input.description);
            });

        await superApp
            .patch(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send({
                description: 'UPDATED Description',
                status_id: SPACE_1_TICKET_STATUS_2.id,
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.description).to.equal('UPDATED Description');
            });

        await superApp
            .get(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.description).to.equal('UPDATED Description');
            });
    });


    it('Ticket delete', async () => {
        const input = {
            summary: 'Summary test payload',
            description: 'description test payload',
            priority: 3,
            milestone_id: SPACE_1_MILESTONE_1.id,
            space_id: SPACE_1.id,
            status_id: SPACE_1_TICKET_STATUS_1.id,
            estimate: 0,
        };

        let created_id = null;

        await superApp
            .post('/api/v1/tickets')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                created_id = response.body.id;
            });

        await superApp
            .get(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK);

        await superApp
            .delete(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK);

        await superApp
            .get(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.NOT_FOUND);

    });


    it('User2 (space2) should not be able to update User1 (space1) ticket', async () => {
        const input = {
            summary: 'Summary test payload',
            description: 'description test payload',
            priority: 3,
            milestone_id: SPACE_1_MILESTONE_1.id,
            space_id: SPACE_1.id,
            status_id: SPACE_1_TICKET_STATUS_1.id,
            estimate: 0,
        };

        let created_id = null;

        await superApp
            .post('/api/v1/tickets')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                created_id = response.body.id;
            });

        await superApp
            .patch(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_2}`)
            .send({
                description: 'UPDATED Description',
            })
            .expect(HTTP_CODE.FORBIDDEN);

    });


    it('User2 (space2) should be able to read User1 (space1) ticket', async () => {
        const input = {
            summary: 'Summary test payload',
            description: 'description test payload',
            priority: 3,
            milestone_id: SPACE_1_MILESTONE_1.id,
            space_id: SPACE_1.id,
            status_id: SPACE_1_TICKET_STATUS_1.id,
            estimate: 0,
        };

        let created_id = null;

        await superApp
            .post('/api/v1/tickets')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                created_id = response.body.id;
            });

        await superApp
            .get(`/api/v1/tickets/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_2}`)
            .expect(HTTP_CODE.OK);
    });

    describe('Ticket search', () => {
        it('Search', async () => {
            const input = {
                summary: 'Summary test payload search',
                description: 'description test payload',
                priority: 3,
                milestone_id: SPACE_1_MILESTONE_1.id,
                space_id: SPACE_1.id,
                status_id: SPACE_1_TICKET_STATUS_1.id,
                estimate: 0,
            };

            let created_id = null;

            await superApp
                .post('/api/v1/tickets')
                .set('Authorization', `Bearer ${token_USER_1}`)
                .send(input)
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    created_id = response.body.id;
                });

            await superApp
                .get('/api/v1/tickets')
                .set('Authorization', `Bearer ${token_USER_2}`)
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response.body).to.be.an('object');
                    expect(response.body).to.have.property('ticket_list');

                    const {
                        ticket_list,
                    } = response.body;

                    const result = ticket_list.find((ticket) => ticket.id === created_id);
                    expect(result).to.be.an('object');
                });

        });
    });

});


