'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    USER_1,
} = require('../seeds');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);

let token_USER_1 = null;

describe('SpaceAccessProfile functional', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        token_USER_1 = DataLoader.generateToken(USER_1);
    });

    it('SpaceAccessProfile search', async () => {
        await superApp
            .get('/api/v1/space-access-profiles')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.be.an('Object');
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('Object');
                expect(response.body).to.have.property('space_access_profile_list');
            });
    });


});

