'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const qs = require('qs');

const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    USER_1,
    USER_2,
    SPACE_1,
    SPACE_2,

    SPACE_ACCESS_PROFILE_1,
    SPACE_ACCESS_PROFILE_2,
} = require('../seeds');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);

let token_USER_1 = null;
let token_USER_2 = null;

describe('SpaceUserAccess functional', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        token_USER_1 = DataLoader.generateToken(USER_1);
        token_USER_2 = DataLoader.generateToken(USER_2);
    });

    it('User_2 should have access to Space_1', async () => {
        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_2}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                const team_mate_user_1 = response.body.teammate_list
                    .find((item) => item.id === USER_1.id);
                expect(team_mate_user_1.user_access.access_profile_id)
                    .to.equal(SPACE_ACCESS_PROFILE_1.id);
            });
    });


    it('User_1 should not have access to Space_2', async () => {
        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_2.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                const team_mate_user_1 = response.body.teammate_list
                    .find((item) => item.id === USER_1.id);

                expect(team_mate_user_1.user_access)
                    .to.equal(null);
            });
    });


    it('User_1 should be able to update User_2 access in space 1', async () => {

        let teammate_user_2 = null;
        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                teammate_user_2 = response.body.teammate_list
                    .find((item) => item.id === USER_2.id);
                expect(teammate_user_2.user_access.access_profile_id)
                    .to.equal(SPACE_ACCESS_PROFILE_2.id);
            });

        await superApp
            .patch(`/api/v1/space-user-accesses/${teammate_user_2.user_access.id}`)
            .send({
                access_profile_id: SPACE_ACCESS_PROFILE_1.id,
            })
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.access_profile_id).to.equal(SPACE_ACCESS_PROFILE_1.id);
            });

        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                teammate_user_2 = response.body.teammate_list
                    .find((item) => item.id === USER_2.id);
                expect(teammate_user_2.user_access.access_profile_id)
                    .to.equal(SPACE_ACCESS_PROFILE_1.id);
            });

    });


    it('User_1 should be able to delete User_2 access in space 1', async () => {

        let teammate_user_2 = null;
        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                teammate_user_2 = response.body.teammate_list
                    .find((item) => item.id === USER_2.id);
                expect(teammate_user_2.user_access.access_profile_id)
                    .to.equal(SPACE_ACCESS_PROFILE_2.id);
            });

        await superApp
            .delete(`/api/v1/space-user-accesses/${teammate_user_2.user_access.id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK);

        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                teammate_user_2 = response.body.teammate_list
                    .find((item) => item.id === USER_2.id);
                expect(teammate_user_2.user_access)
                    .to.equal(null);
            });

    });


    it('User_1 should be able to delete User_2 access and then create in space 1', async () => {

        let teammate_user_2 = null;
        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                teammate_user_2 = response.body.teammate_list
                    .find((item) => item.id === USER_2.id);
                expect(teammate_user_2.user_access.access_profile_id)
                    .to.equal(SPACE_ACCESS_PROFILE_2.id);
            });


        await superApp
            .delete(`/api/v1/space-user-accesses/${teammate_user_2.user_access.id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK);


        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                teammate_user_2 = response.body.teammate_list
                    .find((item) => item.id === USER_2.id);
                expect(teammate_user_2.user_access)
                    .to.equal(null);
            });


        await superApp
            .post('/api/v1/space-user-accesses')
            .send({
                user_id: USER_2.id,
                space_id: SPACE_1.id,
                override_access_list: [],
                access_profile_id: SPACE_ACCESS_PROFILE_1.id,
            })
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK);


        await superApp
            .get('/api/v1/teammates')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                teammate_user_2 = response.body.teammate_list
                    .find((item) => item.id === USER_2.id);
                expect(teammate_user_2.user_access.access_profile_id)
                    .to.equal(SPACE_ACCESS_PROFILE_1.id);
            });
    });

});
