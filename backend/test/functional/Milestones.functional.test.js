'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const qs = require('qs');

const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    USER_1,
    SPACE_1,
} = require('../seeds');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);

let token_USER_1 = null;

describe('Milestones functional', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        token_USER_1 = DataLoader.generateToken(USER_1);
    });

    it('Milestones search without params should fail', async () => {
        await superApp
            .get('/api/v1/milestones')

            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.BAD_REQUEST);
    });

    it('Milestones search with space_id param', async () => {
        await superApp
            .get('/api/v1/milestones')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {

                expect(response).to.be.an('Object');
                expect(response).to.have.property('body');

                expect(response.body).to.be.an('Object');
                expect(response.body).to.have.property('milestone_list');
                expect(response.body.milestone_list).to.be.an('Array');

            });
    });

});

