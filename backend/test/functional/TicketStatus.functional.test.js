'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const qs = require('qs');
const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    USER_1,
    SPACE_1,
    SPACE_1_TICKET_STATUS_1,
} = require('../seeds');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);

let token_USER_1 = null;

describe('Ticket status functional', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        token_USER_1 = DataLoader.generateToken(USER_1);

    });

    it('Ticket status search', async () => {
        await superApp
            .get('/api/v1/ticket-statuses')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                const {
                    ticket_status_list,
                } = response.body;
                expect(ticket_status_list.length).to.equal(3);
            });
    });

    it('Ticket status create', async () => {
        const input = {
            name: 'Created ticket status by functional test',
            list_order: 13,
            space_id: SPACE_1.id,
            state: 1,
        };

        let created_id = null;

        await superApp
            .post('/api/v1/ticket-statuses')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send(input)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                created_id = response.body.id;
            });

        await superApp
            .get(`/api/v1/ticket-statuses/${created_id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect(({
                body,
            }) => {
                expect(body).to.deep.equal({
                    ...input,
                    id: created_id,
                });
            });
    });

    it('Ticket status update', async () => {

        await superApp
            .patch(`/api/v1/ticket-statuses/${SPACE_1_TICKET_STATUS_1.id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send({
                name: 'To do updated by functional test',
            })
            .expect(HTTP_CODE.OK);

        await superApp
            .get(`/api/v1/ticket-statuses/${SPACE_1_TICKET_STATUS_1.id}`)
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect(({
                body,
            }) => {
                expect(body).to.be.an('Object');
                const {
                    name,
                    id,
                } = body;

                expect(name).to.equal('To do updated by functional test');
                expect(id).to.equal(SPACE_1_TICKET_STATUS_1.id);
            });
    });


    it('Ticket status update - should fail with unexisting id', async () => {

        await superApp
            .patch('/api/v1/ticket-statuses/992745a0000b1ced2cd1b277')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .send({
                name: 'To do updated by functional test',
            })
            .expect(HTTP_CODE.NOT_FOUND);

    });
});

