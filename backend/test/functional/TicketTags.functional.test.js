'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const qs = require('qs');
const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    USER_1,
    SPACE_1,
} = require('../seeds');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);

let token_USER_1 = null;

describe('Ticket tags functional', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        token_USER_1 = DataLoader.generateToken(USER_1);

    });

    it('Ticket tags search', async () => {
        await superApp
            .get('/api/v1/ticket-tags')
            .query(qs.stringify({
                space_id: SPACE_1.id,
            }))
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body).to.be.an('object');
                expect(response.body).to.have.property('ticket_tag_list');
                const {
                    ticket_tag_list,
                } = response.body;
                expect(ticket_tag_list).to.be.an('array');
            });
    });
});

