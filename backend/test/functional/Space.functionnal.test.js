'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    USER_1,
    USER_2,
    SPACE_1,
    SPACE_1_TICKET_STATUS_1,
    SPACE_1_TICKET_STATUS_2,
    SPACE_1_MILESTONE_1,
} = require('../seeds');

const {
    server,
} = require('../../src/server');

const superApp = superTest(server);

let token_USER_1 = null;

describe('Space functional', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        token_USER_1 = DataLoader.generateToken(USER_1);
    });

    it('Space search', async () => {

        await superApp
            .get('/api/v1/spaces')
            .set('Authorization', `Bearer ${token_USER_1}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.be.an('object');

                expect(response.body).to.have.property('space_list');
                expect(response.body.space_list).to.be.an('array');

                const [
                    first_element,
                ] = response.body.space_list;

                expect(first_element).to.have.property('id');
                expect(first_element).to.have.property('organization_id');
                expect(first_element).to.have.property('name');
                expect(first_element).to.have.property('prefix');
                expect(first_element).to.have.property('created_at');
                expect(first_element).to.have.property('created_by_user_id');
                expect(first_element).to.have.property('space_tag');
                expect(first_element).to.have.property('description');
                expect(first_element).to.have.property('updated_at');
                expect(first_element).to.have.property('updated_by_user_id');
            });
    });

});

