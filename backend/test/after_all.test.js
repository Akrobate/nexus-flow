'use strict';

const {
    MongoDbRepository,
} = require('../src/repositories/MongoDbRepository');

after((done) => {
    const mongo_db_repository = MongoDbRepository.getInstance();

    setTimeout(
        () => {
            mongo_db_repository.closeConnection()
                .then(() => {
                    done();
                });
        },
        1000
    );

});
