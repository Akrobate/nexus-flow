'use strict';

const repository_class = require('../../src/repositories');
const service_class = require('../../src/services');

const {
    USER_1,
    SPACE_1,
    SPACE_USER_1_ACCESS_1,
    SPACE_ACCESS_PROFILE_1,
    SPACE_ACCESS_PROFILE_2,
    USER_2,
    SPACE_2,
    SPACE_USER_2_ACCESS_1,
    SPACE_USER_2_ACCESS_2,

    SPACE_1_TICKET_STATUS_1,
    SPACE_1_TICKET_STATUS_2,
    SPACE_1_TICKET_STATUS_3,

    SPACE_2_TICKET_STATUS_1,
    SPACE_2_TICKET_STATUS_2,
    SPACE_2_TICKET_STATUS_3,

    SPACE_1_MILESTONE_1,
    TICKET_1,
    TICKET_2,
    TICKET_3,

    TICKET_TAG_1,
    TICKET_TAG_2,
} = require('../seeds');

const repositories = {
    user_repository: repository_class.UserRepository.getInstance(),
    organization_repository: repository_class.OrganizationRepository.getInstance(),
    space_repository: repository_class.SpaceRepository.getInstance(),
    space_user_access_repository: repository_class.SpaceUserAccessRepository.getInstance(),
    space_access_profile_repository: repository_class.SpaceAccessProfileRepository.getInstance(),
    ticket_status_repository: repository_class.TicketStatusRepository.getInstance(),
    ticket_repository: repository_class.TicketRepository.getInstance(),
    ticket_tag_repository: repository_class.TicketTagRepository.getInstance(),
    milestone_repository: repository_class.MilestoneRepository.getInstance(),
};

class DataLoader {

    /**
     * @returns {Promise}
     */
    static async removeAll() {
        for (const key of Object.keys(repositories)) {
            await repositories[key].removeAll();
        }
    }


    /**
     * @param {*} repository
     * @param {*} seed
     * @return {Promise}
     */
    static async createSeed(repository, seed) {
        await repositories[repository].create(seed);
    }

    /**
     * @returns {Promise}
     */
    static async createAllSeeds() {

        await DataLoader.createSeed('space_access_profile_repository', SPACE_ACCESS_PROFILE_1);
        await DataLoader.createSeed('space_access_profile_repository', SPACE_ACCESS_PROFILE_2);

        await DataLoader.createSeed('ticket_status_repository', SPACE_1_TICKET_STATUS_1);
        await DataLoader.createSeed('ticket_status_repository', SPACE_1_TICKET_STATUS_2);
        await DataLoader.createSeed('ticket_status_repository', SPACE_1_TICKET_STATUS_3);

        await DataLoader.createSeed('ticket_status_repository', SPACE_2_TICKET_STATUS_1);
        await DataLoader.createSeed('ticket_status_repository', SPACE_2_TICKET_STATUS_2);
        await DataLoader.createSeed('ticket_status_repository', SPACE_2_TICKET_STATUS_3);

        await DataLoader.createSeed('user_repository', USER_1);
        await DataLoader.createSeed('space_repository', SPACE_1);
        await DataLoader.createSeed('space_user_access_repository', SPACE_USER_1_ACCESS_1);

        await DataLoader.createSeed('milestone_repository', SPACE_1_MILESTONE_1);

        await DataLoader.createSeed('user_repository', USER_2);
        await DataLoader.createSeed('space_repository', SPACE_2);
        await DataLoader.createSeed('space_user_access_repository', SPACE_USER_2_ACCESS_1);
        await DataLoader.createSeed('space_user_access_repository', SPACE_USER_2_ACCESS_2);

        await DataLoader.createSeed('ticket_repository', TICKET_1);
        await DataLoader.createSeed('ticket_repository', TICKET_2);
        await DataLoader.createSeed('ticket_repository', TICKET_3);

        await DataLoader.createSeed('ticket_tag_repository', TICKET_TAG_1);
        await DataLoader.createSeed('ticket_tag_repository', TICKET_TAG_2);
        
    }


    /**
     * @param {Object} user
     * @returns {String}
     */
    static generateToken(user) {
        const {
            id: user_id,
            login,
        } = user;

        const user_service = service_class.UserService.getInstance();

        return user_service.tryToSignJwt({
            user_id,
            login,
        });
    }

}


module.exports = {
    DataLoader,
};

