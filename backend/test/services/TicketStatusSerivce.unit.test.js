/* eslint-disable no-multi-str */

'use strict';

const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    TicketStatusService,
} = require('../../src/services');
const {
    TicketRepository,
    TicketStatusRepository,
} = require('../../src/repositories');

const {
    mock,
    stub,
} = require('sinon');


describe('TicketStatusService.unit.test', () => {


    const ticket_status_serivce = TicketStatusService.getInstance();
    const ticket_repository = TicketRepository.getInstance();
    const ticket_status_repository = TicketStatusRepository.getInstance();

    const ticket_status_list = [
        {
            id: '66d78b892b0e8babeff54059',
            name: 'New',
            list_order: 0,
            state: 1,
            space_id: '664e1a311ba7a603e6126685',
        },
        {
            id: '66d78b892b0e8babeff5405a',
            name: 'To do',
            list_order: 1,
            state: 1,
            space_id: '664e1a311ba7a603e6126685',

        },
        {
            id: '66d78b892b0e8babeff5405b',
            name: 'In Progress',
            list_order: 2,
            state: 1,
            space_id: '664e1a311ba7a603e6126685',
        },
        {
            id: '66d78b892b0e8babeff5405c',
            name: 'Technical Review (PR)',
            list_order: 3,
            state: 1,
            space_id: '664e1a311ba7a603e6126685',
        },
        {
            id: '66d78b892b0e8babeff5405d',
            name: 'Validated (PR)',
            list_order: 4,
            state: 1,
            space_id: '664e1a311ba7a603e6126685',
        },
        {
            id: '66d78b892b0e8babeff5405e',
            name: 'To test Devs (RC)',
            list_order: 5,
            state: 1,
            space_id: '664e1a311ba7a603e6126685',
        },
        {
            id: '66d78b892b0e8babeff5405f',
            name: 'To test MOA',
            list_order: 6,
            state: 0,
            space_id: '664e1a311ba7a603e6126685',
        },
        {
            id: '66d78b892b0e8babeff54060',
            name: 'Done',
            list_order: 7,
            state: 0,
            space_id: '664e1a311ba7a603e6126685',

        },
        {
            id: '66d78b892b0e8babeff54061',
            name: 'Deployed',
            list_order: 8,
            state: 0,
            space_id: '664e1a311ba7a603e6126685',
        },
    ];


    const ticket = {
        id: '664e1a311ba7a603e6126685',
        space_id: '664e1a311ba7a603e6126685',
        comment_list: [
            {
                id: 1707922737,
                comment: '',
                ticket_id: 225768651,
                user_id: 'cDO_tCQNir55qGacwqEsg8',
                created_on: '2023-08-29T12:38:03.000Z',
                updated_at: '2023-08-29T12:38:03.000Z',
                ticket_changes: '---\n- - status\n  - Done\n  - Deployed\n',
                user_name: 'Arti',
                user_avatar_url: '',
            },
            {
                id: 1621856021,
                comment: null,
                ticket_id: 225768651,
                user_id: 'a7AieCPBGr5ONddmr6CpXy',
                created_on: '2019-07-04T15:48:46.000Z',
                updated_at: '2019-07-04T15:48:46.000Z',
                ticket_changes: '---\n- - status\n  - Merged (To test)\n  - Done\n',
                user_name: 'Marianne',
                user_avatar_url: '',
            },
            {
                id: 1621855991,
                comment: 'OK\nhttp://preprod-src.nomination.fr/organization/5140/historic',
                ticket_id: 225768651,
                user_id: 'a7AieCPBGr5ONddmr6CpXy',
                created_on: '2019-07-04T15:48:40.000Z',
                updated_at: '2019-07-04T15:48:40.000Z',
                ticket_changes: '--- []\n',
                user_name: 'Marianne',
                user_avatar_url: '',
            },
            {
                id: 1621310051,
                comment: null,
                ticket_id: 225768651,
                user_id: 'dgnMbwqJ4r6j9facwqEsg8',
                created_on: '2019-07-03T15:42:14.000Z',
                updated_at: '2019-07-03T15:42:14.000Z',
                ticket_changes: '---\n- - status\n  - Technical Done\n  - Merged (To test)\n',
                user_name: 'Benjamin',
                user_avatar_url: '',
            },
            {
                id: 1621281451,
                comment: null,
                ticket_id: 225768651,
                user_id: 'dgnMbwqJ4r6j9facwqEsg8',
                created_on: '2019-07-03T15:08:11.000Z',
                updated_at: '2019-07-03T15:08:11.000Z',
                ticket_changes: '---\n- - status\n  - Technical Review (PR)\n  - Technical Done\n',
                user_name: 'Benjamin',
                user_avatar_url: '',
            },
            {
                id: 1620540711,
                comment: null,
                ticket_id: 225768651,
                user_id: 'dgnMbwqJ4r6j9facwqEsg8',
                created_on: '2019-07-02T10:43:06.000Z',
                updated_at: '2019-07-02T10:43:06.000Z',
                ticket_changes: '---\n- - Estimate\n  - \'0.5\'\n  - \'0.25\'\n- - Sum of Child Estimates\n  - \'0.5\'\n  - \'0.25\'\n',
                user_name: 'Benjamin',
                user_avatar_url: '',
            },
            {
                id: 1619978841,
                comment: null,
                ticket_id: 225768651,
                user_id: 'dgnMbwqJ4r6j9facwqEsg8',
                created_on: '2019-07-01T10:14:13.000Z',
                updated_at: '2019-07-01T10:14:13.000Z',
                ticket_changes: '---\n- - status\n  - In Progress\n  - Technical Review (PR)\n',
                user_name: 'Benjamin',
                user_avatar_url: '',
            },
            {
                id: 1619973201,
                comment: '@username on veut tout ce qui y est déjà et qui ne concerne pas les users\nen plus de ce que tu dis il y a aussi les ajouts  et retraits de crédits notamment',
                ticket_id: 225768651,
                user_id: 'a7AieCPBGr5ONddmr6CpXy',
                created_on: '2019-07-01T10:05:05.000Z',
                updated_at: '2019-07-01T10:05:05.000Z',
                ticket_changes: '--- []\n',
                user_name: 'Marianne',
                user_avatar_url: '',
            },
            {
                id: 1619972001,
                comment: '@mrault Ce ne sont pas des doublons, c\'est juste que les commentaires des users s\'affichent dans l\'orga.\nDans l\'orga, vous voulez quoi dans l\'historique ? Les infos des exports et les commentaires qui ne sont que sur l\'orga ?',
                ticket_id: 225768651,
                user_id: 'dgnMbwqJ4r6j9facwqEsg8',
                created_on: '2019-07-01T10:03:19.000Z',
                updated_at: '2019-07-01T10:03:19.000Z',
                ticket_changes: '--- []\n',
                user_name: 'Benjamin',
                user_avatar_url: '',
            },
            {
                id: 1619968021,
                comment: null,
                ticket_id: 225768651,
                user_id: 'dgnMbwqJ4r6j9facwqEsg8',
                created_on: '2019-07-01T09:56:55.000Z',
                updated_at: '2019-07-01T09:56:55.000Z',
                ticket_changes: '---\n- - status\n  - To do\n  - In Progress\n',
                user_name: 'Benjamin',
                user_avatar_url: '',
            },
            {
                id: 1619964611,
                comment: '@username ce sera possible de virer ce qui a été "doublonné"?',
                ticket_id: 225768651,
                user_id: 'a7AieCPBGr5ONddmr6CpXy',
                created_on: '2019-07-01T09:49:47.000Z',
                updated_at: '2019-07-01T09:49:47.000Z',
                ticket_changes: '--- []\n',
                user_name: 'Marianne',
                user_avatar_url: '',
            },
            {
                id: 1619943691,
                comment: '@mrault 0.5 pour investigation de @username ',
                ticket_id: 225768651,
                user_id: 'aybFP0PBGr5Oykdmr6CpXy',
                created_on: '2019-07-01T09:09:13.000Z',
                updated_at: '2019-07-01T09:09:13.000Z',
                ticket_changes: '--- []\n',
                user_name: 'Mathieu',
                user_avatar_url: '',
            },
            {
                id: 1619943531,
                comment: null,
                ticket_id: 225768651,
                user_id: 'aybFP0PBGr5Oykdmr6CpXy',
                created_on: '2019-07-01T09:08:55.000Z',
                updated_at: '2019-07-01T09:08:55.000Z',
                ticket_changes: '---\n- - Estimate\n  - \'0.0\'\n  - \'0.5\'\n- - Sum of Child Estimates\n  - \'0.0\'\n  - \'0.5\'\n',
                user_name: 'Mathieu',
                user_avatar_url: '',
            },
            {
                id: 1619943481,
                comment: null,
                ticket_id: 225768651,
                user_id: 'aybFP0PBGr5Oykdmr6CpXy',
                created_on: '2019-07-01T09:08:52.000Z',
                updated_at: '2019-07-01T09:08:52.000Z',
                ticket_changes: '---\n- - status\n  - New\n  - To do\n',
                user_name: 'Mathieu',
                user_avatar_url: '',
            },
            {
                id: 1619943451,
                comment: null,
                ticket_id: 225768651,
                user_id: 'aybFP0PBGr5Oykdmr6CpXy',
                created_on: '2019-07-01T09:08:50.000Z',
                updated_at: '2019-07-01T09:08:50.000Z',
                ticket_changes: '---\n- - assigned_to_id\n  - \n  - username\n',
                user_name: 'Mathieu',
                user_avatar_url: '',
            },
            {
                id: 1619923651,
                comment: null,
                ticket_id: 225768651,
                user_id: 'a7AieCPBGr5ONddmr6CpXy',
                created_on: '2019-07-01T08:30:05.000Z',
                updated_at: '2019-07-01T08:30:05.000Z',
                ticket_changes: '---\n- - description\n  - \'\'\n  - https://polarity.assembla.c...\n',
                user_name: 'Marianne',
                user_avatar_url: '',
            },
            {
                id: 1619923481,
                comment: '',
                ticket_id: 225768651,
                user_id: 'a7AieCPBGr5ONddmr6CpXy',
                created_on: '2019-07-01T08:29:46.000Z',
                updated_at: '2019-07-01T08:29:46.000Z',
                ticket_changes: '--- []\n',
                user_name: 'Marianne',
                user_avatar_url: '',
            },
        ],
    };

    const mocks = {};

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();

        mocks.ticket_repository = mock(ticket_repository);
        mocks.ticket_status_repository = mock(ticket_status_repository);
    });


    afterEach(() => {
        mocks.ticket_repository.restore();
        mocks.ticket_status_repository.restore();

    });

    it('updateFlatDatesStatuses without comment list', async () => {

        mocks.ticket_repository
            .expects('search')
            .once()
            .resolves([{
                id: '664e1a311ba7a603e6126685',
                space_id: '664e1a311ba7a603e6126685',
            }]);

        mocks.ticket_status_repository
            .expects('search')
            .once()
            .resolves(ticket_status_list);

        const ticket_repository_update_stub = stub(ticket_repository, 'update');
        ticket_repository_update_stub.callsFake((id_param, payload_param) => {
            expect(id_param).to.be.equal(ticket.id);
            expect(payload_param).to.be.an('object');
            return Promise.resolve();
        });

        const DEBUG = false;
        await ticket_status_serivce.updateFlatDatesStatuses(DEBUG);

        ticket_repository_update_stub.restore();
        mocks.ticket_repository.verify();

    });

    it('updateFlatDatesStatuses', async () => {

        mocks.ticket_repository
            .expects('search')
            .once()
            .resolves([ticket]);

        mocks.ticket_status_repository
            .expects('search')
            .once()
            .resolves(ticket_status_list);

        const ticket_repository_update_stub = stub(ticket_repository, 'update');
        ticket_repository_update_stub.callsFake((id_param, payload_param) => {
            expect(id_param).to.be.equal(ticket.id);
            expect(payload_param).to.have.property('status_dates');
            expect(payload_param.status_dates).to.have.property('Deployed');
            expect(payload_param.status_dates).to.have.property('Done');
            expect(payload_param.status_dates).to.have.property('Technical Review (PR)');
            expect(payload_param.status_dates).to.have.property('In Progress');
            expect(payload_param.status_dates).to.have.property('To do');

            return Promise.resolve();
        });

        const DEBUG = false;
        await ticket_status_serivce.updateFlatDatesStatuses(DEBUG);

        ticket_repository_update_stub.restore();
        mocks.ticket_repository.verify();

    });


    it('updateFlatDatesStatuses with comment list without status', async () => {

        mocks.ticket_repository
            .expects('search')
            .once()
            .resolves([{
                id: '664e1a311ba7a603e6126685',
                space_id: '664e1a311ba7a603e6126685',
                comment_list: [
                    {
                        id: 1707922737,
                        comment: '',
                        ticket_id: 225768651,
                        user_id: 'cDO_tCQNir55qGacwqEsg8',
                        created_on: '2023-08-29T12:38:03.000Z',
                        updated_at: '2023-08-29T12:38:03.000Z',
                        ticket_changes: '---\n- - status\n  - fsdfsdf\n  - sdfsdfsdf\n',
                        user_name: 'Arti',
                        user_avatar_url: '',
                    },
                ],
            }]);

        mocks.ticket_status_repository
            .expects('search')
            .once()
            .resolves(ticket_status_list);

        const ticket_repository_update_stub = stub(ticket_repository, 'update');
        ticket_repository_update_stub.callsFake((id_param, payload_param) => {
            expect(id_param).to.be.equal(ticket.id);
            expect(payload_param).to.have.property('status_dates');
            expect(payload_param.status_dates).to.have.property('Deployed');
            expect(payload_param.status_dates).to.have.property('Done');
            expect(payload_param.status_dates).to.have.property('Technical Review (PR)');
            expect(payload_param.status_dates).to.have.property('In Progress');
            expect(payload_param.status_dates).to.have.property('To do');

            return Promise.resolve();
        });

        const DEBUG = true;
        await ticket_status_serivce.updateFlatDatesStatuses(DEBUG);

        ticket_repository_update_stub.restore();
        mocks.ticket_repository.verify();

    });

});
