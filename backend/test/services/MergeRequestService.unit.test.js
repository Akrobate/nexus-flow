'use strict';

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');

const {
    MergeRequestService,
} = require('../../src/services');
const {
    MergeRequestRepository,
} = require('../../src/repositories');


describe('MergeRequestService.unit.test', () => {

    const merge_request_service = MergeRequestService.getInstance();
    const merge_request_repository = MergeRequestRepository.getInstance();
    const mocks = {};

    beforeEach(() => {
        mocks.merge_request_repository = mock(merge_request_repository);
    });

    afterEach(() => {
        mocks.merge_request_repository.restore();
    });

    it('mergeRequestStatistics', async () => {
        const result = await merge_request_service.mergeRequestStatistics(null, {});
        expect(result).to.be.an('Array');
    });


    it('mergeRequestStatistics', async () => {
        const user = {};
        const search_criteria = {
            SEARCH_CRITERIA: 'SEARCH_CRITERIA',
        };

        mocks.merge_request_repository
            .expects('search')
            .once()
            .withArgs(search_criteria)
            .resolves(
                [
                    {
                        id: 1,
                        repository: 'repository_a',
                        state: 'opened',
                    },
                    {
                        id: 2,
                        repository: 'repository_a',
                        state: 'opened',
                    },
                    {
                        id: 3,
                        repository: 'repository_b',
                        state: 'merged',
                    },
                    {
                        id: 4,
                        repository: 'repository_b',
                        state: 'closed',
                    },
                    {
                        id: 5,
                        repository: 'repository_b',
                        state: 'closed',
                    },
                ]
            );

        const response = await merge_request_service.mergeRequestStatistics(user, search_criteria);
        mocks.merge_request_repository.verify();

        expect(response).to.deep.equal([
            {
                repository: 'repository_a',
                open: 2,
                closed: 0,
                merged: 0,
                finished: 0,
            },
            {
                repository: 'repository_b',
                open: 0,
                closed: 2,
                merged: 1,
                finished: 3,
            },
        ]);
    });


    describe('detectMergeRequestInTickets', () => {
        it('detectMergeRequestInTickets', async () => {
            const result = await merge_request_service.detectMergeRequestInTickets();
            expect(result).to.be.an('object');
            expect(result).to.deep.equal({
                total_merge_requests_detected: 1,
            });
        });
    });

});

