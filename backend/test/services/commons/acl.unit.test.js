'use strict';


const {
    expect,
} = require('chai');
const {
    DataLoader,
} = require('../../helpers/DataLoader');
const {
    USER_1,
    SPACE_1,
} = require('../../seeds');
const {
    Acl,
} = require('../../../src/services/commons/Acl');


describe('Acl', () => {

    const acl = Acl.getInstance();

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
    });

    it('USER_1 should be able to search ticket in SPACE_1', async () => {
        const response = await acl.checkUserCan('ticket', 'search', SPACE_1.id, USER_1.id);
        expect(response).to.equal(true);
    });

    it('USER_1 should not able to make_unexisting_verb on ticket in SPACE_1', async () => {
        try {
            await acl.checkUserCan('ticket', 'make_unexisting_verb', SPACE_1.id, USER_1.id);
        } catch (error) {
            expect(error.message).to.equal('User cannot make_unexisting_verb ticket in space_id: 888e30a23930d533ee3389f7');
        }
    });


    it('USER_1 should be able to search ticket in UNEXISTING SPACE ID', async () => {
        const unexisting_id = '57ee434f74bbba0904e9d538';
        try {
            await acl.checkUserCan('ticket', 'search', unexisting_id, USER_1.id);
        } catch (error) {
            expect(error.message).to.equal('User cannot search ticket in space_id: 57ee434f74bbba0904e9d538');
        }
    });
});
