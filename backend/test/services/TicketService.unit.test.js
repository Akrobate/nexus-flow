/* eslint-disable no-multi-str */

'use strict';

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    TicketService,
} = require('../../src/services');
const {
    SpaceRepository,
    TicketRepository,
} = require('../../src/repositories');
const {
    TICKET_1,
} = require('../seeds');


const ticket_service = TicketService.getInstance();
const space_repository = SpaceRepository.getInstance();
const ticket_repository = TicketRepository.getInstance();

describe('TicketService.unit.test', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
    });

    describe('detectTicketsInDescription', () => {

        it('Should return detailled object', (done) => {

            // eslint-disable-next-line no-multi-str
            const seed = 'Impacts nouvelles fonctions clés\n \
            https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=628\n \
            Bug du paramétrage des alertes (taille entreprise) depuis la MEP du 07/12\n \
            ';

            const detected_ticket_list = ticket_service.detectTicketsInDescription(seed);
            expect(detected_ticket_list).to.be.an('array');
            expect(detected_ticket_list.length).to.equal(1);

            const [
                detected_ticket,
            ] = detected_ticket_list;

            expect(detected_ticket).to.deep.equal({
                space_tag: 'conception',
                ticket_number: 628,
                url_string: 'https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=628',
            });

            done();
        });


        it('Should return detailled empty array', () => {
            const detected_ticket_list = ticket_service.detectTicketsInDescription('');
            expect(detected_ticket_list).to.be.an('array');
            expect(detected_ticket_list.length).to.equal(0);
        });


        it('Should find 5 relations in description', (done) => {

            // eslint-disable-next-line no-multi-str
            const seed = 'Impacts nouvelles fonctions clés\n \
            https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=628\n \
            Bug du paramétrage des alertes (taille entreprise) depuis la MEP du 07/12\n \
            https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=627\n \
            Newsletter ticks for service-serach testing \
            https://polarity.assembla.com/spaces/conception/tickets/1886-listes-campagne-supprimer-les-anciennes-listes-pouss%C3%A9es-vers-les-campagnes/details \
            Blabla\n \
            https://polarity.assembla.com/spaces/conception/tickets/1886-*listes-campagne*-supprimer-les-anciennes-listes-pouss%C3%A9es-vers-les-campagnes/details \
            Blabla\n \
            https://polarity.assembla.com/spaces/new-desk/tickets/4101/details  coucou\
            Useless link  \
            https://app.nomination.fr/login?fromState=app.home\
            ';

            const detected_ticket_list = ticket_service.detectTicketsInDescription(seed);
            expect(detected_ticket_list).to.be.an('array');
            expect(detected_ticket_list.length).to.equal(4);
            done();
        });

        it('Should not find not number tickets', (done) => {

            const seed = 'Impacts nouvelles fonctions clés\n \
            https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=alphabetical\n \
            https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=627\n \
            ';

            const detected_ticket_list = ticket_service.detectTicketsInDescription(seed);
            expect(detected_ticket_list).to.be.an('array');
            expect(detected_ticket_list.length).to.equal(1);
            done();
        });


        it('Should not find no tickets', (done) => {

            const seed = 'Impacts nouvelles fonctions clés\n \
            https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall\n \
            ';

            const detected_ticket_list = ticket_service.detectTicketsInDescription(seed);
            expect(detected_ticket_list).to.be.an('array');
            // expect(detected_ticket_list.length).to.equal(1);
            done();
        });


        it('Should not find not number tickets', (done) => {

            const seed = 'Impacts nouvelles fonctions clés\n';

            const detected_ticket_list = ticket_service.detectTicketsInDescription(seed);
            expect(detected_ticket_list).to.be.an('array');
            expect(detected_ticket_list.length).to.equal(0);
            done();
        });

    });

    describe('detectTicketsLinks', () => {
        const mocks = {};

        beforeEach(() => {
            mocks.ticket_repository = mock(ticket_repository);
            mocks.space_repository = mock(space_repository);
            mocks.ticket_service = mock(ticket_service);
        });

        afterEach(() => {
            mocks.ticket_repository.restore();
            mocks.space_repository.restore();
            mocks.ticket_service.restore();
        });

        it('Nominal smoke test', async () => {

            mocks.ticket_repository
                .expects('search')
                .resolves([
                    {
                        description: 'https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=628',
                    },
                ]);

            mocks.ticket_repository
                .expects('update')
                .resolves();

            mocks.space_repository
                .expects('search')
                .resolves([]);

            mocks.ticket_service
                .expects('addLinkPointingBy')
                .resolves();

            await ticket_service.detectTicketsLinks();
            mocks.ticket_repository.verify();
            mocks.space_repository.verify();
        });
    });

    describe('resetAllTicketsLinks', () => {
        const mocks = {};

        beforeEach(() => {
            mocks.ticket_repository = mock(ticket_repository);
        });

        afterEach(() => {
            mocks.ticket_repository.restore();
        });

        it('Nominal smoke test', async () => {

            mocks.ticket_repository
                .expects('search')
                .resolves([
                    {
                        description: 'https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=628',
                    },
                ]);

            mocks.ticket_repository
                .expects('update')
                .once()
                .resolves();


            await ticket_service.resetAllTicketsLinks();
            mocks.ticket_repository.verify();
        });

    });

    it('addLinkPointingBy unexisting ticket', async () => {

        const space_list = await space_repository.search({});
        const ticket_link = {
            space_tag: 'conception',
            ticket_number: 628,
            url_string: 'https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=628',
        };

        try {
            await ticket_service
                .addLinkPointingBy(TICKET_1, ticket_link, space_list);
        } catch (error) {
            expect(error.message).to.equal('ticket_link not found');
        }
    });


    it('addLinkPointingBy ticket', async () => {

        const space_list = await space_repository.search({});
        const ticket_link = {
            space_tag: 'conception',
            ticket_number: 4327,
            url_string: 'https://polarity.assembla.com/spaces/conception/tickets/realtime_cardwall?ticket=4327',
        };

        const response = await ticket_service
            .addLinkPointingBy(TICKET_1, ticket_link, space_list);

        expect(response).to.equal(null);

        let [
            ticket,
        ] = await ticket_repository.search({
            number_list: [4327],
        });

        expect(ticket).to.be.an('object');
        expect(ticket).to.have.property('ticket_link_list');
        expect(ticket.ticket_link_list).to.be.an('array');
        expect(ticket.ticket_link_list).to.have.lengthOf(1);
        expect(ticket.ticket_link_list[0]).to.be.an('object');
        expect(ticket.ticket_link_list[0].ticket_number).to.equal(TICKET_1.number);

        await ticket_service
            .addLinkPointingBy(TICKET_1, ticket_link, space_list);

        ([
            ticket,
        ] = await ticket_repository.search({
            number_list: [4327],
        }));

        expect(ticket).to.be.an('object');
        expect(ticket).to.have.property('ticket_link_list');
        expect(ticket.ticket_link_list).to.be.an('array');
        expect(ticket.ticket_link_list).to.have.lengthOf(1);
        expect(ticket.ticket_link_list[0]).to.be.an('object');
        expect(ticket.ticket_link_list[0].ticket_number).to.equal(TICKET_1.number);
    });


    describe('buildTicketLinkObject', () => {

        it('Should be able to build link object from ticket 1', async () => {
            const ticket = {
                id: '66cda23f8777771c8a7c15f6',
                summary: '*Demande * Commentaire non visible dans le ticket desk',
                description: 'https://gitlab.com/nomination/frontend-portal-client-v2/-/merge_requests/757 (merged)\n\nhttps://polarity.assembla.com/spaces/conception/tickets/3069-*demande-d--39-enqu%C3%AAte*-commentaire-non-visible-dans-le-ticket-desk-/details',
                priority: 3,
                estimate: 0.25,
                number: 5799,
                space_id: '888e30a23930d533ee3389f7',
                avatar_url: 'https://s3.amazonaws.com/assembla-avatars/652c4259/aZVJf86xar64o8aIC_Qgzw:1626958636',
                created_by_user_id: null,
                ticket_link_list: [],
            };

            const built_ticket_link = await ticket_service
                .buildTicketLinkObject(ticket);
            expect(built_ticket_link).to.be.an('object');

            const space_tag = 'new-desk';

            expect(built_ticket_link).to.deep.equal({
                ticket_id: '66cda23f8777771c8a7c15f6',
                space_tag,
                ticket_number: 5799,
                url_string: `https://polarity.assembla.com/spaces/${space_tag}/tickets/realtime_cardwall?ticket=5799`,
            });
        });

        it('Should be able to build link object from ticket 2', async () => {
            const ticket = {
                id: '66cda23f8777771c8a7c15f6',
                summary: '*Demande * Commentaire non visible dans le ticket desk',
                description: 'https://gitlab.com/nomination/frontend-portal-client-v2/-/merge_requests/757 (merged)\n\nhttps://polarity.assembla.com/spaces/conception/tickets/3069-*demande-d--39-enqu%C3%AAte*-commentaire-non-visible-dans-le-ticket-desk-/details',
                priority: 3,
                estimate: 0.25,
                number: 5799,
                space_id: '888e30a23930d533ee3389f8',
                avatar_url: 'https://s3.amazonaws.com/assembla-avatars/652c4259/aZVJf86xar64o8aIC_Qgzw:1626958636',
                created_by_user_id: null,
                ticket_link_list: [],
            };

            const built_ticket_link = await ticket_service
                .buildTicketLinkObject(ticket);
            expect(built_ticket_link).to.be.an('object');

            const space_tag = 'conception';

            expect(built_ticket_link).to.deep.equal({
                ticket_id: '66cda23f8777771c8a7c15f6',
                space_tag,
                ticket_number: 5799,
                url_string: `https://polarity.assembla.com/spaces/${space_tag}/tickets/realtime_cardwall?ticket=5799`,
            });
        });

        it('Should not be able to build link object from ticket when unexisting space id', async () => {
            const ticket = {
                id: '66cda23f8777771c8a7c15f6',
                summary: '*Demande * Commentaire non visible dans le ticket desk',
                description: 'https://gitlab.com/nomination/frontend-portal-client-v2/-/merge_requests/757 (merged)\n\nhttps://polarity.assembla.com/spaces/conception/tickets/3069-*demande-d--39-enqu%C3%AAte*-commentaire-non-visible-dans-le-ticket-desk-/details',
                priority: 3,
                estimate: 0.25,
                number: 5799,
                space_id: '888e30a23930d533ee3389f9', // <= unexisting space
                avatar_url: 'https://s3.amazonaws.com/assembla-avatars/652c4259/aZVJf86xar64o8aIC_Qgzw:1626958636',
                created_by_user_id: null,
                ticket_link_list: [],
            };

            try {
                await ticket_service.buildTicketLinkObject(ticket);
            } catch (error) {
                expect(error.message).to.equal('Space not found');
            }
        });

        it('buildTicketLinkObject', async () => {
            const result = await ticket_service.buildTicketLinkObject(TICKET_1);
            expect(result).to.be.an('object');
            expect(result).to.deep
                .equal({
                    ticket_id: 'd54484a5505e88f71651caa0',
                    ticket_number: 4321,
                    space_tag: 'new-desk',
                    url_string: 'https://polarity.assembla.com/spaces/new-desk/tickets/realtime_cardwall?ticket=4321',
                });
        });
    });

});

