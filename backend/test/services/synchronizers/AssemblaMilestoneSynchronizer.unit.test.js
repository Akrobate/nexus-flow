/* eslint-disable no-multi-str */

'use strict';

const axios = require('axios');
const {
    mock,
} = require('sinon');

const {
    DataLoader,
} = require('../../helpers/DataLoader');
const {
    AssemblaMilestoneSynchronizer,
} = require('../../../src/services/synchronizers');

const {
    MilestoneRepository,
    SpaceRepository,
} = require('../../../src/repositories');


const assembla_milestone_synchronizer = AssemblaMilestoneSynchronizer.getInstance();
const milestone_repository = MilestoneRepository.getInstance();
const space_repository = SpaceRepository.getInstance();

describe('AssemblaMilestoneSynchronizer.unit.test', () => {

    const mocks = {};
    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        mocks.axios = mock(axios);
        mocks.milestone_repository = mock(milestone_repository);
        mocks.space_repository = mock(space_repository);

    });

    afterEach(() => {
        mocks.axios.restore();
        mocks.milestone_repository.restore();
        mocks.space_repository.restore();
    });

    it('AssemblaMilestoneSynchronizer', async () => {

        const space_id = '888e30a23930d533ee3389f7';

        mocks.space_repository.expects('search')
            .resolves([
                {
                    id: '1',
                    title: 'Space 1',
                    description: 'Space 1 description',
                    assembla_id: space_id,
                    assembla: {
                        id: space_id,
                    },
                },
            ]);

        mocks.axios
            .expects('get')
            .withArgs(
                `https://api.assembla.com/v1/spaces/${space_id}/milestones/all.json`,
                {
                    params: {
                        page: 1,
                        per_page: 100,
                        due_date_order: undefined,
                    },
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [
                    {
                        id: '1',
                        title: 'Milestone 1',
                        description: 'Milestone 1 description',
                        space_id,
                    },
                    {
                        id: '2',
                        title: 'Milestone 2',
                        description: 'Milestone 2 description',
                        space_id,
                    },
                ],
            });


        mocks.milestone_repository.expects('search')
            .withArgs({
                assembla_id: '1',
            })
            .once()
            .resolves([
                {
                    id: '1',
                    title: 'Milestone 1',
                    description: 'Milestone 1 description',
                    space_id,
                },
            ]);

        mocks.milestone_repository.expects('search')
            .withArgs({
                assembla_id: '2',
            })
            .once()
            .resolves([]);

        mocks.milestone_repository.expects('update').once()
            .resolves({});
        mocks.milestone_repository.expects('create').once()
            .resolves({});
        await assembla_milestone_synchronizer.process(space_id);

        mocks.milestone_repository.verify();
        mocks.space_repository.verify();
        mocks.axios.verify();

    });

});
