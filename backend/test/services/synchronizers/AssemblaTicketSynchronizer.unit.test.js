/* eslint-disable no-multi-str */

'use strict';

const {
    mock,
} = require('sinon');
const axios = require('axios');
const {
    DataLoader,
} = require('../../helpers/DataLoader');
const {
    AssemblaTicketSynchronizer,
} = require('../../../src/services/synchronizers');


const {
    TicketRepository,
    TicketStatusRepository,
    MilestoneRepository,
    SpaceRepository,
    UserRepository,
} = require('../../../src/repositories');

const assembla_ticket_synchronizer = AssemblaTicketSynchronizer.getInstance();
const ticket_repository = TicketRepository.getInstance();

const ticket_status_repository = TicketStatusRepository.getInstance();
const milestone_repository = MilestoneRepository.getInstance();
const space_repository = SpaceRepository.getInstance();
const user_repository = UserRepository.getInstance();

describe('AssemblaTicketSynchronizer.unit.test', () => {

    const mocks = {};

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();

        mocks.axios = mock(axios);
        mocks.ticket_repository = mock(ticket_repository);
        mocks.ticket_status_repository = mock(ticket_status_repository);
        mocks.milestone_repository = mock(milestone_repository);
        mocks.space_repository = mock(space_repository);
        mocks.user_repository = mock(user_repository);

    });

    afterEach(() => {
        mocks.axios.restore();
        mocks.ticket_repository.restore();
        mocks.ticket_status_repository.restore();
        mocks.milestone_repository.restore();
        mocks.space_repository.restore();
        mocks.user_repository.restore();
    });


    it('AssemblaTicketSynchronizer', async () => {

        const space_id = 'assembla_id_888e30a23930d533ee3389f7';

        const report = 0;
        const sort_order = 'desc';
        const sort_by = 'created_on';

        mocks.axios
            .expects('get')
            .withArgs(
                `https://api.assembla.com/v1/spaces/${space_id}/tickets.json`,
                {
                    params: {
                        report,
                        page: 1,
                        per_page: 100,
                        sort_order,
                        sort_by,
                    },
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [
                    {
                        id: '1',
                        summary: 'ticket summary 1',
                        description: 'ticket 1 description',
                        status: 'Done',
                        space_id,
                    },
                    {
                        id: '2',
                        summary: 'ticket summary 2',
                        description: 'ticket 2 description',
                        status: 'Done',
                        space_id,
                    },
                ],
            });


        mocks.ticket_repository.expects('search')
            .withArgs({
                assembla_id: '1',
            })
            .once()
            .resolves([
                {
                    id: '1',
                    title: 'Milestone 1',
                    description: 'Milestone 1 description',
                    space_id,
                },
            ]);

        mocks.ticket_repository.expects('search')
            .withArgs({
                assembla_id: '2',
            })
            .once()
            .resolves([]);

        mocks.ticket_repository.expects('update').once()
            .resolves({});
        mocks.ticket_repository.expects('create').once()
            .resolves({});
        await assembla_ticket_synchronizer.process(space_id);

        mocks.milestone_repository.verify();
        mocks.space_repository.verify();
        mocks.axios.verify();

    });

});
