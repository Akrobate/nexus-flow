/* eslint-disable no-multi-str */

'use strict';

const axios = require('axios');
const {
    mock,
} = require('sinon');

const {
    DataLoader,
} = require('../../helpers/DataLoader');
const {
    AssemblaSpaceSynchronizer,
} = require('../../../src/services/synchronizers');

const {
    SpaceRepository,
} = require('../../../src/repositories');


const assembla_space_synchronizer = AssemblaSpaceSynchronizer.getInstance();
const space_repository = SpaceRepository.getInstance();

describe('AssemblaSpaceSynchronizer.unit.test', () => {

    const mocks = {};
    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
        mocks.axios = mock(axios);
        mocks.space_repository = mock(space_repository);

    });

    afterEach(() => {
        mocks.axios.restore();
        mocks.space_repository.restore();
    });

    it('AssemblaSpaceSynchronizer', async () => {

        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [
                    {
                        id: '1',
                        title: 'Space 1',
                        description: 'Space 1 description',
                    },
                    {
                        id: '2',
                        title: 'Space 1',
                        description: 'Space 1 description',
                    },
                ],
            });


        mocks.space_repository.expects('search')
            .withArgs({
                assembla_id: '1',
            })
            .once()
            .resolves([
                {
                    id: '1',
                    title: 'Space 1',
                    description: 'Space 1 description',
                },
            ]);

        mocks.space_repository.expects('search')
            .withArgs({
                assembla_id: '2',
            })
            .once()
            .resolves([]);

        mocks.space_repository.expects('update').once()
            .resolves({});
        mocks.space_repository.expects('create').once()
            .resolves({});

        await assembla_space_synchronizer.process();
        mocks.space_repository.verify();
        mocks.axios.verify();
    });

});
