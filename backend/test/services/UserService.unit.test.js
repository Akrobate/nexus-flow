'use strict';

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    UserService,
} = require('../../src/services');


const user_service = UserService.getInstance();

describe('UserService unit tests', () => {

    const mocks = {};

    beforeEach(() => {
        mocks.jwt = mock(user_service.jwt);
    });

    it('HashPassword test', () => {
        const hash = user_service.hashPassword('Password');
        expect(hash).to.equal('clS2ET0nXX5gnr/5TgQzJ8AnxPBOQeXGnJ4xdzwZUVc=');
    });

    it('tryToSignJwt test', () => {

        mocks.jwt.expects('sign')
            .once()
            .throws(new Error('jwt.sign error'));

        const user_data = {
            username: 'admin',
            password: 'Password',
        };

        try {
            user_service.tryToSignJwt(user_data);
        } catch (error) {
            expect(error.message).to.equal('jwt.sign error');
            mocks.jwt.verify();
        }
    });

});
