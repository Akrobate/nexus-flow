'use strict';

const {
    expect,
} = require('chai');

const {
    GitlabService,
} = require('../../src/services');

const gitlab_serivce = GitlabService.getInstance();

describe('GiltabService.unit.test', () => {

    it('detectMergeRequestUrl', () => {
        const content = 'Some content https://gitlab.com/groupname/service-ai/-/merge_requests/25 here';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([{
            project: 'service-ai',
            merge_request_id: 25,
            web_url: 'https://gitlab.com/groupname/service-ai/-/merge_requests/25',
        }]);
    });

    it('detectMergeRequestUrl 2', () => {
        const content = 'https://gitlab.com/groupname/service-ai/-/merge_requests/25';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([{
            project: 'service-ai',
            merge_request_id: 25,
            web_url: 'https://gitlab.com/groupname/service-ai/-/merge_requests/25',
        }]);
    });

    it('detectMergeRequestUrl 3', () => {
        const content = 'https://gitlab.com/groupname/service-ai/-/merge_requests/25 and an other one https://gitlab.com/groupname/service-ai-2/-/merge_requests/26';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([
            {
                project: 'service-ai',
                merge_request_id: 25,
                web_url: 'https://gitlab.com/groupname/service-ai/-/merge_requests/25',
            },
            {
                project: 'service-ai-2',
                merge_request_id: 26,
                web_url: 'https://gitlab.com/groupname/service-ai-2/-/merge_requests/26',
            },
        ]);
    });


    it('detectMergeRequestUrl 3.1', () => {
        const content = 'https://gitlab.com/groupname/service-ai/-/merge_requests/25 and an other one https://gitlab.com/groupname/service-ai-2/-/merge_requests/26 https://gitlab.com/groupname/service-ai/-/merge_requests/25 and an other one https://gitlab.com/groupname/service-ai-2/-/merge_requests/26';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([
            {
                project: 'service-ai',
                merge_request_id: 25,
                web_url: 'https://gitlab.com/groupname/service-ai/-/merge_requests/25',
            },
            {
                project: 'service-ai-2',
                merge_request_id: 26,
                web_url: 'https://gitlab.com/groupname/service-ai-2/-/merge_requests/26',
            },
        ]);
    });


    it('detectMergeRequestUrl 4', () => {
        const content = 'https://gitlab.com/groupname/service-ai/-/merge_requests/25 and an other one https://gitlab.com/groupname/service-ai-2/-/merge_requests/26 and should not detect this one https://github.com/groupname/service-ai/-/merge_requests/25';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([
            {
                project: 'service-ai',
                merge_request_id: 25,
                web_url: 'https://gitlab.com/groupname/service-ai/-/merge_requests/25',
            },
            {
                project: 'service-ai-2',
                merge_request_id: 26,
                web_url: 'https://gitlab.com/groupname/service-ai-2/-/merge_requests/26',
            },
        ]);
    });


    it('detectMergeRequestUrl 5', () => {
        const content = 'https://gitlab.com/invalid_format/-/merge_requests/25 and an other one https://gitlab.com/groupname/service-ai-2/-/merge_requests/26 and should not detect this one https://github.com/groupname/service-ai/-/merge_requests/25';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([
            {
                project: 'service-ai-2',
                merge_request_id: 26,
                web_url: 'https://gitlab.com/groupname/service-ai-2/-/merge_requests/26',
            },
        ]);
    });

    it('detectMergeRequestUrl 6', () => {
        const content = 'Content witouhout any url';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([]);
    });

    it('detectMergeRequestUrl 7', () => {
        const content = 'Content witouhout any gitlab url http://github.com';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([]);
    });

    it('detectMergeRequestUrl 8', () => {
        const content = 'https://gitlab.com/groupname/service-ai/-/merge_requests/zzz/';
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([]);
    });

    it('detectMergeRequestUrl 9', () => {
        const content = null;
        const result = gitlab_serivce.detectMergeRequestUrl(content);
        expect(result).to.deep.equal([]);
    });


    describe('normalizeMergeRequestUrl', () => {
        it('normalizeMergeRequestUrl 1', () => {
            const input = 'https://gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707/diffs?view=inline';
            const result = gitlab_serivce.normalizeMergeRequestUrl(input);
            expect(result).to.equal('https://gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707');

        });

        it('normalizeMergeRequestUrl 2', () => {
            const input = 'https://gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707/diffs';
            const result = gitlab_serivce.normalizeMergeRequestUrl(input);
            expect(result).to.equal('https://gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707');
        });

        it('normalizeMergeRequestUrl 2', () => {
            const input = 'https://gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707/';
            const result = gitlab_serivce.normalizeMergeRequestUrl(input);
            expect(result).to.equal('https://gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707');
        });
    });


    describe('extractProjectName', () => {
        it('extractProjectName 1', () => {
            const input = 'https://gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707/diffs?view=inline';
            const result = gitlab_serivce.extractProjectName(input);
            expect(result).to.equal('frontend-portal-client');
        });

        it('extractProjectName 2 (invalid gitlab url)', () => {
            const input = 'https://___gitlab.com/nomination/frontend-portal-client/-/merge_requests/1707/diffs?view=inline';
            const result = gitlab_serivce.extractProjectName(input);
            expect(result).to.equal(null);
        });

        it('extractProjectName 3', () => {
            const input = 'https://gitlab.com/nomination/merge_requests/';
            const result = gitlab_serivce.extractProjectName(input);
            expect(result).to.equal(null);
        });
    });


});

