const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    AuthenticationMiddleware,
} = require('../../src/middlewares/AuthenticationMiddleware');
const jwt = require('jsonwebtoken');


const authentication_middleware = AuthenticationMiddleware.getInstance();

describe('AuthenticationMiddleware unit', () => {

    const mocks = {};

    beforeEach(() => {
        mocks.jwt = mock(jwt);
    });

    afterEach(() => {
        mocks.jwt.restore();
    });

    it('getJwtFromHeader should throw Invalid token', () => {
        const request = {
            get: () => '',
        };
        try {
            AuthenticationMiddleware.getJwtFromHeader(request);
        } catch (error) {
            expect(error.message).to.be.equal('Invalid token');
        }
    });


    it('getJwtFromHeader should throw Invalid authorization header', () => {
        const request = {
            get: () => 'bearer bearer bearer bearer',
        };
        try {
            AuthenticationMiddleware.getJwtFromHeader(request);
        } catch (error) {
            expect(error.message).to.be.equal('Invalid authorization header');
        }
    });


    it('getJwtFromHeader should throw Invalid authorization header', () => {
        const request = {
            get: () => 'Zearer TOken',
        };
        try {
            AuthenticationMiddleware.getJwtFromHeader(request);
        } catch (error) {
            expect(error.message).to.be.equal('Invalid authorization header');
        }
    });


    it('checkJwtValidity(jwt_token) should throw error', () => {

        mocks.jwt.expects('verify').throws(new Error('jwt error'));

        try {
            authentication_middleware.checkJwtValidity('jwt_token');
        } catch (error) {
            expect(error).to.be.an('error');
            expect(error.message).to.be.equal('jwt error');
        }
    });


    it('injectJwtData should throw error', () => {

        mocks.jwt.expects('verify').throws(new Error('jwt error'));
        const request = {
            get: () => 'Bearer tokentokentokentokentokentokentokentoken',
        };

        const _next = (param) => {
            expect(param).to.be.an('error');
            expect(param.message).to.be.equal('jwt error');
        };
        authentication_middleware.injectJwtData()(request, {}, _next);

    });

});
