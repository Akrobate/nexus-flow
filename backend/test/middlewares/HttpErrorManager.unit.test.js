const {
    expect,
} = require('chai');
const {
    CustomError,
} = require('../../src/CustomError');
const status = require('http-status');

const {
    error_manager_middleware,
    not_found_error_middleware,
} = require('../../src/middlewares/HttpErrorManager');

describe('HttpErrorManager unit', () => {

    it('error_manager_middleware error code undefined', () => {
        const error = {
            message: 'test',
        };
        const response = {
            status: (_status) => {
                expect(_status).to.equal(status.INTERNAL_SERVER_ERROR);
                return {
                    json: (message) => {
                        expect(message.message).to.equal(error.message);
                    },
                };
            },
        };
        error_manager_middleware(error, null, response, null);
    });


    it('error_manager_middleware error code ACCESS_FORBIDDEN', () => {
        const error = {
            code: CustomError.ACCESS_FORBIDDEN,
        };
        const response = {
            status: (_status) => {
                expect(_status).to.equal(status.FORBIDDEN);
                return {
                    json: (message) => {
                        expect(message.message).to.equal(undefined);
                    },
                };
            },
        };
        error_manager_middleware(error, null, response, null);
    });


    it('error_manager_middleware error code unknown', () => {
        const error = {
            message: 'test',
            code: 'UNKNOWN',
        };
        let passed_in_callback = false;
        error_manager_middleware(error, null, null, () => {
            passed_in_callback = true;
        });
        expect(passed_in_callback).to.equal(true);

    });


    it('not_found_error_middleware', () => {
        const result = {
            status: (_status) => {
                expect(_status).to.equal(status.NOT_FOUND);
                return {
                    send: (_data) => {
                        expect(_data).to.deep.equal({});
                    },
                };
            },
        };
        not_found_error_middleware(null, result);
    });

});
