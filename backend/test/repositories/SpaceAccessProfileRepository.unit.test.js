'use strict';

const {
    expect,
} = require('chai');
const {
    ObjectId,
} = require('mongodb');
const {
    SpaceAccessProfileRepository,
} = require('../../src/repositories');


describe('SpaceAccessProfileRepository.unit.test', () => {

    const space_access_profile_repository = SpaceAccessProfileRepository.getInstance();

    describe('SpaceAccessProfileRepository formatSearchCriteria', () => {

        it('name', () => {
            expect(
                space_access_profile_repository.formatSearchCriteria(
                    {
                        name: 'Test_name_value',
                    }
                )
            ).to.deep.equal(
                {
                    name: {
                        $eq: 'Test_name_value',
                    },
                }
            );
        });


        it('user_id_list', () => {
            expect(
                space_access_profile_repository.formatSearchCriteria(
                    {
                        user_id_list: [
                            '888e30a23930d533ee1119f7',
                        ],
                    }
                )
            ).to.deep.equal(
                {
                    user_id: {
                        $in: [
                            new ObjectId('888e30a23930d533ee1119f7'),
                        ],
                    },
                }
            );
        });


        it('space_id_list', () => {
            expect(
                space_access_profile_repository.formatSearchCriteria(
                    {
                        space_id_list: [
                            '888e30a23930d533ee1119f7',
                        ],
                    }
                )
            ).to.deep.equal(
                {
                    space_id: {
                        $in: [
                            new ObjectId('888e30a23930d533ee1119f7'),
                        ],
                    },
                }
            );
        });
    });
});

