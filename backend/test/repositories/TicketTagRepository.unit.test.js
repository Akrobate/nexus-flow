'use strict';

const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    expect,
} = require('chai');
const {
    TicketTagRepository,
} = require('../../src/repositories');

const {
    TICKET_TAG_1,
} = require('../seeds');

const ticket_tag_repository = TicketTagRepository.getInstance();

describe('TicketTagRepository unit test', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
    });


    describe('Search', () => {

        it('all without filters', async () => {
            const result = await ticket_tag_repository.search();
            expect(result.length).to.equal(2);
        });

        it('by space_id', async () => {
            const result_1 = await ticket_tag_repository.search({
                space_id: TICKET_TAG_1.space_id,
            });
            expect(result_1.length).to.equal(2);
        });

    });

    describe('TicketTagRepository formatSearchCriteria', () => {

        it('assembla_id', () => {
            expect(
                ticket_tag_repository.formatSearchCriteria(
                    {
                        assembla_id: 'assambla_id_value',
                    }
                )
            ).to.deep.equal(
                {
                    'assembla.id': {
                        $eq: 'assambla_id_value',
                    },
                }
            );
        });

        it('assembla_id_list', () => {
            expect(
                ticket_tag_repository.formatSearchCriteria(
                    {
                        assembla_id_list: [
                            'assambla_id_value_1',
                            'assambla_id_value_2',
                        ],
                    }
                )
            ).to.deep.equal(
                {
                    'assembla.id': {
                        $in: [
                            'assambla_id_value_1',
                            'assambla_id_value_2',
                        ],
                    },
                }
            );
        });
    });

});

