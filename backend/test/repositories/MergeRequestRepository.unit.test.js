'use strict';

const {
    expect,
} = require('chai');

const {
    MergeRequestRepository,
} = require('../../src/repositories');


describe('MergeRequestRepository.unit.test.unit.test', () => {

    const merge_request_repository = MergeRequestRepository.getInstance();

    describe('MergeRequestRepository formatSearchCriteria', () => {
        it('merge_request_id', () => {
            expect(
                merge_request_repository.formatSearchCriteria(
                    {
                        merge_request_id: 1,
                    }
                )
            ).to.deep.equal({
                'merge_request.id': {
                    $eq: 1,
                },
            });
        });

        it('created_on_lower_boundary', () => {
            expect(
                merge_request_repository.formatSearchCriteria(
                    {
                        created_on_lower_boundary: '2020-01-01',
                    }
                )
            ).to.deep.equal(
                {
                    created_on: {
                        $gte: new Date('2020-01-01'),
                    },
                }
            );
        });

        it('created_on_upper_boundary', () => {
            expect(
                merge_request_repository.formatSearchCriteria(
                    {
                        created_on_upper_boundary: '2020-01-01',
                    }
                )
            ).to.deep.equal(
                {
                    created_on: {
                        $lte: new Date('2020-01-01'),
                    },
                }
            );
        });
    });
});

