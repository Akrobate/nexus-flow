'use strict';

const {
    v4,
} = require('uuid');
const {
    expect,
} = require('chai');
const {
    UserRepository,
} = require('../../src/repositories');

const user_repository = UserRepository.getInstance();


describe('UserRepository unit test', () => {

    it('User Search', async () => {

        const user_list = [
            {
                email: `${v4()}@${v4()}.${v4()}`,
            },
            {
                email: `${v4()}@${v4()}.${v4()}`,
            },
            {
                email: `${v4()}@${v4()}.${v4()}`,
            },
            {
                email: `${v4()}@${v4()}.${v4()}`,
            },
        ];

        const created_user_list = [];
        for (const user of user_list) {
            created_user_list.push(await user_repository.create(user));
        }

        const found_user_list = await user_repository.search({
            email_list: [
                user_list[2].email,
            ],
        });
        expect(found_user_list.length).to.equal(1);

        const found_by_id_user_list = await user_repository.search({
            id_list: [
                created_user_list[2].id,
                created_user_list[3].id,
            ],
        });

        expect(found_by_id_user_list[0].email).to.equal(created_user_list[2].email);
        expect(found_by_id_user_list[1].email).to.equal(created_user_list[3].email);
    });


    describe('UserRepository formatSearchCriteria', () => {

        it('assembla_id', () => {
            expect(
                user_repository.formatSearchCriteria(
                    {
                        assembla_id: 'ASSEMBLA_ID',
                    }
                )
            ).to.deep.equal(
                {
                    'assembla.id': {
                        $eq: 'ASSEMBLA_ID',
                    },
                }
            );
        });
    });

});

