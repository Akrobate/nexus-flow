'use strict';

const {
    expect,
} = require('chai');
const {
    ObjectId,
} = require('mongodb');
const {
    MilestoneRepository,
} = require('../../src/repositories');

describe('MilestoneRepository.unit.test', () => {

    const milestone_repository = MilestoneRepository.getInstance();

    describe('MilestoneRepository formatSearchCriteria', () => {

        it('assembla_id', () => {
            expect(
                milestone_repository.formatSearchCriteria(
                    {
                        assembla_id: 'ASSEMBLA_ID',
                    }
                )
            ).to.deep.equal(
                {
                    'assembla.id': {
                        $eq: 'ASSEMBLA_ID',
                    },
                }
            );
        });


        it('space_id', () => {
            expect(
                milestone_repository.formatSearchCriteria(
                    {
                        space_id: [
                            '888e30a23930d533ee1119f7',
                        ],
                    }
                )
            ).to.deep.equal(
                {
                    space_id: {
                        $eq: new ObjectId('888e30a23930d533ee1119f7'),
                    },
                }
            );
        });

    });
});

