'use strict';

const axios = require('axios');
const {
    mock,
} = require('sinon');
const {
    v4,
} = require('uuid');
const {
    expect,
} = require('chai');
const {
    GitlabRepository,
} = require('../../src/repositories/external/GitlabRepository');
const {
    configuration,
} = require('../../src/configuration');

const gitlab_repository = GitlabRepository.getInstance();


describe('GitlabRepository unit test', () => {

    const mocks = {};

    beforeEach(() => {
        mocks.axios = mock(axios);

    });

    afterEach(() => {
        mocks.axios.restore();
    });


    it('GitlabRepository Search', async () => {

        mocks.axios
            .expects('get')
            .withArgs(
                `${GitlabRepository.GITLAB_API_URL}/groups/${configuration.gitlab.group}/merge_requests`,
                {
                    params: {
                        state: 'all',
                        scope: 'all',
                        order_by: 'created_at',
                        sort: 'asc',
                        page: 0,
                        per_page: 20,
                    },
                    headers: GitlabRepository.REQUEST_HEADERS,
                    json: true,
                    timeout: GitlabRepository.TIMEOUT,
                }
            )
            .once()
            .resolves({
                data: {
                    id: 1,
                },
            });

        const result = await gitlab_repository.getAllMergeRequests();
        expect(result).to.be.an('Object');
        expect(result).to.not.have.property('data');
        expect(result).to.have.property('id');

    });


    it('GitlabRepository getMergeRequest', async () => {

        const project_id = v4();
        const merge_request_iid = v4();

        mocks.axios
            .expects('get')
            .withArgs(
                `${GitlabRepository.GITLAB_API_URL}/projects/${project_id}/merge_requests/${merge_request_iid}`,
                {
                    headers: GitlabRepository.REQUEST_HEADERS,
                    json: true,
                    timeout: GitlabRepository.TIMEOUT,
                }
            )
            .once()
            .resolves({
                data: {
                    id: 1,
                },
            });

        const result = await gitlab_repository.getMergeRequest(project_id, merge_request_iid);
        expect(result).to.be.an('Object');
        expect(result).to.not.have.property('data');
        expect(result).to.have.property('id');

    });
});

