'use strict';

const {
    expect,
} = require('chai');
const {
    ObjectId,
} = require('mongodb');
const {
    SpaceRepository,
} = require('../../src/repositories');

describe('SpaceRepository.unit.test', () => {

    const space_repository = SpaceRepository.getInstance();

    describe('SpaceRepository formatSearchCriteria', () => {

        it('assembla_id', () => {
            expect(
                space_repository.formatSearchCriteria(
                    {
                        assembla_id: 'ASSEMBLA_ID',
                    }
                )
            ).to.deep.equal(
                {
                    'assembla.id': {
                        $eq: 'ASSEMBLA_ID',
                    },
                }
            );
        });

        it('id_list', () => {
            expect(
                space_repository.formatSearchCriteria(
                    {
                        id_list: [
                            '888e30a23930d533ee1119f7',
                        ],
                    }
                )
            ).to.deep.equal(
                {
                    _id: {
                        $in: [
                            new ObjectId('888e30a23930d533ee1119f7'),
                        ],
                    },
                }
            );
        });
    });
});

