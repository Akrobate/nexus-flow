'use strict';

const {
    expect,
} = require('chai');
const {
    ObjectId,
} = require('mongodb');
const {
    MongoDbRepository,
} = require('../../src/repositories');
const {
    mock,
} = require('sinon');

// @todo: 379-380
describe('MongoDbRepository.unit.test', () => {

    const test_collection_name = 'test_collection_value';

    const mongo_db_repository = MongoDbRepository.getInstance();

    beforeEach(async () => {
        await mongo_db_repository.dropCollectionIfExists(test_collection_name);
    });


    it('Create collection', async () => {

        const list = await mongo_db_repository.listAllCollectionNames();
        expect(list).to.not.include(test_collection_name);

        const response = await mongo_db_repository
            .createCollectionIfNotExists(test_collection_name);
        expect(response).to.be.an('object');
        expect(response).to.have.property('s');
        expect(response.s).to.have.property('namespace');
        expect(response.s.namespace).to.have.property('collection');
        expect(response.s.namespace.collection).to.be.a('string');
        expect(response.s.namespace.collection).to.equal(test_collection_name);

        const list_after = await mongo_db_repository.listAllCollectionNames();
        expect(list_after).to.include(test_collection_name);

        await mongo_db_repository.dropCollection(test_collection_name);

        const list_after_remove = await mongo_db_repository.listAllCollectionNames();
        expect(list_after_remove).to.not.include(test_collection_name);
    });

    it('Create collection twice should return true if collection already exists', async () => {
        const response = await mongo_db_repository
            .createCollectionIfNotExists(test_collection_name);
        expect(response).to.be.an('object');
        expect(response).to.have.property('s');
        expect(response.s).to.have.property('namespace');
        expect(response.s.namespace).to.have.property('collection');
        expect(response.s.namespace.collection).to.be.a('string');
        expect(response.s.namespace.collection).to.equal(test_collection_name);

        const response_twice = await mongo_db_repository
            .createCollectionIfNotExists(test_collection_name);
        expect(response_twice).to.equal(true);
    });


    it('drop collection should return false if collection not exists', async () => {
        const response = await mongo_db_repository
            .dropCollectionIfExists(test_collection_name);
        expect(response).to.equal(false);
    });

    it('formatIdCriteria autoformat _id ObjectId', () => {
        const id = '888e30a23930d533ee1119f7';
        const criteria = mongo_db_repository.formatIdCriteria({
            _id: id,
        });
        expect(criteria).to.be.an('object');
        expect(criteria).to.have.property('_id');
        expect(criteria).to.deep.equal({
            _id: new ObjectId(id),
        });

    });

    it('formatIdCriteria autoformat _id ObjectId', () => {
        const id = new ObjectId('888e30a23930d533ee1119f7');
        const criteria = mongo_db_repository.formatIdCriteria({
            _id: id,
        });
        expect(criteria).to.be.an('object');
        expect(criteria).to.have.property('_id');
        expect(criteria).to.deep.equal({
            _id: new ObjectId('888e30a23930d533ee1119f7'),
        });

    });

    describe('updateMultipleDocuments', () => {

        it('insertDocumentList', async () => {
            await mongo_db_repository.createCollectionIfNotExists(test_collection_name);

            await mongo_db_repository.insertDocumentList(test_collection_name, [
                {
                    name: 'name_1',
                    value: 'value_1',
                },
                {
                    name: 'name_2',
                    value: 'value_2',
                },
            ]);

            const list = await mongo_db_repository.findDocumentList(test_collection_name);

            expect(list).to.be.an('Array');
            expect(list).to.have.length(2);
            expect(list[0]).to.have.property('name', 'name_1');
            expect(list[0]).to.have.property('value', 'value_1');
            expect(list[1]).to.have.property('name', 'name_2');
            expect(list[1]).to.have.property('value', 'value_2');
        });
    });


    describe('FindDocuments', () => {

        beforeEach(async () => {
            await mongo_db_repository.createCollectionIfNotExists(test_collection_name);
            await mongo_db_repository.insertDocumentList(test_collection_name, [
                {
                    name: 'name_1',
                    value: 'value_1',
                },
                {
                    name: 'name_2',
                    value: 'value_2',
                },
                {
                    name: 'name_3',
                    value: 'value_2',
                },
                {
                    name: 'name_4',
                    value: 'value_2',
                },
            ]);
        });


        it('findDocumentList', async () => {
            // query, sort, limit, offset, fields_selection
            const list = await mongo_db_repository
                .findDocumentList(test_collection_name, {}, {}, 2, 1);

            expect(list).to.be.an('Array');
            expect(list).to.have.length(2);
        });
    });


    it('updateDocumentList', async () => {
        await mongo_db_repository.createCollectionIfNotExists(test_collection_name);

        await mongo_db_repository.insertDocumentList(test_collection_name, [
            {
                name: 'name_1',
                value: 'value_1',
            },
            {
                name: 'name_2',
                value: 'value_2',
            },
            {
                name: 'name_3',
                value: 'value_3',
            },
        ]);

        const list = await mongo_db_repository.findDocumentList(test_collection_name);

        expect(list).to.be.an('Array');
        expect(list).to.have.length(3);
        expect(list[0]).to.have.property('name', 'name_1');
        expect(list[0]).to.have.property('value', 'value_1');
        expect(list[1]).to.have.property('name', 'name_2');
        expect(list[1]).to.have.property('value', 'value_2');
        expect(list[2]).to.have.property('name', 'name_3');
        expect(list[2]).to.have.property('value', 'value_3');

        await mongo_db_repository.updateDocuments(
            test_collection_name,
            {
                value: {
                    $in: ['value_1', 'value_2'],
                },
            },
            {
                value: 'value_updated',
                name: 'name_updated',
            }
        );

        const list_after_update = await mongo_db_repository.findDocumentList(test_collection_name);
        expect(list_after_update).to.be.an('Array');
        expect(list_after_update).to.have.length(3);
        expect(list_after_update[0]).to.have.property('name', 'name_updated');
        expect(list_after_update[0]).to.have.property('value', 'value_updated');
        expect(list_after_update[1]).to.have.property('name', 'name_updated');
        expect(list_after_update[1]).to.have.property('value', 'value_updated');
        expect(list_after_update[2]).to.have.property('name', 'name_3');
        expect(list_after_update[2]).to.have.property('value', 'value_3');

    });


    describe('dropDatabase', () => {

        it('dropDatabase', async () => {

            const mongo_db_repository_mock = mock(mongo_db_repository);
            const data_base_name = 'test_database_name_value';
            mongo_db_repository_mock
                .expects('getDatabaseConnection')
                .withArgs(data_base_name)
                .resolves(({
                    dropDatabase: () => Promise.resolve('from_mock'),
                }));

            const response = await mongo_db_repository.dropDatabase(data_base_name);
            expect(response).to.equal('from_mock');
            mongo_db_repository_mock.verify();
            mongo_db_repository_mock.restore();
        });
    });


    describe('Open/close connection', () => {
        it('Open/close connection', async () => {
            await mongo_db_repository.getConnection();
            const first_close_connection = await mongo_db_repository.closeConnection();
            expect(first_close_connection).to.equal(undefined);

            const second_close_connection = await mongo_db_repository.closeConnection();
            expect(second_close_connection).to.equal(null);
        });
    });


});
