'use strict';

const {
    DataLoader,
} = require('../helpers/DataLoader');
const {
    expect,
} = require('chai');
const {
    TicketRepository,
} = require('../../src/repositories');

const {
    TICKET_1,
    TICKET_3,
} = require('../seeds');

const ticket_repository = TicketRepository.getInstance();


describe('TicketRepository unit test', () => {

    beforeEach(async () => {
        await DataLoader.removeAll();
        await DataLoader.createAllSeeds();
    });


    describe('Search', () => {

        it('all without filters', async () => {
            const result = await ticket_repository.search();
            expect(result.length).to.equal(3);
        });

        it('by space_id 1', async () => {
            const result = await ticket_repository.search({
                space_id: TICKET_1.space_id,
            });
            expect(result.length).to.equal(2);
        });

        it('by space_id 2', async () => {
            const result = await ticket_repository.search({
                space_id: TICKET_3.space_id,
            });
            expect(result.length).to.equal(1);
        });

        it('by assembla id', async () => {
            const result = await ticket_repository.search({
                assembla_id: TICKET_3.assembla.id,
            });
            expect(result.length).to.equal(1);
        });

        it('should be able to offset results', async () => {
            const result = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                offset: 1,
            });
            expect(result.length).to.equal(1);
        });

        it('should be able to limit results', async () => {
            const result = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                limit: 1,
            });
            expect(result.length).to.equal(1);
        });

        it('criteria id / id_list', async () => {
            const [first_result, second_result] = await ticket_repository.search({
                space_id: TICKET_1.space_id,
            });
            const result_1 = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                id: first_result.id,
            });
            expect(result_1.length).to.equal(1);
            expect(result_1[0].id).to.equal(first_result.id);
            const result_2 = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                id: second_result.id,
            });
            expect(result_2.length).to.equal(1);
            expect(result_2[0].id).to.equal(second_result.id);

            const result_3 = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                id_list: [first_result.id],
            });
            expect(result_3.length).to.equal(1);
            expect(result_3[0].id).to.equal(first_result.id);

        });

        it('criteria status', async () => {
            const [first_result, second_result] = await ticket_repository.search({
                space_id: TICKET_1.space_id,
            });

            const result_1 = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                status_id: second_result.status_id,
            });
            expect(result_1[0].status_id).to.equal(second_result.status_id);

            const result_2 = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                status_id: first_result.status_id,
            });
            expect(result_2[0].status_id).to.equal(first_result.status_id);

            const result_3 = await ticket_repository.search({
                space_id: TICKET_1.space_id,
                status_id_list: [first_result.status_id],
            });

            expect(result_3).to.deep.equal(result_2);

        });


        it('criteria assembla_assigned_to_id', async () => {
            const [response] = await ticket_repository.search({
                assembla_assigned_to_id: TICKET_1.assembla.assigned_to_id,
            });

            expect(response).to.have.property('assembla');
            expect(response.assembla).to.have.property(
                'assigned_to_id',
                TICKET_1.assembla.assigned_to_id
            );
        });


        it('criteria assembla_reporter_id', async () => {
            const [response] = await ticket_repository.search({
                assembla_reporter_id: TICKET_1.assembla.reporter_id,
            });
            expect(response).to.have.property('assembla');
            expect(response.assembla).to.have.property(
                'reporter_id',
                TICKET_1.assembla.reporter_id
            );
        });


        it('criteria milestone_id_list', async () => {
            const [response] = await ticket_repository.search({
                milestone_id_list: [
                    TICKET_1.milestone_id,
                ],
            });

            expect(response).to.have.property('milestone_id', TICKET_1.milestone_id);
        });

        it('criteria assigned_to_user_id', async () => {
            const response = await ticket_repository.search({
                assigned_to_user_id: '42c24f6bd482227327d4bdc8',
            });
            expect(response.length).to.equal(0);
        });
    });


    describe('Count', () => {
        it('by space_id 1', async () => {
            const result = await ticket_repository.count({
                space_id: TICKET_1.space_id,
            });
            expect(result).to.equal(2);
        });

        it('Count all', async () => {
            const result = await ticket_repository.count();
            expect(result).to.equal(3);
        });
    });


    describe('Techincals unitary methods', () => {
        it('formatDocumentId', () => {
            const id = '5f6f3f4a0000b1ced2cd1b2a';
            const document = {
                _id: id,
            };
            ticket_repository.formatDocumentId(document);
            expect(document.id).to.equal(id);
            expect(document._id).to.equal(undefined);
        });
    });


    describe('Update', () => {
        it('should not be able to update with unexisting id', async () => {
            const NOT_EXISTING_ID = '664745a0000b1ced2cd1b2a3';
            try {
                await ticket_repository.update(NOT_EXISTING_ID, {});
            } catch (error) {
                expect(error.message).to.equal('Document to update not found');
            }
        });
    });


    describe('formatings', () => {

        it('formatSort', () => {
            const result = ticket_repository.formatSort([
                'test',
                '-coucou',
                'suptest.coucou',
            ]);
            expect(result).to.deep.equal({
                test: 1,
                coucou: -1,
                'suptest_data.coucou': 1,
            });
        });


        it('formatCreateUpdateInput - ObjectId|null', () => {
            const result = ticket_repository.formatCreateUpdateInput({
                assigned_to_user_id: '5f6f3f4a0000b1ced2cd1b2a',
            });

            expect(result).to.have.property('assigned_to_user_id');
            expect(result.assigned_to_user_id).to.deep.equal(
                ticket_repository.stringToObjectId('5f6f3f4a0000b1ced2cd1b2a')
            );
        });

        it('formatCreateUpdateInput - Array<ObjectId>', () => {
            const result = ticket_repository.formatCreateUpdateInput({
                merge_request_id_list: ['5f6f3f4a0000b1ced2cd1b2a'],
            });

            expect(result).to.have.property('merge_request_id_list');
            expect(result.merge_request_id_list).to.deep.equal(
                [
                    ticket_repository.stringToObjectId('5f6f3f4a0000b1ced2cd1b2a'),
                ]
            );
        });

        it('formatCreateUpdateInput - Date', () => {
            const result = ticket_repository.formatCreateUpdateInput({
                created_on: '2022-02-02',
            });

            expect(result).to.have.property('created_on');
            expect(result.created_on).to.deep.equal(
                new Date('2022-02-02')
            );
        });

    });

    describe('formatDocumentId', () => {
        it('formatDocumentId with _id', () => {

            const id = '5f6f3f4a0000b1ced2cd1b2a';
            const document = {
                _id: id,
            };
            const result = ticket_repository.formatDocumentId(document);

            expect(result).to.have.property('id', id);
            expect(result).not.to.have.property('_id');

        });

        it('formatDocumentId without _id', () => {

            const id = '5f6f3f4a0000b1ced2cd1b2a';
            const document = {
                id,
            };
            const result = ticket_repository.formatDocumentId(document);

            expect(result).to.have.property('id', id);
            expect(result).not.to.have.property('_id');

        });

    });
});

