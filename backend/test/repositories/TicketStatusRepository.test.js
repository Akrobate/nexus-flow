'use strict';

const {
    expect,
} = require('chai');
const {
    TicketStatusRepository,
} = require('../../src/repositories');

const ticket_status_repository = TicketStatusRepository.getInstance();

describe('TicketStatusRepository unit test', () => {


    describe('TicketStatusRepository formatSearchCriteria', () => {

        it('assembla_id', () => {
            expect(
                ticket_status_repository.formatSearchCriteria(
                    {
                        assembla_id: 'assambla_id_value',
                    }
                )
            ).to.deep.equal(
                {
                    'assembla.id': {
                        $eq: 'assambla_id_value',
                    },
                }
            );
        });
    });

});

