'use strict';

const {
    expect,
} = require('chai');
const {
    ObjectId,
} = require('mongodb');
const {
    SpaceUserAccessRepository,
} = require('../../src/repositories');

describe('SpaceUserAccessRepository.unit.test', () => {

    const space_user_access_repository = SpaceUserAccessRepository.getInstance();

    describe('SpaceRepository formatSearchCriteria', () => {

        it('user_id', () => {
            expect(
                space_user_access_repository.formatSearchCriteria(
                    {
                        user_id: '888e30a23930d533ee1119f7',
                    }
                )
            ).to.deep.equal(
                {
                    user_id: {
                        $eq: new ObjectId('888e30a23930d533ee1119f7'),
                    },
                }
            );
        });
    });
});

