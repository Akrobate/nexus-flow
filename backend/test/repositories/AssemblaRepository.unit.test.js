'use strict';

const axios = require('axios');
const {
    mock,
} = require('sinon');

const {
    expect,
} = require('chai');
const {
    AssemblaRepository,
} = require('../../src/repositories/external/AssemblaRepository');

const assembla_repository = AssemblaRepository.getInstance();

describe('AssemblaRepository unit test', () => {

    const mocks = {};

    beforeEach(() => {
        mocks.axios = mock(axios);

    });

    afterEach(() => {
        mocks.axios.restore();
    });


    it('getMe', async () => {

        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/user.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: {
                    username: 'test',
                },
            });

        const result = await assembla_repository.getMe();
        expect(result).to.be.an('Object');
        expect(result).to.not.have.property('data');
        expect(result).to.have.property('username', 'test');

    });


    it('getUsersBySpaceId', async () => {

        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/users.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getUsersBySpaceId('SPACE_ID_VALUE');
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('getSpaces', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getSpaces();
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('getTicket', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tickets/TICKET_NUMBER_VALUE',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getTicket('SPACE_ID_VALUE', 'TICKET_NUMBER_VALUE');
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');
    });


    it('getTicketComments', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tickets/TICKET_NUMBER_VALUE/ticket_comments',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                    params: {
                        per_page: 100,
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getTicketComments('SPACE_ID_VALUE', 'TICKET_NUMBER_VALUE');
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('getTags', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tags.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                    params: {
                        page: 1,
                        per_page: 10,
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getTags('SPACE_ID_VALUE', 1, 10);
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });

    it('getTicketsByMilestone', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tickets/milestone/MILESTONE_ID.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                    params: {
                        ticket_status: 'TICKET_STATUS',
                        page: 'PAGE',
                        per_page: 'PER_PAGE',
                        sort_order: 'SORT_ORDER',
                        sort_by: 'SORT_BY',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getTicketsByMilestone(
            'SPACE_ID_VALUE',
            'MILESTONE_ID',
            'TICKET_STATUS',
            'PAGE',
            'PER_PAGE',
            'SORT_ORDER',
            'SORT_BY'
        );
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('getTagsByTicketNumber', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tickets/TICKET_ID_VALUE/tags.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getTagsByTicketNumber('SPACE_ID_VALUE', 'TICKET_ID_VALUE');
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('getTicketStatusesBySpaceId', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tickets/statuses.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getTicketStatusesBySpaceId('SPACE_ID_VALUE');
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('getTickets', async () => {

        const report = 'value_report';
        const page = 'value_page';
        const per_page = 'value_per_page';
        const sort_order = 'value_sort_order';
        const sort_by = 'value_sort_by';

        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tickets.json',
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                    params: {
                        report,
                        page,
                        per_page,
                        sort_order,
                        sort_by,
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getTickets('SPACE_ID_VALUE', report, page, per_page, sort_order, sort_by);
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('getAllMilestones', async () => {
        mocks.axios
            .expects('get')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/milestones/all.json',
                {
                    params: {
                        page: 1,
                        per_page: 10,
                        due_date_order: 'due_date_order',
                    },
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const result = await assembla_repository.getAllMilestones('SPACE_ID_VALUE', 1, 10, 'due_date_order');
        expect(result).to.be.an('Array');
        expect(result).to.not.have.property('data');

    });


    it('updateTicket', async () => {
        mocks.axios
            .expects('put')
            .withArgs(
                'https://api.assembla.com/v1/spaces/SPACE_ID_VALUE/tickets/SPACE_ID_VALUE',
                {
                    ticket: {
                        a: 1,
                        b: 2,
                    },
                },
                {
                    headers: {
                        Accept: 'application/json',
                        'Content-type': 'application/json',
                        'X-Api-Key': 'assembla_client_id',
                        'X-Api-Secret': 'assembla_client_secret',
                    },
                }
            )
            .once()
            .resolves({
                data: [],
            });

        const data = {
            a: 1,
            b: 2,
        };

        const result = await assembla_repository.updateTicket('SPACE_ID_VALUE', 'SPACE_ID_VALUE', data);
        expect(result).to.be.an('object');
    });


});

