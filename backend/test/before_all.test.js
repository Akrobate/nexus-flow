'use strict';

const {
    stub,
} = require('sinon');

const MUTE_CONSOLE_LOGS = true;

before(() => {
    if (MUTE_CONSOLE_LOGS) {
        stub(console, 'log').callsFake(() => null);
        stub(console, 'info').callsFake(() => null);
        stub(console, 'error').callsFake(() => null);
    }
});
