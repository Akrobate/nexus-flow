'use strict';


const {
    expect,
} = require('chai');

const {
    CustomError,
} = require('../src/CustomError');


describe('CustomError unit', () => {

    it('Error message CustomError', () => {
        const error = new CustomError(CustomError.BAD_PARAMETER, 'Error message');
        expect(error.message).to.equal('Error message');
    });


    it('Error message object CustomError', () => {
        const error = new CustomError(CustomError.BAD_PARAMETER, {
            here: 'is an object',
        });
        expect(error.message_object.message).to.deep.equal({
            here: 'is an object',
        });
    });

    it('getCode message CustomError', () => {
        const error = new CustomError(CustomError.BAD_PARAMETER);

        expect(error.getCode()).to.equal(CustomError.BAD_PARAMETER);
    });

});
