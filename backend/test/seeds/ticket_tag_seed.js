'use strict';

const {
    TICKET_TAG_1_ID,
    TICKET_TAG_2_ID,
    SPACE_1_ID,
} = require('./id_referential');

const TICKET_TAG_1 = {
    id: TICKET_TAG_1_ID,
    name: 'A CULTIVER',
    space_id: SPACE_1_ID,
    state: 2,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
    color: '#e4e3e3',
};

const TICKET_TAG_2 = {
    id: TICKET_TAG_2_ID,
    name: 'Red Tag',
    space_id: SPACE_1_ID,
    state: 2,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
    color: '#ff0000',
};

module.exports = {
    TICKET_TAG_1,
    TICKET_TAG_2,
};

