'use strict';

const {
    SPACE_ACCESS_PROFILE_1_ID,
    SPACE_ACCESS_PROFILE_2_ID,
} = require('./id_referential');


const SPACE_ACCESS_PROFILE_1 = {
    id: SPACE_ACCESS_PROFILE_1_ID,
    name: 'Space member',
    access_list: [
        'ticket.search',
        'ticket.create',
        'ticket.read',
        'ticket.update',
        'ticket.delete',
        'space_user_access.update',
        'space_user_access.create',
        'space_user_access.delete',
        'ticket_status.create',
        'ticket_status.read',
        'ticket_status.update',
    ],
};

const SPACE_ACCESS_PROFILE_2 = {
    id: SPACE_ACCESS_PROFILE_2_ID,
    name: 'Space viewer',
    access_list: [
        'ticket.search',
        'ticket.read',
    ],
};

module.exports = {
    SPACE_ACCESS_PROFILE_1,
    SPACE_ACCESS_PROFILE_2,
};
