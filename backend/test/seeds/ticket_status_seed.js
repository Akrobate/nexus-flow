'use strict';

const {
    SPACE_1_ID,
    SPACE_2_ID,
    SPACE_1_TICKET_STATUS_1_ID,
    SPACE_1_TICKET_STATUS_2_ID,
    SPACE_1_TICKET_STATUS_3_ID,

    SPACE_2_TICKET_STATUS_1_ID,
    SPACE_2_TICKET_STATUS_2_ID,
    SPACE_2_TICKET_STATUS_3_ID,
} = require('./id_referential');

// Space 1 (dev)
const SPACE_1_TICKET_STATUS_1 = {
    id: SPACE_1_TICKET_STATUS_1_ID,
    name: 'To do',
    list_order: 0,
    space_id: SPACE_1_ID,
    state: 1,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
};

const SPACE_1_TICKET_STATUS_2 = {
    id: SPACE_1_TICKET_STATUS_2_ID,
    name: 'In progress',
    list_order: 1,
    space_id: SPACE_1_ID,
    state: 1,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
};

const SPACE_1_TICKET_STATUS_3 = {
    id: SPACE_1_TICKET_STATUS_3_ID,
    name: 'Done',
    list_order: 2,
    space_id: SPACE_1_ID,
    state: 2,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
};

// Space 2 (Conception)
const SPACE_2_TICKET_STATUS_1 = {
    id: SPACE_2_TICKET_STATUS_1_ID,
    name: 'To do',
    list_order: 0,
    space_id: SPACE_2_ID,
    state: 1,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
};

const SPACE_2_TICKET_STATUS_2 = {
    id: SPACE_2_TICKET_STATUS_2_ID,
    name: 'In progress',
    list_order: 1,
    space_id: SPACE_2_ID,
    state: 1,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
};

const SPACE_2_TICKET_STATUS_3 = {
    id: SPACE_2_TICKET_STATUS_3_ID,
    name: 'Done',
    list_order: 2,
    space_id: SPACE_2_ID,
    state: 2,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
};


module.exports = {
    SPACE_1_TICKET_STATUS_1,
    SPACE_1_TICKET_STATUS_2,
    SPACE_1_TICKET_STATUS_3,
    SPACE_2_TICKET_STATUS_1,
    SPACE_2_TICKET_STATUS_2,
    SPACE_2_TICKET_STATUS_3,
};

