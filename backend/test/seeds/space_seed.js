'use strict';

const {
    USER_1_ID,
    USER_2_ID,
    SPACE_1_ID,
    SPACE_2_ID,
} = require('./id_referential');

const SPACE_1 = {
    id: SPACE_1_ID,
    organization_id: '222e30a23930d533ee3389f7',
    name: 'Developpement',
    prefix: 'DEV',
    created_at: '2019-10-28T15:37:03.000Z',
    created_by_user_id: USER_1_ID,
    space_tag: 'new-desk',
    description: 'Space description',
    updated_at: '2020-01-15T16:00:06.000Z',
    updated_by_user_id: USER_1_ID,
    assembla: {
        id: `assembla_id_${SPACE_1_ID}`,
    },
};

const SPACE_2 = {
    id: SPACE_2_ID,
    organization_id: '222e30a23930d533ee3389f7',
    name: 'Conception',
    prefix: 'CONC',
    created_at: '2019-10-28T15:37:03.000Z',
    created_by_user_id: USER_2_ID,
    space_tag: 'conception',
    description: 'Space description',
    updated_at: '2020-01-15T16:00:06.000Z',
    updated_by_user_id: USER_2_ID,
    assembla: {
        id: `assembla_id_${SPACE_2_ID}`,
    },
};

module.exports = {
    SPACE_1,
    SPACE_2,
};
