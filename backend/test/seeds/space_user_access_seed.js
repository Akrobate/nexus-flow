'use strict';

const {
    USER_1_ID,
    USER_2_ID,

    SPACE_1_ID,
    SPACE_2_ID,

    SPACE_ACCESS_PROFILE_1_ID,
    SPACE_ACCESS_PROFILE_2_ID,

    SPACE_USER_1_ACCESS_1_ID,
    SPACE_USER_2_ACCESS_1_ID,
    SPACE_USER_2_ACCESS_2_ID,
} = require('./id_referential');

const SPACE_USER_1_ACCESS_1 = {
    id: SPACE_USER_1_ACCESS_1_ID,
    organization_id: '222e30a23930d533ee3389f7',
    user_id: USER_1_ID,
    space_id: SPACE_1_ID,
    override_access_list: [],
    access_profile_id: SPACE_ACCESS_PROFILE_1_ID,
};

const SPACE_USER_2_ACCESS_1 = {
    id: SPACE_USER_2_ACCESS_1_ID,
    organization_id: '222e30a23930d533ee3389f7',
    user_id: USER_2_ID,
    space_id: SPACE_2_ID,
    override_access_list: [],
    access_profile_id: SPACE_ACCESS_PROFILE_1_ID,
};

const SPACE_USER_2_ACCESS_2 = {
    id: SPACE_USER_2_ACCESS_2_ID,
    organization_id: '222e30a23930d533ee3389f7',
    user_id: USER_2_ID,
    space_id: SPACE_1_ID,
    override_access_list: [],
    access_profile_id: SPACE_ACCESS_PROFILE_2_ID,
};


module.exports = {
    SPACE_USER_1_ACCESS_1,
    SPACE_USER_2_ACCESS_1,
    SPACE_USER_2_ACCESS_2,
};
