'use strict';

const {
    USER_1_ID,
    USER_2_ID,
} = require('./id_referential');


const USER_1 = {
    id: USER_1_ID,
    email: 'USER_1.test@test.js',
    login: 'USER_1.test@test.js',
    password: 'rfTwjsiyzh/WnNVOUB1HlEOKewKUvP1/wfpyWbQHjHY=',
    organization_id: '777e30a23930d533ee3389f7',
};


const USER_2 = {
    id: USER_2_ID,
    email: 'USER_2.test@test.js',
    login: 'USER_2.test@test.js',
    password: 'rfTwjsiyzh/WnNVOUB1HlEOKewKUvP1/wfpyWbQHjHY=',
    organization_id: '777e30a23930d533ee3389f7',
};

const USER_1_PASSWORD = 'Mypassword';
const USER_2_PASSWORD = 'Mypassword';

module.exports = {
    USER_1,
    USER_1_PASSWORD,
    USER_2,
    USER_2_PASSWORD,
};


