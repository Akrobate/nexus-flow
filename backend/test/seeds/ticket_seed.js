'use strict';

const {
    SPACE_1_ID,
    SPACE_2_ID,
    SPACE_1_MILESTONE_1_ID,
    SPACE_1_TICKET_STATUS_1_ID,
    TICKET_1_ID,
    TICKET_2_ID,
    TICKET_3_ID,
} = require('./id_referential');

const TICKET_1 = {
    id: TICKET_1_ID,
    number: 4321,
    summary: 'Summary test payload TICKET_1',
    description: 'description test payload TICKET_1 https://gitlab.com/company/service-one/-/merge_requests/343',
    priority: 3,
    milestone_id: SPACE_1_MILESTONE_1_ID,
    space_id: SPACE_1_ID,
    assigned_to_user_id: null,
    created_by_user_id: null,
    status_id: SPACE_1_TICKET_STATUS_1_ID,
    estimate: 0,
    assembla: {
        id: 'assembla_id_1',
        assigned_to_id: 'aDPs5-RHar7QpccP_HzTya',
        reporter_id: 'aDPs5-RHar7QpccP_HzTya',
    },
};

const TICKET_2 = {
    id: TICKET_2_ID,
    number: 4325,
    summary: 'Summary test payload TICKET_2',
    description: 'description test payload TICKET_2',
    priority: 3,
    milestone_id: SPACE_1_MILESTONE_1_ID,
    space_id: SPACE_1_ID,
    status_id: SPACE_1_TICKET_STATUS_1_ID,
    assigned_to_user_id: null,
    created_by_user_id: null,
    estimate: 0,
    assembla: {
        id: 'assembla_id_2',
        assigned_to_id: 'cDO_tCQNir55qGacwqEsg8',
        reporter_id: 'cDO_tCQNir55qGacwqEsg8',
    },
};

const TICKET_3 = {
    id: TICKET_3_ID,
    number: 4327,
    summary: 'Summary test payload TICKET_2',
    description: 'description test payload TICKET_2',
    priority: 3,
    milestone_id: SPACE_1_MILESTONE_1_ID,
    space_id: SPACE_2_ID,
    status_id: SPACE_1_TICKET_STATUS_1_ID,
    assigned_to_user_id: null,
    created_by_user_id: null,
    estimate: 0,
    assembla: {
        id: 'assembla_id_3',
        assigned_to_id: 'aDPs5-RHar7QpccP_HzTya',
        reporter_id: 'aDPs5-RHar7QpccP_HzTya',
    },
};

module.exports = {
    TICKET_1,
    TICKET_2,
    TICKET_3,
};
