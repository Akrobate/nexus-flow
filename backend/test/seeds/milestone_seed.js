'use strict';

const {
    USER_1_ID,
    SPACE_1_ID,
    SPACE_1_MILESTONE_1_ID,
} = require('./id_referential');

const SPACE_1_MILESTONE_1 = {
    id: SPACE_1_MILESTONE_1_ID,
    start_date: '2019-11-04',
    due_date: '2019-11-11',
    title: 'Dev - Sprint 1 - space 1',
    description: 'Milestone description',
    user_id: USER_1_ID,
    created_at: '2019-10-28T15:37:03.000Z',
    created_by_user_id: USER_1_ID,
    space_id: SPACE_1_ID,
    is_completed: false,
    completed_date: '2020-01-15',
    updated_at: '2020-01-15T16:00:06.000Z',
    updated_by_user_id: USER_1_ID,
    assembla: {
        id: `assembla_id_${SPACE_1_MILESTONE_1_ID}`,
    },
};

module.exports = {
    SPACE_1_MILESTONE_1,
};
