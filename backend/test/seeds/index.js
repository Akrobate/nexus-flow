'use strict';

const {
    SPACE_ACCESS_PROFILE_1,
    SPACE_ACCESS_PROFILE_2,
} = require('./space_access_profile_seed');

const {
    SPACE_1,
    SPACE_2,
} = require('./space_seed');

const {
    SPACE_USER_1_ACCESS_1,
    SPACE_USER_2_ACCESS_1,
    SPACE_USER_2_ACCESS_2,
} = require('./space_user_access_seed');

const {
    USER_1,
    USER_1_PASSWORD,
    USER_2,
    USER_2_PASSWORD,
} = require('./user_seed');

const {
    ORGANIZATION_1,
} = require('./organization_seed');

const {
    SPACE_1_TICKET_STATUS_1,
    SPACE_1_TICKET_STATUS_2,
    SPACE_1_TICKET_STATUS_3,
    SPACE_2_TICKET_STATUS_1,
    SPACE_2_TICKET_STATUS_2,
    SPACE_2_TICKET_STATUS_3,
} = require('./ticket_status_seed');

const {
    SPACE_1_MILESTONE_1,
} = require('./milestone_seed');

const {
    TICKET_1,
    TICKET_2,
    TICKET_3,
} = require('./ticket_seed');

const {
    TICKET_TAG_1,
    TICKET_TAG_2,
} = require('./ticket_tag_seed');

module.exports = {
    USER_1,
    USER_2,
    USER_1_PASSWORD,
    USER_2_PASSWORD,
    ORGANIZATION_1,
    SPACE_1,
    SPACE_2,
    SPACE_USER_1_ACCESS_1,
    SPACE_USER_2_ACCESS_1,
    SPACE_USER_2_ACCESS_2,
    SPACE_ACCESS_PROFILE_1,
    SPACE_ACCESS_PROFILE_2,

    SPACE_1_TICKET_STATUS_1,
    SPACE_1_TICKET_STATUS_2,
    SPACE_1_TICKET_STATUS_3,
    SPACE_2_TICKET_STATUS_1,
    SPACE_2_TICKET_STATUS_2,
    SPACE_2_TICKET_STATUS_3,

    SPACE_1_MILESTONE_1,

    TICKET_1,
    TICKET_2,
    TICKET_3,

    TICKET_TAG_1,
    TICKET_TAG_2,
};
