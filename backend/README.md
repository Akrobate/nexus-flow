

https://www.browserling.com/tools/random-hex


```mermaid
erDiagram
    tickets ||--o{ ticket_comments : has
    tickets ||--|| ticket_statuses: has 
    tickets ||--o{ ticket_tags: has

    milestones ||--o{ tickets : has

    spaces ||--o{ tickets : has
    spaces ||--o{ ticket_statuses : has
    spaces ||--o{ ticket_tags : has
    spaces ||--o{ milestones : has

    users ||--o{ tickets : creates
    users ||--o{ ticket_comments : comment
    users ||--o{ milestones : creates

    tickets {
        string id
        string summary
        string description
        number priority
        date completed_date
        date created_on
        string milestone_id
        string space_id
        string status_id
        date updated_at
        number working_hours
        number estimate
        string assigned_to_user_id
        string created_by_user_id
        number consumed_time
        date due_date
    }
    ticket_comments {
        string id
        string comment
        string ticket_id
        string user_id
        string space_id
        date created_on
        date updated_at
        string ticket_changes
    }
    ticket_statuses {
        string id
        string name
        string state
        number list_order
        date created_at
        date updated_at
        string space_id
    }
    ticket_tags {
        string id
        string name
        string space_id
        number state
        date created_at
        date updated_at
        string color
    }
    milestones {
        string id
        date start_date
        date due_date
        string title
        string user_id
        date created_at
        string created_by_user_id
        string space_id
        string description
        boolean is_completed
        date completed_date
        date updated_at
        string updated_by_user_id
    }
    spaces {
        string id
        string organization_id
        date start_date
        date due_date
        string title
        string user_id
        date created_at
        string created_by_user_id
        string space_id
        string description
        boolean is_completed
        date completed_date
        date updated_at
        string updated_by_user_id
    }
    organizations ||--o{ users : Belongs

    space_user_accesses ||--o{ organizations : has
    space_user_accesses ||--o{ users : has
    space_user_accesses ||--o{ spaces : has
    space_user_accesses ||--o{ user_access_profiles : has
    
    organizations {
        string id
        string name
    }
    users {
        string id
        string organization_id
        string first_name
        string last_name
        string name
        boolean organization_manager
        string email
        string login
        string password
        date created_at
        date updated_at
    }
    space_user_accesses {
        string id
        string organization_id
        string user_id
        string space_id
        string override_access_list
        string access_profile_id
    }
    user_access_profiles {
        string id
        string name
        string access_list
    }

```



```js
const ticket = {
    id: 231235306,
    number: 1,
    summary: 'Bug fix ticket summary',
    description: 'ticket description',
    priority: 3,
    completed_date: '2019-11-12T11:16:21.000Z',
    created_at: '2019-11-12T11:16:01.000Z',
    milestone_id: 12948597,
    space_id: 'bXqO74QNir54k9acwqEsg8',
    status_id: 'Deployed',
    updated_at: '2023-08-29T12:39:15.000Z',
    working_hours: 0,
    estimate: 0.25,
    assigned_to_user_id: '4k9acwqEsg8bXqO74QNir5',
    created_by_user_id: 'ir54k9acwqEsg8bXqO74QN',
    consumed_time: 0.25,
    due_date: null,
}
```


```js
const ticket_comment = {
    id: 1707922899,
    comment: 'Ticket comment',
    ticket_id: 231235306,
    user_id: 'cDO_tCQNir55qGacwqEsg8',
    space_id: 'bXqO74QNir54k9acwqEsg8',
    created_on: '2023-08-29T12:39:15.000Z',
    updated_at: '2023-08-29T12:39:15.000Z',
    ticket_changes: '---\n- - status\n  - Done\n  - Deployed\n',
}
```


```js
const ticket_status = {
    id: 23586753,
    name: 'New',
    state: 1,
    list_order: 0,
    created_at: '2017-10-06T08:46:22.000Z',
    updated_at: '2017-10-06T08:46:22.000Z',
    space_id: 'bXqO74QNir54k9acwqEsg8',
}
```


```js
const ticket_tag = {
    id: 2052271,
    name: 'A CULTIVER',
    space_id: 'bXqO74QNir54k9acwqEsg8',
    state: 2,
    created_at: '2019-01-21T11:38:14.000Z',
    updated_at: '2019-01-21T11:38:14.000Z',
    color: '#e4e3e3',
}
```


```js
const milestone = {
    id: 12948597,
    start_date: '2019-11-04',
    due_date: '2019-11-11',
    title: "Sprint 52",
    user_id: null,
    created_at: '2019-10-28T15:37:03.000Z',
    created_by_user_id: 654654,
    space_id: 'bXqO74QNir54k9acwqEsg8',
    description: 'Milestone description',
    is_completed: true,
    completed_date: '2020-01-15',
    updated_at: '2020-01-15T16:00:06.000Z',
    updated_by_user_id: 987987,
}
```


```js
const space = {
    id: 12948597,
    organization_id: 654654,
    name: "Conception",
    prefix: "CO",
    created_at: '2019-10-28T15:37:03.000Z',
    created_by_user_id: 654654,
    description: 'Space description',
    updated_at: '2020-01-15T16:00:06.000Z',
    updated_by_user_id: 987987,
}
```


```js
const organization = {
    id: 123,
    name: 'Organization name',
}
```


```js
const user = {
    id: 123,
    organization_id: 654654,
    first_name: 'First name',
    last_name: 'Last name',
    name: 'FName',
    organization_manager: true,
    email: 'aze@aze.aze',
    login: 'aze@aze.aze',
    password: 'qlsdkjfhqsldkfjhqsdlkfjh',
    avatar_url: '', 
    created_at: '',
    updated_at: '',
}
```


```js
const assembla_user_format = {
    id: 'aybFP0PBGr5Oykdmr6CpXy',
    login: 'mlagarde',
    name: 'Mathieu Lagarde',
    picture: 'https://www.assembla.com/v1/users/aybFP0PBGr5Oykdmr6CpXy/picture',
    email: 'mlagarde@nomination.fr',
    organization: null,
    phone: null
  };
```

SEEED
{
    _id: ObjectId('111e30a23930d533ee3389f7'),
    email: 'USER_1.test@test.js',
    login: 'USER_1.test@test.js',
    password: 'rfTwjsiyzh/WnNVOUB1HlEOKewKUvP1/wfpyWbQHjHY=',
    organization_id: ObjectId('777e30a23930d533ee3389f7')
}




```js
const space_user_access = {
    id: 123,
    organization_id: 654654,
    user_id: 987897,
    space_id: 987987987,
    override_access_list: [
        'TICKET_UPDATE',
        'TICKET_CREATE',
        'TICKET_SEARCH',
        'TICKET_DELETE',
    ],
    access_profile_id: 123,
}
```


```js
const user_access_profile = {
    id: 123,
    name: 'Administrator',
    access_list: [
        'TICKET_UPDATE',
        'TICKET_CREATE',
        'TICKET_SEARCH',
        'TICKET_DELETE',
    ],
}
```
