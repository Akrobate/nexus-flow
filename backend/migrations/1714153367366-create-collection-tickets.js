'use strict';

const {
    MongoDbRepository,
} = require('../src/repositories');

const mongo_db_repository = MongoDbRepository.getInstance();

module.exports.up = (next) => {
    mongo_db_repository
        .createCollection('tickets')
        .then(() => next());
};


module.exports.down = (next) => {
    mongo_db_repository
        .dropCollection('tickets')
        .then(() => next());
};
