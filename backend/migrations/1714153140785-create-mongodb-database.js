'use strict';

const {
    MongoDbRepository,
} = require('../src/repositories');

const {
    configuration,
} = require('../src/configuration');

const {
    database_name,
} = configuration.storage.mongodb;

const mongo_db_repository = MongoDbRepository.getInstance();

module.exports.up = (next) => {
    next();
};

module.exports.down = (next) => {
    mongo_db_repository
        .dropDatabase(database_name)
        .then(() => next());
};
