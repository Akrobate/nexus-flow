/* istanbul ignore file */

'use strict';

const {
    configuration,
} = require('./configuration');

const {
    logger,
} = require('./logger');

const {
    server,
} = require('./server');

server.listen(configuration.server.port, () => logger
    .info(`NexusFlow listening on port: ${configuration.server.port}`)
);
