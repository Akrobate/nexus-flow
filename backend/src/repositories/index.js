'use strict';

const {
    MongoDbRepository,
} = require('./MongoDbRepository');
const {
    MilestoneRepository,
} = require('./MilestoneRepository');
const {
    AbstractRepository,
} = require('./AbstractRepository');
const {
    TicketRepository,
} = require('./TicketRepository');
const {
    TicketStatusRepository,
} = require('./TicketStatusRepository');
const {
    TicketTagRepository,
} = require('./TicketTagRepository');
const {
    OrganizationRepository,
} = require('./OrganizationRepository');
const {
    UserRepository,
} = require('./UserRepository');
const {
    SpaceRepository,
} = require('./SpaceRepository');
const {
    SpaceUserAccessRepository,
} = require('./SpaceUserAccessRepository');
const {
    SpaceAccessProfileRepository,
} = require('./SpaceAccessProfileRepository');
const {
    AssemblaRepository,
} = require('./external/AssemblaRepository');
const {
    GitlabRepository,
} = require('./external/GitlabRepository');
const {
    MergeRequestRepository,
} = require('./MergeRequestRepository');

module.exports = {
    AbstractRepository,
    AssemblaRepository,
    SpaceRepository,
    SpaceUserAccessRepository,
    SpaceAccessProfileRepository,
    TicketRepository,
    TicketStatusRepository,
    TicketTagRepository,
    MilestoneRepository,
    MongoDbRepository,
    UserRepository,
    OrganizationRepository,
    GitlabRepository,
    MergeRequestRepository,
};
