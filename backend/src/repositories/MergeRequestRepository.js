'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class MergeRequestRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {MergeRequestRepository}
     */
    static getInstance() {
        if (MergeRequestRepository.instance === null) {
            MergeRequestRepository.instance = new MergeRequestRepository();
        }

        return MergeRequestRepository.instance;
    }


    /**
     * @return {MergeRequestRepository}
     */
    constructor() {
        super();
        this.collection_name = 'merge_requests';

        this.schema = {
            created_on: 'Date',
            updated_at: 'Date',
        };
    }

    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            merge_request_id,
            created_on_lower_boundary,
            created_on_upper_boundary,
            web_url,
        } = input;

        const query = {};

        if (merge_request_id) {
            query['merge_request.id'] = {
                $eq: merge_request_id,
            };
        }

        if (created_on_lower_boundary) {
            query.created_on = {
                $gte: new Date(created_on_lower_boundary),
            };
        }

        if (created_on_upper_boundary) {
            query.created_on = {
                $lte: new Date(created_on_upper_boundary),
            };
        }

        if (web_url) {
            query.web_url = {
                $eq: web_url,
            };
        }

        return query;
    }

}

MergeRequestRepository.instance = null;

module.exports = {
    MergeRequestRepository,
};
