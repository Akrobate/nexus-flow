'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class TicketTagRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {TicketTagRepository}
     */
    static getInstance() {
        if (TicketTagRepository.instance === null) {
            TicketTagRepository.instance = new TicketTagRepository();
        }

        return TicketTagRepository.instance;
    }


    /**
     * @return {TicketTagRepository}
     */
    constructor() {
        super();
        this.collection_name = 'ticket_tags';

        this.schema = {
            space_id: 'ObjectId',
        };
    }

    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            space_id,
            assembla_id,
            assembla_id_list,
        } = input;

        const query = {};

        if (space_id) {
            query.space_id = {
                $eq: this.stringToObjectId(space_id),
            };
        }

        if (assembla_id) {
            query['assembla.id'] = {
                $eq: assembla_id,
            };
        }

        if (assembla_id_list) {
            query['assembla.id'] = {
                $in: assembla_id_list,
            };
        }

        return query;
    }

}

TicketTagRepository.instance = null;

module.exports = {
    TicketTagRepository,
};
