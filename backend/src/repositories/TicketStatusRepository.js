'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class TicketStatusRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {TicketStatusRepository}
     */
    static getInstance() {
        if (TicketStatusRepository.instance === null) {
            TicketStatusRepository.instance = new TicketStatusRepository();
        }

        return TicketStatusRepository.instance;
    }


    /**
     * @return {TicketStatusRepository}
     */
    constructor() {
        super();
        this.collection_name = 'ticket_statuses';

        this.schema = {
            space_id: 'ObjectId',
        };
    }

    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            space_id,
            assembla_id,
        } = input;

        const query = {};

        if (space_id) {
            query.space_id = {
                $eq: this.stringToObjectId(space_id),
            };
        }

        if (assembla_id) {
            query['assembla.id'] = {
                $eq: assembla_id,
            };
        }

        return query;
    }
}

TicketStatusRepository.instance = null;

module.exports = {
    TicketStatusRepository,
};
