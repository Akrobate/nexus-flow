'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class SpaceUserAccessRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {SpaceUserAccessRepository}
     */
    static getInstance() {
        if (SpaceUserAccessRepository.instance === null) {
            SpaceUserAccessRepository.instance = new SpaceUserAccessRepository();
        }

        return SpaceUserAccessRepository.instance;
    }


    /**
     * @return {SpaceUserAccessRepository}
     */
    constructor() {
        super();
        this.collection_name = 'space_user_accesses';

        this.schema = {
            organization_id: 'ObjectId',
            user_id: 'ObjectId',
            space_id: 'ObjectId',
            access_profile_id: 'ObjectId',
        };
    }


    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            user_id,
            user_id_list,
            space_id,
            space_id_list,
        } = input;

        const query = {};

        if (user_id) {
            query.user_id = {
                $eq: this.stringToObjectId(user_id),
            };
        }
        if (user_id_list) {
            query.user_id = {
                $in: this.stringListToObjectIdList(user_id_list),
            };
        }
        if (space_id) {
            query.space_id = {
                $eq: this.stringToObjectId(space_id),
            };
        }
        if (space_id_list) {
            query.space_id = {
                $in: this.stringListToObjectIdList(space_id_list),
            };
        }


        return query;
    }


}

SpaceUserAccessRepository.instance = null;

module.exports = {
    SpaceUserAccessRepository,
};
