'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class SpaceRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {SpaceRepository}
     */
    static getInstance() {
        if (SpaceRepository.instance === null) {
            SpaceRepository.instance = new SpaceRepository();
        }

        return SpaceRepository.instance;
    }


    /**
     * @return {SpaceRepository}
     */
    constructor() {
        super();
        this.collection_name = 'spaces';

        this.schema = {
            organization_id: 'ObjectId',
            created_by_user_id: 'ObjectId',
            updated_by_user_id: 'ObjectId',
        };
    }

    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            assembla_id,
            id,
            id_list,
        } = input;

        const query = {};

        if (assembla_id) {
            query['assembla.id'] = {
                $eq: assembla_id,
            };
        }

        if (id) {
            query._id = {
                $eq: this.stringToObjectId(id),
            };
        }


        if (id_list) {
            query._id = {
                $in: this.stringListToObjectIdList(id_list),
            };
        }

        return query;
    }

}

SpaceRepository.instance = null;

module.exports = {
    SpaceRepository,
};
