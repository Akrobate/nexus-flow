'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class UserRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {UserRepository}
     */
    static getInstance() {
        if (UserRepository.instance === null) {
            UserRepository.instance = new UserRepository();
        }

        return UserRepository.instance;
    }


    /**
     * @return {UserRepository}
     */
    constructor() {
        super();
        this.collection_name = 'users';

        this.schema = {
            organization_id: 'ObjectId',
        };
    }


    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            email_list,
            id_list,
            login,
            assembla_id,
        } = input;

        const query = {};

        if (login) {
            query.login = {
                $eq: login,
            };
        }
        if (email_list) {
            query.email = {
                $in: email_list,
            };
        }

        if (id_list) {
            query._id = {
                $in: this.stringListToObjectIdList(id_list),
            };
        }

        if (assembla_id) {
            query['assembla.id'] = {
                $eq: assembla_id,
            };
        }

        return query;
    }

}

UserRepository.instance = null;

module.exports = {
    UserRepository,
};
