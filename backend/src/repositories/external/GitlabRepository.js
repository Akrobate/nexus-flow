'use strict';

const axios = require('axios');

const {
    configuration,
} = require('../../configuration');

class GitlabRepository {

    /**
     * @static
     * @retunrs {Number}
     */
    static get TIMEOUT() {
        return 60000;
    }

    /**
     * @static
     * @retunrs {Object}
     */
    static get GITLAB_API_URL() {
        return 'https://gitlab.com/api/v4';
    }

    /**
     * @static
     * @retunrs {Object}
     */
    static get REQUEST_HEADERS() {
        return {
            'PRIVATE-TOKEN': configuration.gitlab.private_token,
        };
    }

    /**
     * @static
     * @retunrs {Object}
     */
    static get REQUEST_GENERIC_PARAMS() {
        return {
            headers: GitlabRepository.REQUEST_HEADERS,
            json: true,
            timeout: GitlabRepository.TIMEOUT,
        };
    }

    /* istanbul ignore next */
    /**
     * getInstance instance getter
     * @param  {String} token_access       Authentication token
     * @return {GitlabRepository}
     */
    static getInstance() {
        if (GitlabRepository.instance === null) {
            GitlabRepository.instance = new GitlabRepository(
                Request
            );
        }
        return GitlabRepository.instance;
    }


    /**
     * @param {*} page
     * @param {*} per_page
     * @returns {Promise}
     */
    async getAllMergeRequests(
        page = 0,
        per_page = 20
    ) {
        const response = await axios.get(
            `${GitlabRepository.GITLAB_API_URL}/groups/${configuration.gitlab.group}/merge_requests`,
            {
                params: {
                    state: 'all',
                    scope: 'all',
                    order_by: 'created_at',
                    sort: 'asc',
                    per_page,
                    page,
                },
                ...GitlabRepository.REQUEST_GENERIC_PARAMS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }


    /**
     * @param {*} project_id
     * @param {*} merge_request_iid
     * @returns {Promise}
     */
    async getMergeRequest(project_id, merge_request_iid) {
        const response = await axios.get(
            `${GitlabRepository.GITLAB_API_URL}/projects/${project_id}/merge_requests/${merge_request_iid}`,
            GitlabRepository.REQUEST_GENERIC_PARAMS
        );

        const {
            data,
        } = response;
        return data;
    }

}

GitlabRepository.instance = null;

module.exports = {
    GitlabRepository,
};
