'use strict';

const {
    configuration,
} = require('../../configuration');

const axios = require('axios');

class AssemblaRepository {


    /**
     * @static
     * @retunrs {Object}
     */
    static get ASSEMBLA_URL() {
        return 'https://api.assembla.com';
    }

    /**
     * @static
     * @retunrs {Object}
     */
    static get REQUEST_HEADERS() {
        return {
            Accept: 'application/json',
            'Content-type': 'application/json',
            'X-Api-Key': configuration.assembla.client_id,
            'X-Api-Secret': configuration.assembla.client_secret,
        };
    }

    /* istanbul ignore next */
    /**
     * getInstance instance getter
     * @param  {String} token_access       Authentication token
     * @return {AssemblaRepository}
     */
    static getInstance() {
        if (AssemblaRepository.instance === null) {
            AssemblaRepository.instance = new AssemblaRepository(
                Request
            );
        }
        return AssemblaRepository.instance;
    }

    /**
     * constructor
     * @param  {Request} request
     * @returns {AssemblaRepository}
     */
    constructor(request) {
        this.assembla_api_host = 'https://api.assembla.com';
        this.request = request;
    }


    /**
     * getMe return the connected user information
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getMe() {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/user.json`,
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }


    /**
     * getMe return the connected user information
     * @param {String} space_id Return promise of the Api result
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getUsersBySpaceId(space_id) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/users.json`,
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }


    /**
     * getSpaces
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getSpaces() {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces.json`,
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }


    /**
     * getTickets
     * @param  {String} space_id
     * @param  {[type]} report       [description]
     * @param  {[type]} page       [description]
     * @param  {[type]} per_page   [description]
     * @param  {[type]} sort_order [description]
     * @param  {[type]} sort_by    [description]
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getTickets(space_id, report, page, per_page, sort_order, sort_by) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets.json`,
            {
                params: {
                    report,
                    page,
                    per_page,
                    sort_order,
                    sort_by,
                },
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }


    /**
     * Returns ticket by ticket number
     *
     * GET /v1/spaces/:space_id/tickets/:number
     *
     * @param  {String} space_id
     * @param  {Number} ticket_number
     * @return {Promise<Object>}
     */
    async getTicket(space_id, ticket_number) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/${ticket_number}`,
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );
        const {
            data,
        } = response;
        return data;
    }


    /**
     * @param {*} space_id
     * @param {*} ticket_number
     * @returns {Promise}
     */
    async getTicketComments(space_id, ticket_number) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/${ticket_number}/ticket_comments`,
            {
                params: {
                    per_page: 100,
                },
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }


    /**
     * PUT /v1/spaces/:space_id/tickets/:number
     *
     * @param  {String} space_id
     * @param  {Number} ticket_number
     * @param  {Object} data
     * @return {Promise<Object>}
     */
    updateTicket(space_id, ticket_number, data) {
        return axios.put(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/${ticket_number}`,
            {
                ticket: data,

            },
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );
    }


    /**
     * [getAllMilestones description]
     * @param  {String} space_id
     * @param  {[type]} page       [description]
     * @param  {[type]} per_page   [description]
     * @param  {[type]} due_date_order    [description]
     * @return {Promise<Object>} Return promise of the Api result
     */
    async getAllMilestones(space_id, page, per_page, due_date_order) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/milestones/all.json`,
            {
                params: {
                    page,
                    per_page,
                    due_date_order,
                },
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );
        const {
            data,
        } = response;
        return data;
    }

    /**
     * @param  {String} space_id
     * @param  {Number} page
     * @param  {Number} per_page
     * @return {Promise<Object>}
     */
    async getTags(space_id, page, per_page) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tags.json`,
            {
                params: {
                    page,
                    per_page,
                },
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }

    /**
     * @param {String} space_id
     * @param {String} milestone_id
     * @param {String} ticket_status ('active', 'closed' and 'all'. By default, 'active' tickets are fetched)
     * @param {Number} page
     * @param {Number} per_page
     * @param {*} sort_order
     * @param {*} sort_by
     * @returns {Promise<Object>}
     */
    async getTicketsByMilestone(
        space_id,
        milestone_id,
        ticket_status,
        page,
        per_page,
        sort_order,
        sort_by
    ) {

        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/milestone/${milestone_id}.json`,
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
                params: {
                    ticket_status,
                    page,
                    per_page,
                    sort_order,
                    sort_by,
                },
            }
        );

        const {
            data,
        } = response;

        return data;
    }

    /**
     * @param {String} space_id
     * @param {Number} ticket_number
     * @returns {Promise<Object>}
     */
    async getTagsByTicketNumber(space_id, ticket_number) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/${ticket_number}/tags.json`,
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;

        return data;
    }


    /**
     * Returns all available statuses
     * @param {String} space_id
     * @returns {Promise}
     */
    async getTicketStatusesBySpaceId(space_id) {
        const response = await axios.get(
            `${AssemblaRepository.ASSEMBLA_URL}/v1/spaces/${space_id}/tickets/statuses.json`,
            {
                headers: AssemblaRepository.REQUEST_HEADERS,
            }
        );

        const {
            data,
        } = response;
        return data;
    }

}

AssemblaRepository.instance = null;

module.exports = {
    AssemblaRepository,
};
