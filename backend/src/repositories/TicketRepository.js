'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class TicketRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {TicketRepository}
     */
    static getInstance() {
        if (TicketRepository.instance === null) {
            TicketRepository.instance = new TicketRepository();
        }

        return TicketRepository.instance;
    }


    /**
     * @return {TicketRepository}
     */
    constructor() {
        super();
        this.collection_name = 'tickets';

        this.schema = {
            milestone_id: 'ObjectId',
            space_id: 'ObjectId',
            status_id: 'ObjectId',
            assigned_to_user_id: 'ObjectId|null',
            created_by_user_id: 'ObjectId|null',
            merge_request_id_list: 'Array<ObjectId>',
            updated_at: 'Date',
            created_on: 'Date',
        };
    }

    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            id,
            id_list,
            status_id,
            status_id_list,
            space_id,
            assembla_id,
            milestone_id_list,
            number_list,
            assembla_assigned_to_id,
            assembla_reporter_id,
            assigned_to_user_id,
        } = input;

        const query = {};

        if (id) {
            query._id = {
                $eq: this.stringToObjectId(id),
            };
        }

        if (id_list) {
            query._id = {
                $in: this.stringListToObjectIdList(id_list),
            };
        }

        if (space_id) {
            query.space_id = {
                $eq: this.stringToObjectId(space_id),
            };
        }

        if (status_id) {
            query.status_id = {
                $eq: this.stringToObjectId(status_id),
            };
        }

        if (status_id_list) {
            query.status_id = {
                $in: this.stringListToObjectIdList(status_id_list),
            };
        }

        if (assembla_id) {
            query['assembla.id'] = {
                $eq: assembla_id,
            };
        }

        if (assembla_assigned_to_id) {
            query['assembla.assigned_to_id'] = {
                $eq: assembla_assigned_to_id,
            };
        }

        if (assembla_reporter_id) {
            query['assembla.reporter_id'] = {
                $eq: assembla_reporter_id,
            };
        }

        if (milestone_id_list) {
            query.milestone_id = {
                $in: this.stringListToObjectIdList(milestone_id_list),
            };
        }

        if (number_list) {
            query.number = {
                $in: number_list,
            };
        }

        if (assigned_to_user_id) {
            query.assigned_to_user_id = {
                $eq: this.stringToObjectId(assigned_to_user_id),
            };
        }

        return query;
    }

}

TicketRepository.instance = null;

module.exports = {
    TicketRepository,
};
