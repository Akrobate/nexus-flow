'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class MilestoneRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {MilestoneRepository}
     */
    static getInstance() {
        if (MilestoneRepository.instance === null) {
            MilestoneRepository.instance = new MilestoneRepository();
        }

        return MilestoneRepository.instance;
    }


    /**
     * @return {MilestoneRepository}
     */
    constructor() {
        super();
        this.collection_name = 'milestones';
        this.schema = {
            // user_id: 'ObjectId',
            // created_by_user_id: 'ObjectId',
            // updated_by_user_id: 'ObjectId',
            space_id: 'ObjectId',
        };
    }

    /**
     * @param {Object} input
     * @returns {Object}
    */
    formatSearchCriteria(input) {
        const {
            space_id,
            assembla_id,
        } = input;

        const query = {};

        if (space_id) {
            query.space_id = {
                $eq: this.stringToObjectId(space_id),
            };
        }

        if (assembla_id) {
            query['assembla.id'] = {
                $eq: assembla_id,
            };
        }

        return query;
    }

}

MilestoneRepository.instance = null;

module.exports = {
    MilestoneRepository,
};
