'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class OrganizationRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {OrganizationRepository}
     */
    static getInstance() {
        if (OrganizationRepository.instance === null) {
            OrganizationRepository.instance = new OrganizationRepository();
        }

        return OrganizationRepository.instance;
    }


    /**
     * @return {OrganizationRepository}
     */
    constructor() {
        super();
        this.collection_name = 'organizations';
    }

}

OrganizationRepository.instance = null;

module.exports = {
    OrganizationRepository,
};
