'use strict';

const {
    AbstractRepository,
} = require('./AbstractRepository');


class SpaceAccessProfileRepository extends AbstractRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {SpaceAccessProfileRepository}
     */
    static getInstance() {
        if (SpaceAccessProfileRepository.instance === null) {
            SpaceAccessProfileRepository.instance = new SpaceAccessProfileRepository();
        }

        return SpaceAccessProfileRepository.instance;
    }


    /**
     * @return {SpaceAccessProfileRepository}
     */
    constructor() {
        super();
        this.collection_name = 'space_access_profiles';
    }


    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatSearchCriteria(input) {
        const {
            user_id_list,
            space_id_list,
            name,
        } = input;

        const query = {};

        if (name) {
            query.name = {
                $eq: name,
            };
        }

        // Not tested
        if (user_id_list) {
            query.user_id = {
                $in: this.stringListToObjectIdList(user_id_list),
            };
        }

        // not tested
        if (space_id_list) {
            query.space_id = {
                $in: this.stringListToObjectIdList(space_id_list),
            };
        }

        return query;
    }
}

SpaceAccessProfileRepository.instance = null;

module.exports = {
    SpaceAccessProfileRepository,
};
