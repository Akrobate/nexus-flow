'use strict';

const {
    CustomError,
} = require('../CustomError');

const {
    MongoDbRepository,
} = require('./MongoDbRepository');

class AbstractRepository {

    /**
     * @returns {Repository}
     */
    constructor() {
        this.mongo_db_repository = MongoDbRepository.getInstance();
        this.schema = {};
    }


    /**
     * @param {Object} input
     * @returns {Promise}
     */
    async create(input) {
        const formated_input = this.formatCreateUpdateInput(input);
        const result = await this.mongo_db_repository
            .insertDocument(this.collection_name, formated_input);
        const {
            insertedId,
        } = result;
        return this.mongo_db_repository.formatOutput({
            _id: insertedId,
            ...input,
        });
    }


    /**
     * @param {String} id
     * @return {Promise<Object>}
     */
    async read(id) {
        const result = await this.mongo_db_repository.findDocument(this.collection_name, {
            _id: id,
        });
        if (result === null) {
            throw new CustomError(CustomError.NOT_FOUND, 'Not found');
        }
        return this.mongo_db_repository.formatOutput(result);

    }


    /**
     * @param {String} id
     * @param {Object} input
     * @returns {Promise}
     */
    async update(id, input) {
        const formated_input = this.formatCreateUpdateInput(input);
        const result = await this.mongo_db_repository
            .updateDocument(
                this.collection_name,
                this.mongo_db_repository.formatIdCriteria({
                    id,
                }),
                formated_input
            );
        const {
            matchedCount,
        } = result;

        if (matchedCount === 0) {
            throw new CustomError(CustomError.NOT_FOUND, 'Document to update not found');
        }

        return this.read(id);
    }


    /**
     * @param {String} id
     * @returns {Promise}
     */
    async delete(id) {
        const result = await this.mongo_db_repository
            .deleteDocument(
                this.collection_name,
                this.mongo_db_repository.formatIdCriteria({
                    id,
                })
            );
        return result;
    }


    /**
     * @async
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    async search(input = {}) {

        const {
            limit,
            offset,
            sort,
        } = input;

        const query = this.formatSearchCriteria(input);

        const aggregation = [
            {
                $match: query,
            },
        ];

        const formatted_sort_object = this.formatSort(sort);
        if (formatted_sort_object) {
            aggregation.push({
                $sort: formatted_sort_object,
            });
        }

        if (offset) {
            aggregation.push({
                $skip: offset,
            });
        }

        if (limit) {
            aggregation.push({
                $limit: limit,
            });
        }

        let document_list = [];

        document_list = await this.mongo_db_repository.aggregate(
            this.collection_name,
            aggregation
        );

        return document_list.map((item) => this.mongo_db_repository.formatOutput(item));
    }


    /**
     * @async
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    async count(input = {}) {
        const query = this.formatSearchCriteria(input);

        const count = await this.mongo_db_repository.countDocuments(
            this.collection_name,
            query
        );

        return count;
    }


    /**
     * @returns {Promise}
     */
    removeAll() {
        return this.mongo_db_repository.deleteAllDocuments(this.collection_name);
    }


    /**
     * @param {Object} document
     * @returns {Object}
     */
    formatDocumentId(document) {
        if (document && document._id) {
            document.id = document._id;
            delete document._id;
        }
        return document;
    }


    /**
     * @param {Array<String>} sort
     * @return {Object|null}
     */
    formatSort(sort) {
        if (!sort) {
            return null;
        }
        const sort_object = {};
        sort.forEach((sort_param) => {
            let sort_key = sort_param;
            if (sort_param.indexOf('.') !== -1) {
                sort_key = `${sort_param.split('.')[0]}_data.${sort_param.split('.')[1]}`;
            }
            if (sort_key.indexOf('-') === -1) {
                sort_object[sort_key.replace('+', '')] = 1;
            } else {
                sort_object[sort_key.replace('-', '')] = -1;
            }
        });
        return sort_object;
    }


    /**
     * @param {String} input
     * @returns {Object}
     */
    stringToObjectId(input) {
        return this.mongo_db_repository.stringToObjectId(input);
    }

    /**
     * @param {Array} input
     * @returns {Array}
     */
    stringListToObjectIdList(input) {
        return this.mongo_db_repository.stringListToObjectIdList(input);
    }


    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatCreateUpdateInput(input) {

        const _input = {
            ...input,
        };
        const response = {};

        if (_input.id) {
            response._id = this.stringToObjectId(_input.id);
            delete _input.id;
        }

        Object.keys(_input).forEach((key) => {
            switch (this.schema[key]) {
                case 'ObjectId|null':
                    if (_input[key] === null) {
                        response[key] = null;
                    } else {
                        response[key] = this.stringToObjectId(_input[key]);
                    }
                    break;
                case 'Array<ObjectId>':
                    response[key] = _input[key]
                        .map((item) => this.stringToObjectId(item));
                    break;
                case 'ObjectId':
                    response[key] = this.stringToObjectId(_input[key]);
                    break;
                case 'Date':
                    response[key] = new Date(_input[key]);
                    break;
                default:
                    response[key] = _input[key];
            }
        });
        return response;
    }
}

module.exports = {
    AbstractRepository,
};
