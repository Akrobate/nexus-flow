'use strict';

const {
    MongoClient,
    ObjectId,
} = require('mongodb');

const {
    configuration,
} = require('../configuration');

class MongoDbRepository {

    /**
     * connection pattern
     * 'mongodb://'+DATABASEUSERNAME+':'+DATABASEPASSWORD+'@'+DATABASEHOST+':'DATABASEPORT+'/
     *
     * @static
     * @returns {String}
     */
    static get CONNECTION_CREDENTIAL() {
        const {
            username,
            password,
            host,
            port,
            database_name,
        } = configuration.storage.mongodb;
        return `mongodb://${username}:${password}@${host}:${port}/${database_name}?authSource=admin`;
    }


    /**
     * @returns {String}
     */
    static get DATABASE_NAME() {
        const {
            database_name,
        } = configuration.storage.mongodb;
        return database_name;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {MongoDbRepository}
     */
    static getInstance() {
        if (MongoDbRepository.instance === null) {
            MongoDbRepository.instance = new MongoDbRepository(
                MongoClient
            );
        }
        return MongoDbRepository.instance;
    }


    /**
     * @param {MongoClient} mongodb_client
     */
    constructor(
        mongodb_client
    ) {
        this.mongodb_client = mongodb_client;
        this.mongodb_connection_handler = null;
        this.mongodb_connection_database_handler = {};
    }


    /**
     * @param {Object} database
     * @returns {Promise}
     */
    async getDatabaseConnection(database) {
        await this.getConnection();
        return this.useDataBase(database);
    }


    /**
     * Connection handler
     *
     * @returns {Promise<Object|error>}
     */
    async getConnection() {
        if (this.mongodb_connection_handler === null) {
            this.mongodb_connection_handler = await MongoClient
                .connect(MongoDbRepository.CONNECTION_CREDENTIAL, {});
            return this.mongodb_connection_handler;
        }
        return this.mongodb_connection_handler;
    }


    /**
     * Close the connection to mongodb
     * Must be called, if not script will not exit
     *
     * @returns {Promse<Object|error>}
     */
    async closeConnection() {
        let result = null;
        if (this.mongodb_connection_handler !== null) {
            this.mongodb_connection_database_handler[MongoDbRepository.DATABASE_NAME] = undefined;
            result = await this.mongodb_connection_handler.close();
        }
        this.mongodb_connection_handler = null;
        return result;
    }


    /**
     * Selects the database to use
     *
     * @param {String} database_name
     * @returns {Object}
     */
    async useDataBase(database_name) {
        if (this.mongodb_connection_database_handler[database_name] === undefined) {
            this.mongodb_connection_database_handler[database_name] = await this
                .mongodb_connection_handler.db(database_name);
        }
        return this.mongodb_connection_database_handler[database_name];
    }


    /**
     * Creates a new collection if not exists in database
     *
     * @param {String} collection_name
     * @returns {Promise<Object|error>}
     */
    async createCollectionIfNotExists(collection_name) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        const collection_list = await dbo.collections();
        if (!collection_list.map((colection) => colection.s.namespace.collection)
            .includes(collection_name)
        ) {
            return this.createCollection(collection_name);
        }
        return true;
    }


    /**
     * List all collection mongodb objects
     * @returns {Promise<Object|error>}
     */
    async listAllCollections() {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collections();
    }

    /**
     * List all collections names
     * @returns {Promise<Object|error>}
     */
    async listAllCollectionNames() {
        const collection_object_list = await this.listAllCollections();
        return collection_object_list
            .map((collection) => collection.s.namespace.collection);
    }


    /**
     * Create a collection on default database with collection_name
     * @param {String} collection_name
     * @returns {Promise<Object|error>}
     */
    async createCollection(collection_name) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.createCollection(collection_name);
    }


    /**
     * Check collection exists
     * @param {String} collection_name
     * @returns {Promise<Object|error>}
     */
    async collectionExists(collection_name) {
        const collection_list = await this.listAllCollectionNames();
        return collection_list.includes(collection_name);
    }


    /**
     * Create a collection on default database with collection_name
     * @param {String} collection_name
     * @returns {Promise<Object|error>}
     */
    async dropCollectionIfExists(collection_name) {
        const collection_exists = await this.collectionExists(collection_name);
        if (collection_exists) {
            const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
            return dbo.collection(collection_name).drop();
        }
        return false;
    }


    /**
     * Create a collection on default database with collection_name
     * @param {String} collection_name
     * @returns {Promise<Object|error>}
     */
    async dropCollection(collection_name) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name).drop();
    }


    /**
     * Drop database
     *
     *
     * @param {String} database_name
     * @returns {Promise<Object|error>}
     */
    async dropDatabase(database_name) {
        const dbo = await this.getDatabaseConnection(database_name);
        return dbo.dropDatabase();
    }


    /**
     * Insert multiple documents
     * @param {String} collection_name
     * @param {String} document_list
     * @returns {Promise<Object|error>}
     */
    async insertDocumentList(collection_name, document_list) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo
            .collection(collection_name)
            .insertMany(document_list);
    }


    /**
     * Insert single document
     * @param {String} collection_name
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async insertDocument(collection_name, document) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo
            .collection(collection_name)
            .insertOne(document);
    }


    /**
     * Returns found document
     *
     * @param {String} collection_name
     * @param {Object} query
     * @param {Object} fields_selection
     * @returns {Promise<Object|error>}
     */
    async findDocument(collection_name, query) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .findOne(this.formatIdCriteria(query));
    }


    /**
     * Returns a list of found documents
     *
     * @param {String} collection_name
     * @param {Object} query
     * @param {Object} sort
     * @param {Number} limit
     * @param {Number} offset
     * @param {Object} fields_selection
     * @returns {Promise<Object|error>}
     */
    async findDocumentList(collection_name, query, sort, limit, offset, fields_selection) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .find(query, fields_selection)
            .sort(sort === undefined ? {} : sort)
            .skip(offset === undefined ? 0 : offset)
            .limit(limit === undefined ? 0 : limit)
            .toArray();
    }


    /**
     * Returns a list of found documents
     *
     * @async
     * @param {String} collection_name
     * @param {Object} aggregation
     * @returns {Promise<Object|error>}
     */
    async aggregate(collection_name, aggregation) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .aggregate(
                aggregation,
                {
                    allowDiskUse: true,
                }
            )
            .toArray();
    }


    /**
     * Returns a list of found documents
     *
     * @param {String} collection_name
     * @param {Object} query
     * @returns {Promise<Object|error>}
     */
    async countDocuments(collection_name, query) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .countDocuments(query);
    }

    /**
     * Returns a list of found documents
     *
     * @param {String} collection_name
     * @param {Object} query
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async updateDocument(collection_name, query, document) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .updateOne(
                query,
                {
                    $set: document,
                }
            );
    }


    /**
     * @param {String} collection_name
     * @param {Object} query
     * @returns {Promise<Object|error>}
     */
    async deleteDocument(collection_name, query) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .deleteOne(
                query
            );
    }


    /**
     * @param {String} collection_name
     * @returns {Promise<Object|error>}
     */
    async deleteAllDocuments(collection_name) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .deleteMany({});
    }

    /**
     * Updates all found documents
     *
     * @param {String} collection_name
     * @param {Object} query
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async updateDocuments(collection_name, query, document) {
        const dbo = await this.getDatabaseConnection(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .updateMany(
                query,
                {
                    $set: document,
                }
            );
    }


    /**
     * @param {Object} criteria
     * @return {Object}
     */
    formatIdCriteria(criteria) {

        const _criteria = {
            ...criteria,
        };

        if (_criteria.id && !_criteria._id) {
            _criteria._id = _criteria.id;
            delete _criteria.id;
        }

        if (_criteria._id !== undefined && typeof _criteria._id === 'string') {
            return {
                ..._criteria,
                _id: new ObjectId(`${_criteria._id}`),
            };
        }
        return _criteria;
    }


    /**
     * @param {Object} input
     * @returns {Object}
     */
    formatOutput(input) {
        const response = {};
        Object.keys(input).forEach((key) => {
            if (key === '_id') {
                response.id = input._id.toString();
            } else if (input[key]?.constructor?.name === 'ObjectId') {
                response[key] = input[key].toString();
            } else {
                response[key] = input[key];
            }
        });
        return response;
    }


    /**
     * @param {String} input
     * @returns {Object}
     */
    stringToObjectId(input) {
        return new ObjectId(`${input}`);
    }

    /**
     * @param {Array} input
     * @returns {Array}
     */
    stringListToObjectIdList(input) {
        return input.map((item) => this.stringToObjectId(item));
    }

}

MongoDbRepository.instance = null;

module.exports = {
    MongoDbRepository,
};


