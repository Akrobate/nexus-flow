'use strict';

const {
    Router,
} = require('express');

const {
    ticketRoutes,
    ticketStatusRoutes,
} = require('./items');

const {
    UserController,
    MilestoneController,
    SpaceController,
    TicketTagController,
    TeammateController,
    SpaceAccessProfileController,
    SpaceUserAccessController,
    MergeRequestController,
} = require('../controllers');

const {
    auth,
    routeCallback,
} = require('./common');

const user_controller = UserController.getInstance();
const milestone_controller = MilestoneController.getInstance();
const space_controller = SpaceController.getInstance();
const ticket_tag_controller = TicketTagController.getInstance();
const teammate_controller = TeammateController.getInstance();
const space_access_profile_controller = SpaceAccessProfileController.getInstance();
const space_user_access_controller = SpaceUserAccessController.getInstance();
const merge_request_controller = MergeRequestController.getInstance();

const url_prefix = '/api/v1';

function initServerRoutes(server) {

    const router = new Router();

    // Users
    router.post('/login', routeCallback(user_controller, 'login'));
    router.post('/register', routeCallback(user_controller, 'register'));

    // SpaceAccessProfile
    router.get('/space-access-profiles', auth(), routeCallback(space_access_profile_controller, 'search'));

    // SpaceUserAccess
    router.post('/space-user-accesses', auth(), routeCallback(space_user_access_controller, 'create'));
    router.patch('/space-user-accesses/:id', auth(), routeCallback(space_user_access_controller, 'update'));
    router.delete('/space-user-accesses/:id', auth(), routeCallback(space_user_access_controller, 'delete'));

    // Teammate
    router.get('/teammates', routeCallback(teammate_controller, 'search'));

    // Tags
    router.get('/ticket-tags', auth(), routeCallback(ticket_tag_controller, 'search'));

    // Milestones
    router.get('/milestones', auth(), routeCallback(milestone_controller, 'search'));

    // Spaces
    router.get('/spaces', auth(), routeCallback(space_controller, 'search'));

    // MergeRequest
    router.get('/merge-request-statistics', auth(), routeCallback(merge_request_controller, 'searchStatistics'));

    server.use(url_prefix, ticketRoutes);
    server.use(url_prefix, ticketStatusRoutes);
    server.use(url_prefix, router);
}

module.exports = initServerRoutes;
