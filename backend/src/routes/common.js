'use strict';

const {
    AuthenticationMiddleware,
} = require('../middlewares/AuthenticationMiddleware');

const authentication_middleware = AuthenticationMiddleware.getInstance();

function auth() {
    return authentication_middleware.injectJwtData();
}

const routeCallback = (controller, controller_method) => (
    request, response, next) => controller[controller_method](request, response).catch(next);

module.exports = {
    auth,
    routeCallback,
};
