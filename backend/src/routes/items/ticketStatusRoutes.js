'use strict';

const {
    Router,
} = require('express');

const {
    TicketStatusController,
} = require('../../controllers');

const {
    auth,
    routeCallback,
} = require('../common');

const ticket_status_controller = TicketStatusController.getInstance();

const router = new Router();

// TicketStatus
router.get('/ticket-statuses', auth(), routeCallback(ticket_status_controller, 'search'));
router.get('/ticket-statuses/:id', auth(), routeCallback(ticket_status_controller, 'read'));
router.post('/ticket-statuses', auth(), routeCallback(ticket_status_controller, 'create'));
router.patch('/ticket-statuses/:id', auth(), routeCallback(ticket_status_controller, 'update'));

module.exports = {
    ticketStatusRoutes: router,
};
