'use strict';

const {
    Router,
} = require('express');

const {
    TicketController,
} = require('../../controllers');

const {
    auth,
    routeCallback,
} = require('../common');

const ticket_controller = TicketController.getInstance();

const router = new Router();

// Tickets
router.post('/tickets', auth(), routeCallback(ticket_controller, 'create'));
router.get('/tickets', auth(), routeCallback(ticket_controller, 'search'));
router.get('/tickets/:id', auth(), routeCallback(ticket_controller, 'read'));
router.patch('/tickets/:id', auth(), routeCallback(ticket_controller, 'update'));
router.delete('/tickets/:id', auth(), routeCallback(ticket_controller, 'delete'));

module.exports = {
    ticketRoutes: router,
};
