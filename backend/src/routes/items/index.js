'use strict';

const {
    ticketRoutes,
} = require('./ticketRoutes');
const {
    ticketStatusRoutes,
} = require('./ticketStatusRoutes');

module.exports = {
    ticketStatusRoutes,
    ticketRoutes,
};
