'use strict';

const {
    AbstractService,
} = require('./AbstractService');

const {
    TicketRepository,
    SpaceRepository,
} = require('../repositories');

const {
    CustomError,
} = require('../CustomError');

const extractUrls = require('extract-urls');

const Url = require('url-parse');

class TicketService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {TicketService}
     */
    static getInstance() {
        if (TicketService.instance === null) {
            TicketService.instance = new TicketService(
                TicketRepository.getInstance(),
                SpaceRepository.getInstance()
            );
        }
        return TicketService.instance;
    }


    /**
     * @param {TicketRepository} ticket_repository
     * @param {SpaceRepository} space_repository
     */
    constructor(
        ticket_repository,
        space_repository
    ) {
        super(ticket_repository);
        this.space_repository = space_repository;
        this.ticket_repository = ticket_repository;
        this.entity = 'ticket';
    }


    /**
     * @param {String} id
     * @param {Object} input
     * @returns {Object}
     */
    repositoryUpdate(id, input) {
        return this.repository.update(id, input);
    }


    /**
     * @param {Object} input
     * @returns {Object}
     */
    repositoryCreate(input) {
        return this.repository.create(input);
    }


    /**
     * Detect links in tickets
     * @returns {void}
     */
    async detectTicketsLinks() {
        const ticket_list = await this.repository.search();
        const space_list = await this.space_repository.search();

        for (const ticket of ticket_list) {
            const ticket_link_list = this.detectTicketsInDescription(ticket.description);
            for (const ticket_link of ticket_link_list) {
                await this.addLinkPointingBy(ticket, ticket_link, space_list);
            }
            await this.ticket_repository.update(ticket.id,
                {
                    ticket_link_list: ticket_link_list.map((ticket_link) => ({
                        ...ticket_link,
                        relation: 'pointing_to',
                    })),
                }
            );
        }

    }


    /**
     * @description ticket should be added as ticket pointed_by to ticket_link ticket (bidirectional pointing)
     * @param {Object} ticket
     * @param {Object} ticket_link
     * @param {Array} space_list
     * @returns {Promise<void>}
     */
    async addLinkPointingBy(ticket, ticket_link, space_list) {

        const ticket_link_space_id = space_list
            .find((space) => space.space_tag === ticket_link.space_tag).id;

        const pointed_ticket_list = await this.ticket_repository.search({
            number_list: [ticket_link.ticket_number],
            space_id: ticket_link_space_id,
        });
        if (pointed_ticket_list.length === 0) {
            throw new CustomError(CustomError.ORM_ERROR, 'ticket_link not found');
        }
        const [
            pointed_ticket,
        ] = pointed_ticket_list;

        const ticket_link_object = await this.buildTicketLinkObject(ticket);

        if (pointed_ticket.ticket_link_list === undefined) {
            pointed_ticket.ticket_link_list = [];
        }

        const link_exists = pointed_ticket.ticket_link_list
            .findIndex((item) => item.ticket_number === ticket_link_object.ticket_number) !== -1;

        if (!link_exists) {

            await this.ticket_repository.update(
                pointed_ticket.id,
                {
                    ticket_link_list: [
                        ...pointed_ticket.ticket_link_list,
                        {
                            ...ticket_link_object,
                            relation: 'pointed_by',
                        },
                    ],
                }
            );
        }

        return null;

    }


    /**
     * @param {String} description
     * @return {Array<String>}
     */
    detectTicketsInDescription(description) {
        const result = [];
        const url_list = extractUrls(description ? description : '');
        if (url_list === undefined) {
            return result;
        }
        url_list.forEach((url_string) => {
            const url_object = new Url(url_string, true);
            const path_name = url_object.pathname;

            let space_tag = '';
            let ticket_number = '';
            const space_match = path_name.match(/\/spaces\/(.*)\/tickets\//);
            // eslint-disable-next-line no-useless-escape
            const ticket_match = path_name.match(/\/spaces\/.*\/tickets\/([0-9]*)[\/|-]/);
            if (space_match !== null && space_match !== undefined && space_match.length > 1) {
                [
                    ,
                    space_tag,
                ] = space_match;
            } else {
                return;
            }
            if (ticket_match !== null && ticket_match !== undefined && ticket_match.length > 1) {
                [
                    ,
                    ticket_number,
                ] = ticket_match;
            } else if (url_object.query && url_object.query.ticket) {
                ticket_number = url_object.query.ticket;
            } else {
                return;
            }
            ticket_number = parseInt(ticket_number, 10);
            if (isNaN(ticket_number)) {
                return;
            }

            if (result.find((element) => element.ticket_number === ticket_number) !== undefined) {
                return;
            }

            result.push({
                space_tag,
                ticket_number,
                url_string,
            });
        });

        return result;
    }


    /**
     * @param {Object} ticket
     * @returns {Object}
     */
    async buildTicketLinkObject(ticket) {
        const {
            number,
            space_id,
        } = ticket;

        const [
            space,
        ] = await this.space_repository.search({
            id: space_id,
        });

        if (space === undefined) {
            throw new CustomError(CustomError.ORM_ERROR, 'Space not found');
        }

        const {
            space_tag,
        } = space;

        const url_string = `https://polarity.assembla.com/spaces/${space_tag}/tickets/realtime_cardwall?ticket=${number}`;

        return {
            ticket_id: ticket.id,
            ticket_number: number,
            space_tag,
            url_string,
        };
    }


    /**
     * @returns {void}
     */
    async resetAllTicketsLinks() {
        const ticket_list = await this.repository.search();
        for (const ticket of ticket_list) {
            await this.repository.update(
                ticket.id,
                {
                    ticket_link_list: [],
                }
            );
        }
    }

}

TicketService.instance = null;

module.exports = {
    TicketService,
};
