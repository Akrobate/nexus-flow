'use strict';

const {
    AbstractService,
} = require('./AbstractService');
const {
    TicketService,
} = require('./TicketService');
const {
    TicketStatusService,
} = require('./TicketStatusService');
const {
    SpaceService,
} = require('./SpaceService');
const {
    MilestoneService,
} = require('./MilestoneService');
const {
    UserService,
} = require('./UserService');
const {
    TicketTagService,
} = require('./TicketTagService');
const {
    TeammateService,
} = require('./TeammateService');
const {
    SpaceAccessProfileService,
} = require('./SpaceAccessProfileService');
const {
    SpaceUserAccessService,
} = require('./SpaceUserAccessService');
const {
    GitlabService,
} = require('./GitlabService');
const {
    MergeRequestService,
} = require('./MergeRequestService');

module.exports = {
    AbstractService,
    UserService,
    SpaceService,
    TicketService,
    TicketStatusService,
    MilestoneService,
    TicketTagService,
    TeammateService,
    SpaceAccessProfileService,
    SpaceUserAccessService,
    GitlabService,
    MergeRequestService,
};
