'use strict';

const {
    AbstractService,
} = require('./AbstractService');

const {
    TicketTagRepository,
} = require('../repositories');

class TicketTagService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {TicketTagService}
     */
    static getInstance() {
        if (TicketTagService.instance === null) {
            TicketTagService.instance = new TicketTagService(
                TicketTagRepository.getInstance()
            );
        }
        return TicketTagService.instance;
    }


    /**
     * @param {TicketStatusRepository} repository
     */
    constructor(
        repository
    ) {
        super(repository);
        this.entity = 'ticket_tag';
    }

}

TicketTagService.instance = null;

module.exports = {
    TicketTagService,
};
