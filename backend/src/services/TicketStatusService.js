'use strict';

const {
    logger,
} = require('../logger');

const {
    AbstractService,
} = require('./AbstractService');

const {
    TicketStatusRepository,
    TicketRepository,
} = require('../repositories');

class TicketStatusService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {TicketStatusService}
     */
    static getInstance() {
        if (TicketStatusService.instance === null) {
            TicketStatusService.instance = new TicketStatusService(
                TicketStatusRepository.getInstance(),
                TicketRepository.getInstance()
            );
        }
        return TicketStatusService.instance;
    }


    /**
     * @param {TicketStatusRepository} repository
     * @param {TicketRepository} ticket_repository
     */
    constructor(
        repository,
        ticket_repository
    ) {
        super(repository);
        this.ticket_repository = ticket_repository;
        this.entity = 'ticket_status';
    }


    /**
     * @return {void}
     */
    async updateFlatDatesStatuses() {

        const status_list = await this.repository.search();
        const ticket_list = await this.ticket_repository.search();

        const ticket_list_count = ticket_list.length;

        for (const [index, ticket] of ticket_list.entries()) {

            /* istanbul ignore next */
            if (index % 100 === 0) {
                logger.log(`ticket ${index + 1} / ${ticket_list_count}`);
            }

            if (!ticket.comment_list) {
                // eslint-disable-next-line no-continue
                continue;
            }

            const status_comments = ticket.comment_list.filter((comment) => comment.ticket_changes.includes('---\n- - status\n'));

            const space_status_list = status_list
                .filter((status) => status.space_id === ticket.space_id);

            const status_dates_object = {};
            status_comments.forEach((comment) => {
                const result = [];
                space_status_list.forEach((status) => {
                    const _index = comment.ticket_changes.indexOf(status.name);
                    if (_index > -1) {
                        result.push({
                            status: status.name,
                            index: _index,
                            date: comment.created_on,
                        });
                    }
                });

                if (result.length >= 1) {
                    result.sort((a, b) => b.index - a.index);
                    if (status_dates_object[result[0].status] === undefined) {
                        status_dates_object[result[0].status] = new Date(result[0].date);
                    }
                }
            });

            const DEBUG = true;

            /* istanbul ignore next */
            if (DEBUG && status_comments.length > 0) {
                logger.log(status_comments.length);
            }

            /* istanbul ignore next */
            if (DEBUG && status_comments.length === 1) {
                logger.log(status_comments);
            }

            /* istanbul ignore next */
            if (!DEBUG) {
                await this.ticket_repository.update(ticket.id, {
                    status_dates: status_dates_object,
                });
            }
        }
    }

}

TicketStatusService.instance = null;

module.exports = {
    TicketStatusService,
};
