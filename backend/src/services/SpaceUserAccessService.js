'use strict';

const {
    AbstractService,
} = require('./AbstractService');

const {
    SpaceUserAccessRepository,
} = require('../repositories');

class SpaceUserAccessService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {SpaceUserAccessService}
     */
    static getInstance() {
        if (SpaceUserAccessService.instance === null) {
            SpaceUserAccessService.instance = new SpaceUserAccessService(
                SpaceUserAccessRepository.getInstance()
            );
        }
        return SpaceUserAccessService.instance;
    }


    /**
     * @param {SpaceUserAccessRepository} space_user_access_repository
     */
    constructor(
        space_user_access_repository
    ) {
        super(space_user_access_repository);
        this.entity = 'space_user_access';
    }

}

SpaceUserAccessService.instance = null;

module.exports = {
    SpaceUserAccessService,
};
