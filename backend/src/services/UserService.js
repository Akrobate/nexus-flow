'use strict';

const crypto = require('crypto');

const jwt = require('jsonwebtoken');

const {
    CustomError,
} = require('../CustomError');

const {
    UserRepository,
} = require('../repositories');

const {
    OrganizationRepository,
} = require('../repositories');

const {
    configuration,
} = require('../configuration');

class UserService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {UserService}
     */
    static getInstance() {
        if (UserService.instance === null) {
            UserService.instance = new UserService(
                UserRepository.getInstance(),
                OrganizationRepository.getInstance()
            );
        }
        return UserService.instance;
    }

    /**
     * @param {UserRepository} user_repository
     * @param {OrganizationRepository} organization_repository
     */
    constructor(
        user_repository,
        organization_repository
    ) {
        this.user_repository = user_repository;
        this.organization_repository = organization_repository;

        this.jwt_private_key = configuration.jwt.private_key;
        this.jwt_public_key = configuration.jwt.public_key;
        this.jwt_config = {
            algorithm: configuration.jwt.algorithm,
            expiresIn: configuration.jwt.default_token_duration,
        };

        this.jwt = jwt;
    }


    /**
     * @param {Object} input
     * @returns {Promise}
     */
    async register(input) {

        const {
            login,
            email,
            password,
            organization_name,
        } = input;

        const [
            existing_user,
        ] = await this.user_repository.search({
            email_list: [email],
        });

        if (existing_user) {
            throw new CustomError(CustomError.BAD_PARAMETER, 'Email already exists');
        }

        const organization = await this.organization_repository.create({
            name: organization_name,
        });

        const {
            id: organization_id,
        } = organization;

        const result = await this.user_repository.create({
            login,
            email,
            password: this.hashPassword(password),
            organization_id,
        });

        return result;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async login(input) {

        const {
            login,
            password,
        } = input;

        const [
            user,
        ] = await this.user_repository.search({
            login,
        });

        if (user === undefined) {
            throw new CustomError(CustomError.UNAUTHORIZED, 'Bad login or password');
        }

        if (user.password !== this.hashPassword(password)) {
            throw new CustomError(CustomError.UNAUTHORIZED, 'Bad login or password');
        }

        const jwt_user_data = {
            user_id: user.id,
            email: user.email,
            login: user.login,
        };

        const jwt_token = this.tryToSignJwt(jwt_user_data);

        return {
            token: jwt_token,
        };
    }


    /**
     * @param {String} password
     * @returns {String}
     */
    hashPassword(password) {
        return crypto
            .createHash('sha256')
            .update(`${configuration.security.salt}${password}`)
            .digest('base64');
    }


    /**
     * @param {Object} jwt_user_data
     * @return {Object|Error}
     */
    tryToSignJwt(jwt_user_data) {
        let jwt_token = null;
        try {
            jwt_token = this.jwt.sign(jwt_user_data, this.jwt_private_key, this.jwt_config);
        } catch (error) {
            throw new CustomError(CustomError.INTERNAL_ERROR, error.message);
        }
        return jwt_token;
    }

}

UserService.instance = null;

module.exports = {
    UserService,
};
