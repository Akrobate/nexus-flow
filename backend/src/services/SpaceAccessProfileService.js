'use strict';

const {
    AbstractService,
} = require('./AbstractService');

const {
    SpaceAccessProfileRepository,
} = require('../repositories');

class SpaceAccessProfileService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {SpaceAccessProfileService}
     */
    static getInstance() {
        if (SpaceAccessProfileService.instance === null) {
            SpaceAccessProfileService.instance = new SpaceAccessProfileService(
                SpaceAccessProfileRepository.getInstance()
            );
        }
        return SpaceAccessProfileService.instance;
    }


    /**
     * @param {SpaceAccessProfileRepository} space_access_profile_repository
     */
    constructor(
        space_access_profile_repository
    ) {
        super(space_access_profile_repository);
        this.entity = 'space_access_profile';
    }

}

SpaceAccessProfileService.instance = null;

module.exports = {
    SpaceAccessProfileService,
};
