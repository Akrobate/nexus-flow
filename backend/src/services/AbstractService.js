'use strict';

const {
    Acl,
} = require('./commons/Acl');

class AbstractService {

    /**
     * @param {Repository} repository
     */
    constructor(
        repository
    ) {
        this.repository = repository;
        this.acl = Acl.getInstance();
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Object}
     */
    async create(user, input) {
        const {
            user_id,
        } = user;

        const {
            space_id,
        } = input;

        await this.acl.checkUserCan(this.entity, 'create', space_id, user_id);
        return this.repositoryCreate(input);
    }


    /**
     * @param {Object} input
     * @returns {Object}
     */
    repositoryCreate(input) {
        return this.repository.create(input);
    }


    /**
     * @param {Object} user
     * @param {String} id
     * @returns {Object}
     */
    async read(user, id) {
        const {
            user_id,
        } = user;

        const data = await this.repository.read(id);

        const {
            space_id,
        } = data;

        await this.acl.checkUserCan(this.entity, 'read', space_id, user_id);

        return data;
    }

    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Object}
     */
    search(user, input) {
        return this.repository.search(input);
    }

    /**
     * @param {Object} user
     * @param {String} id
     * @param {Object} input
     * @returns {Object}
     */
    async update(user, id, input) {
        const {
            user_id,
        } = user;

        const data = await this.repository.read(id);

        const {
            space_id,
        } = data;

        await this.acl.checkUserCan(this.entity, 'update', space_id, user_id);
        return this.repositoryUpdate(id, input);
    }


    /**
     * @param {String} id
     * @param {Object} input
     * @returns {Object}
     */
    repositoryUpdate(id, input) {
        return this.repository.update(id, input);
    }

    /**
     * @param {Object} user
     * @param {String} id
     * @returns {Object}
     */
    async delete(user, id) {
        const {
            user_id,
        } = user;
        const data = await this.repository.read(id);
        const {
            space_id,
        } = data;

        await this.acl.checkUserCan(this.entity, 'delete', space_id, user_id);

        return this.repository.delete(id);
    }
}

module.exports = {
    AbstractService,
};
