'use strict';

const {
    AbstractService,
} = require('./AbstractService');

const {
    MilestoneRepository,
} = require('../repositories');

class MilestoneService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {MilestoneService}
     */
    static getInstance() {
        if (MilestoneService.instance === null) {
            MilestoneService.instance = new MilestoneService(
                MilestoneRepository.getInstance()
            );
        }
        return MilestoneService.instance;
    }


    /**
     * @param {MilestoneRepository} repository
     */
    constructor(
        repository
    ) {
        super(repository);
        this.entity = 'milestone';
    }

}

MilestoneService.instance = null;

module.exports = {
    MilestoneService,
};
