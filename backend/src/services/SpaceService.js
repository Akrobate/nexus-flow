'use strict';

const {
    AbstractService,
} = require('./AbstractService');

const {
    SpaceRepository,
} = require('../repositories');

class SpaceService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {SpaceService}
     */
    static getInstance() {
        if (SpaceService.instance === null) {
            SpaceService.instance = new SpaceService(
                SpaceRepository.getInstance()
            );
        }
        return SpaceService.instance;
    }


    /**
     * @param {SpaceRepository} repository
     */
    constructor(
        repository
    ) {
        super(repository);
        this.entity = 'space';
    }
}

SpaceService.instance = null;

module.exports = {
    SpaceService,
};
