const {
    SpaceRepository,
    SpaceUserAccessRepository,
    SpaceAccessProfileRepository,
    TicketRepository,
    UserRepository,
    OrganizationRepository,
} = require('../../repositories');

const {
    CustomError,
} = require('../../CustomError');

class Acl {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {Acl}
     */
    static getInstance() {
        if (Acl.instance === null) {
            Acl.instance = new Acl(
                OrganizationRepository.getInstance(),
                UserRepository.getInstance(),
                SpaceRepository.getInstance(),
                SpaceUserAccessRepository.getInstance(),
                SpaceAccessProfileRepository.getInstance(),
                TicketRepository.getInstance()
            );
        }
        return Acl.instance;
    }


    /**
     * Constructor.
     * @param {OrganizationRepository} organization_repository
     * @param {UserRepository} user_repository
     * @param {SpaceRepository} space_repository
     * @param {SpaceUserAccessRepository} space_user_access_repository
     * @param {SpaceAccessProfileRepository} space_access_profile_repository
     */
    constructor(
        organization_repository,
        user_repository,
        space_repository,
        space_user_access_repository,
        space_access_profile_repository

    ) {
        this.organization_repository = organization_repository;
        this.user_repository = user_repository;
        this.space_repository = space_repository;
        this.space_user_access_repository = space_user_access_repository;
        this.space_access_profile_repository = space_access_profile_repository;
    }


    /**
     *
     * @param {String} entity
     * @param {String} action
     * @param {String} space_id
     * @param {String} user_id
     * @throws {CustomError<FORBIDEN>}
     * @return {Boolean}
     */
    async checkUserCan(entity, action, space_id, user_id) {
        const space_user_access_list = await this.space_user_access_repository.search({
            user_id_list: [user_id],
            space_id_list: [space_id],
        });

        const [
            space_user_access,
        ] = space_user_access_list;

        if (!space_user_access) {
            throw new CustomError(CustomError.ACCESS_FORBIDDEN, `User cannot ${action} ${entity} in space_id: ${space_id}`);
        }

        const {
            access_profile_id,
        } = space_user_access;

        const access_profile = await this.space_access_profile_repository.read(access_profile_id);

        // Should override access @todo
        const {
            access_list,
        } = access_profile;

        const has_access = access_list.includes(`${entity}.${action}`);

        if (!has_access) {
            throw new CustomError(CustomError.ACCESS_FORBIDDEN, `User cannot ${action} ${entity} in space_id: ${space_id}`);
        }
        return has_access;

    }

}

Acl.instance = null;

module.exports = {
    Acl,
};
