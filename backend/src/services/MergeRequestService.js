'use strict';

const {
    logger,
} = require('../logger');

const {
    AbstractService,
} = require('./AbstractService');

const {
    GitlabService,
} = require('./GitlabService');


const {
    MergeRequestRepository,
    TicketRepository,
} = require('../repositories');

class MergeRequestService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {MergeRequestService}
     */
    static getInstance() {
        if (MergeRequestService.instance === null) {
            MergeRequestService.instance = new MergeRequestService(
                MergeRequestRepository.getInstance(),
                TicketRepository.getInstance(),
                GitlabService.getInstance()
            );
        }
        return MergeRequestService.instance;
    }


    /**
     * @param {MergeRequestRepository} repository
     * @param {TicketRepository} ticket_repository
     * @param {GitlabService} gitlab_service
     */
    constructor(
        repository,
        ticket_repository,
        gitlab_service
    ) {
        super(repository);
        this.entity = 'merge_request';

        this.ticket_repository = ticket_repository;
        this.gitlab_service = gitlab_service;
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise}
     */
    async mergeRequestStatistics(user, input) {

        const result_list = [];

        // const merge_request_list = await this.repository.search({
        //     created_on_lower_boundary: '2024-06-10T15:23:44.467Z',
        // });

        const merge_request_list = await this.repository.search(input);

        const repository_list = [...new Set(merge_request_list.map((item) => item.repository))];
        // const state_list = [...new Set(merge_request_list.map((item) => item.state))];

        repository_list.forEach((_repository) => {
            result_list.push({
                repository: _repository,
                open: merge_request_list.filter((item) => item.repository === _repository && item.state === 'opened').length,
                closed: merge_request_list.filter((item) => item.repository === _repository && item.state === 'closed').length,
                merged: merge_request_list.filter((item) => item.repository === _repository && item.state === 'merged').length,
                finished: merge_request_list.filter((item) => item.repository === _repository && (item.state === 'closed' || item.state === 'merged')).length,
            });
        });

        return result_list;

    }


    /**
     * @todo
     * Working on this function moving from script to service detectMergeRequestInTickets.js
     * @param {Boolean} debug
     * @returns {Promise<Object>}
     */
    async detectMergeRequestInTickets(debug = false) {

        const ticket_list = await this.ticket_repository.search();

        const ticket_list_count = ticket_list.length;
        let count = 0;

        for (const [index, ticket] of ticket_list.entries()) {

            /* istanbul ignore next */
            if (index % 100 === 0 && debug) {
                logger.log(`ticket ${index + 1} / ${ticket_list_count}`);
            }
            const content = `${ticket.description} ${ticket.comment_list ? ticket.comment_list.map((item) => (item.comment ? item.comment : '')).join(' ') : ''}`;

            const merge_requests = this.gitlab_service.detectMergeRequestUrl(content);
            count += merge_requests.length;

            const merge_request_id_list = [];
            const merge_request_data_list = [];

            for (const merge_request of merge_requests) {

                const found_merge_request_list = await this.repository.search({
                    web_url: this.gitlab_service.normalizeMergeRequestUrl(merge_request.web_url),
                });

                const [
                    found_merge_request,
                ] = found_merge_request_list;

                if (found_merge_request) {
                    merge_request_id_list.push(found_merge_request.id);
                    const merge_request_data = {
                        id: found_merge_request.id,
                        project: this.gitlab_service.extractProjectName(merge_request.web_url),
                        state: found_merge_request.state,
                        web_url: this.gitlab_service
                            .normalizeMergeRequestUrl(merge_request.web_url),
                        created_on: found_merge_request.created_on,
                        updated_at: found_merge_request.updated_at,

                    };
                    merge_request_data_list.push(merge_request_data);
                }
            }
            await this.ticket_repository.update(ticket.id,
                {
                    merge_request_id_list,
                    merge_request_data_list,
                }
            );
        }

        return {
            total_merge_requests_detected: count,
        };
    }
}

MergeRequestService.instance = null;

module.exports = {
    MergeRequestService,
};
