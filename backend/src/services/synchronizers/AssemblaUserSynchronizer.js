/* istanbul ignore file */

'use strict';

const {
    UserRepository,
    SpaceRepository,
    TicketRepository,
    SpaceUserAccessRepository,
    SpaceAccessProfileRepository,
    AssemblaRepository,
    MongoDbRepository,
} = require('../../repositories');

class AssemblaUserSynchronizer {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaUserSynchronizer}
     */
    static getInstance() {
        if (AssemblaUserSynchronizer.instance === null) {
            AssemblaUserSynchronizer.instance = new AssemblaUserSynchronizer(
                AssemblaRepository.getInstance(),
                UserRepository.getInstance(),
                SpaceUserAccessRepository.getInstance(),
                SpaceAccessProfileRepository.getInstance(),
                SpaceRepository.getInstance(),
                TicketRepository.getInstance()
            );
        }
        return AssemblaUserSynchronizer.instance;
    }


    /**
     * @param {AssemblaRepository} assembla_repository
     * @param {UserRepository} user_repository
     * @param {SpaceUserAccessRepository} space_user_access_repository
     * @param {SpaceAccessProfileRepository} space_access_profile_repository
     * @param {SpaceRepository} space_repository
     * @param {TicketRepository} ticket_repository
     */
    constructor(
        assembla_repository,
        user_repository,
        space_user_access_repository,
        space_access_profile_repository,
        space_repository,
        ticket_repository
    ) {
        this.assembla_repository = assembla_repository;
        this.user_repository = user_repository;
        this.space_user_access_repository = space_user_access_repository;
        this.space_repository = space_repository;
        this.space_access_profile_repository = space_access_profile_repository;
        this.ticket_repository = ticket_repository;

        this.space_list = [];
    }


    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    async process(space_id) {
        this.space_list = await this.space_repository.search();
        this.profile_list = await this.space_access_profile_repository.search({
            name: 'Space member',
        });

        const assembla_user_list = await this.assembla_repository.getUsersBySpaceId(space_id);

        for (const user of assembla_user_list) {
            console.log(`Processing User Sync ${user.login}`);
            await this.upsertUser({
                ...user,
                space_id,
            });
        }
        setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);
    }


    /**
     * @param {*} data
     * @returns {Promise}
     */
    async upsertUser(data) {
        const found = await this.user_repository.search({
            assembla_id: data.id,
        });


        const space = this.space_list.find((item) => item?.assembla?.id === data.space_id);

        const avatar_url = await this.getAvatarUrl(data.id);


        const input = {
            name: data.name,
            organization_manager: false,
            email: data.email,
            login: data.login,
            password: 'rfTwjsiyzh/WnNVOUB1HlEOKewKUvP1/wfpyWbQHjHY=',
            avatar_url,
            assembla: {
                ...data,
            },
        };

        let user_id = null;
        if (found.length === 1) {
            user_id = (await this.user_repository.update(found[0].id, input)).id;
        } else {
            user_id = (await this.user_repository.create(input)).id;
        }

        // console.log('user_id', user_id);
        const space_user_access_list = await this.space_user_access_repository.search({
            user_id,
        });

        if (space_user_access_list.length === 0) {

            const [
                member_profile,
            ] = this.profile_list;

            const payload = {
                user_id,
                access_profile_id: member_profile.id,
                space_id: data.space_id,
            };
            console.log(payload);

            await this.space_user_access_repository.create({
                user_id,
                access_profile_id: member_profile.id,
                space_id: space.id,
            });
        } else {
            console.log('Exists');
        }
        // console.log(space_user_access_list);
        // await space_user_access_repository.create({});
    }


    /**
     * @param {String} id
     * @returns {String}
     */
    async getAvatarUrl(id) {
        let tickets = await this.ticket_repository.search({
            assembla_assigned_to_id: id,
        });

        if (tickets.length === 0) {
            tickets = await this.ticket_repository.search({
                assembla_reporter_id: id,
            });
        }

        if (tickets.length === 0) {
            return null;
        }


        const [
            ticket,
        ] = tickets;

        return ticket.assembla.picture_url;
    }
}

AssemblaUserSynchronizer.instance = null;

module.exports = {
    AssemblaUserSynchronizer,
};

