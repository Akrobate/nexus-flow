/* istanbul ignore file */

'use strict';

const {
    AssemblaTicketSynchronizer,
} = require('./AssemblaTicketSynchronizer');
const {
    AssemblaTicketTagSynchronizer,
} = require('./AssemblaTicketTagSynchronizer');
const {
    AssemblaTicketStatusSynchronizer,
} = require('./AssemblaTicketStatusSynchronizer');
const {
    AssemblaSpaceSynchronizer,
} = require('./AssemblaSpaceSynchronizer');
const {
    AssemblaTicketTagUpdate,
} = require('./AssemblaTicketTagUpdate');
const {
    AssemblaMilestoneSynchronizer,
} = require('./AssemblaMilestoneSynchronizer');
const {
    AssemblaTicketCommentUpdate,
} = require('./AssemblaTicketCommentUpdate');
const {
    AssemblaUserSynchronizer,
} = require('./AssemblaUserSynchronizer');
const {
    GitlabMergeRequestSynchronizer,
} = require('./GitlabMergeRequestSynchronizer');

module.exports = {
    AssemblaSpaceSynchronizer,
    AssemblaTicketSynchronizer,
    AssemblaTicketTagSynchronizer,
    AssemblaTicketStatusSynchronizer,
    AssemblaTicketTagUpdate,
    AssemblaMilestoneSynchronizer,
    AssemblaTicketCommentUpdate,
    AssemblaUserSynchronizer,
    GitlabMergeRequestSynchronizer,
};
