/* istanbul ignore file */

'use strict';

const {
    TicketRepository,
    TicketTagRepository,
    AssemblaRepository,
    MongoDbRepository,
} = require('../../repositories');

class AssemblaTicketTagUpdate {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaTicketTagUpdate}
     */
    static getInstance() {
        if (AssemblaTicketTagUpdate.instance === null) {
            AssemblaTicketTagUpdate.instance = new AssemblaTicketTagUpdate(
                AssemblaRepository.getInstance(),
                TicketRepository.getInstance(),
                TicketTagRepository.getInstance()
            );
        }
        return AssemblaTicketTagUpdate.instance;
    }


    /**
     * @param {AssemblaRepository} assembla_repository
     * @param {TicketRepository} ticket_repository
     * @param {TicketTagRepository} ticket_tag_repository
     */
    constructor(
        assembla_repository,
        ticket_repository,
        ticket_tag_repository
    ) {
        this.assembla_repository = assembla_repository;
        this.ticket_repository = ticket_repository;
        this.ticket_tag_repository = ticket_tag_repository;
    }


    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    async process() {

        const ticket_count = await this.ticket_repository.count({});

        for (let i = 0; i < ticket_count; i++) {
            const [
                ticket,
            ] = await this.ticket_repository.search({
                limit: 1,
                offset: i,
            });

            const week = 7 * 24 * 60 * 60 * 1000;

            if (ticket.tag_id_list_updated_at
                && new Date().getTime() - new Date(ticket.tag_id_list_updated_at).getTime() < week
            ) {
                console.log(`OK - ${ticket.summary}`);
                // eslint-disable-next-line no-continue
                continue;
            }

            if (ticket?.assembla?.space_id && ticket?.assembla?.number) {
                let ticket_tags = null;
                try {
                    ticket_tags = await this.assembla_repository
                        .getTagsByTicketNumber(ticket.assembla.space_id, ticket.assembla.number);
                } catch (error) {
                    console.log(error);
                }
                if (ticket_tags) {
                    const found_tags = await this.ticket_tag_repository.search({
                        assembla_id_list: ticket_tags.map((item) => item.id),
                    });
                    await this.ticket_repository.update(
                        ticket.id,
                        {
                            tag_id_list: found_tags.map((item) => item.id),
                            tag_id_list_updated_at: new Date(),
                        }
                    );
                } else {
                    await this.ticket_repository.update(
                        ticket.id,
                        {
                            tag_id_list: [],
                            tag_id_list_updated_at: new Date(),
                        }
                    );
                }
            }
            console.log(ticket.summary);

        }

        setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);

    }


}

AssemblaTicketTagUpdate.instance = null;

module.exports = {
    AssemblaTicketTagUpdate,
};
