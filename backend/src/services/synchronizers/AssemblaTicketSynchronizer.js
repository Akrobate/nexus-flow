'use strict';

const {
    TicketRepository,
    TicketStatusRepository,
    MilestoneRepository,
    SpaceRepository,
    AssemblaRepository,
    UserRepository,
} = require('../../repositories');

class AssemblaTicketSynchronizer {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaTicketSynchronizer}
     */
    static getInstance() {
        if (AssemblaTicketSynchronizer.instance === null) {
            AssemblaTicketSynchronizer.instance = new AssemblaTicketSynchronizer(
                AssemblaRepository.getInstance(),
                TicketRepository.getInstance(),
                TicketStatusRepository.getInstance(),
                MilestoneRepository.getInstance(),
                SpaceRepository.getInstance(),
                UserRepository.getInstance()
            );
        }
        return AssemblaTicketSynchronizer.instance;
    }


    /**
     * @param {AssemblaRepository} assembla_repository
     * @param {TicketRepository} ticket_repository
     * @param {TicketStatusRepository} ticket_status_repository
     * @param {MilestoneRepository} milestone_repository
     * @param {SpaceRepository} space_repository
     * @param {UserRepository} user_repository
     */
    constructor(
        assembla_repository,
        ticket_repository,
        ticket_status_repository,
        milestone_repository,
        space_repository,
        user_repository
    ) {
        this.assembla_repository = assembla_repository;
        this.ticket_repository = ticket_repository;
        this.ticket_status_repository = ticket_status_repository;
        this.milestone_repository = milestone_repository;
        this.space_repository = space_repository;
        this.user_repository = user_repository;

        this.page = 1;
        this.per_page = 100;
        this.sort_order = 'desc';
        this.sort_by = 'created_on';

        this.processing = false;
        this.processing_tickets_tags = false;
        this.processing_tickets_detection = false;

        this.ticket_status_list = [];
        this.milestone_list = [];
        this.space_list = [];
        this.user_list = [];

    }


    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    async process(space_id) {

        this.ticket_status_list = await this.ticket_status_repository.search({});
        this.milestone_list = await this.milestone_repository.search({});
        this.space_list = await this.space_repository.search({});
        this.user_list = await this.user_repository.search({});

        this.processing = true;
        this.page = 1;
        return this
            .worker(space_id)
            .finally(() => {
                this.processing = false;
            });
    }


    /**
     *
     * @param {*} space_id
     * @param {*} page
     * @returns {Promise<Object|Error>}
     */
    async worker(space_id) {
        const report = 0;
        const result_list = await this
            .assembla_repository
            .getTickets(
                space_id,
                report,
                this.page,
                this.per_page,
                this.sort_order,
                this.sort_by
            );

        for (const data of result_list) {
            console.log(`Processing Tickets Sync ${data.space_id} #${data.number} - ${data.summary}`);
            await this.upsertTicket(data);
        }
        /* istanbul ignore next */
        if (result_list.length < this.per_page) {
            return Promise.resolve();
        }
        /* istanbul ignore next */
        this.page += 1;
        /* istanbul ignore next */
        return this.worker(space_id);
    }


    /**
     * @param {*} data
     * @returns {Promise}
     */
    async upsertTicket(data) {
        const found_ticket = await this.ticket_repository.search({
            assembla_id: data.id,
        });

        const space = this.space_list.find((item) => item?.assembla?.id === data.space_id);
        const ticket_status = this.ticket_status_list.find((item) => item.name === data.status);
        const milestone = this.milestone_list
            .find((item) => item?.assembla?.id === data.milestone_id);


        const assigned_to_user = this.user_list
            .find((item) => item?.assembla?.id === data.assigned_to_id);

        const created_by_user = this.user_list
            .find((item) => item?.assembla?.id === data.space_id);

        /* istanbul ignore next */
        const input = {
            summary: data.summary,
            description: data.description,
            priority: data.priority,
            estimate: data.estimate,
            number: data.number,
            space_id: space.id,
            milestone_id: milestone?.id ? milestone?.id : undefined,
            avatar_url: data.picture_url,
            created_on: data.created_on,
            updated_at: data.updated_at,
            assigned_to_user_id: assigned_to_user ? assigned_to_user.id : null,
            created_by_user_id: created_by_user ? created_by_user.id : null,
            assembla: {
                ...data,
            },
        };

        /* istanbul ignore next */
        if (!milestone) {
            delete input.milestone_id;
        }

        /* istanbul ignore next */
        if (ticket_status) {
            input.status_id = ticket_status.id;
        }


        if (found_ticket.length === 1) {
            await this.ticket_repository.update(found_ticket[0].id, input);
        } else {
            await this.ticket_repository.create(input);
        }
    }

}

AssemblaTicketSynchronizer.instance = null;

module.exports = {
    AssemblaTicketSynchronizer,
};
