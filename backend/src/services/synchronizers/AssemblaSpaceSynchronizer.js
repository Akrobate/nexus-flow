/* istanbul ignore file */

'use strict';

const {
    SpaceRepository,
    AssemblaRepository,
} = require('../../repositories');

class AssemblaSpaceSynchronizer {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaSpaceSynchronizer}
     */
    static getInstance() {
        if (AssemblaSpaceSynchronizer.instance === null) {
            AssemblaSpaceSynchronizer.instance = new AssemblaSpaceSynchronizer(
                AssemblaRepository.getInstance(),
                SpaceRepository.getInstance()
            );
        }
        return AssemblaSpaceSynchronizer.instance;
    }


    /**
     * @param {AssemblaRepository} assembla_repository
     * @param {SpaceRepository} space_repository
     */
    constructor(
        assembla_repository,
        space_repository
    ) {
        this.assembla_repository = assembla_repository;
        this.space_repository = space_repository;
    }


    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    async process() {
        const spaces_list = await this.assembla_repository
            .getSpaces();
        for (const space of spaces_list) {
            console.log(`Processing Space Sync ${space.space_id} #${space.id} - ${space.name}`);
            await this.upsertSpace(space);
        }
    }


    /**
     * @param {*} data
     * @returns {Promise}
     */
    async upsertSpace(data) {
        const found = await this.space_repository.search({
            assembla_id: data.id,
        });

        const input = {
            name: data.name,
            prefix: data.prefix,
            description: data.description,
            status: data.status,
            space_tag: data.wiki_name,
            assembla: {
                ...data,
            },
        };

        if (found.length === 1) {
            await this.space_repository.update(found[0].id, input);
        } else {
            await this.space_repository.create(input);
        }
    }

}

AssemblaSpaceSynchronizer.instance = null;

module.exports = {
    AssemblaSpaceSynchronizer,
};
