/* istanbul ignore file */

'use strict';

const {
    TicketStatusRepository,
    SpaceRepository,
    AssemblaRepository,
    MongoDbRepository,
} = require('../../repositories');

class AssemblaTicketStatusSynchronizer {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaTicketStatusSynchronizer}
     */
    static getInstance() {
        if (AssemblaTicketStatusSynchronizer.instance === null) {
            AssemblaTicketStatusSynchronizer.instance = new AssemblaTicketStatusSynchronizer(
                AssemblaRepository.getInstance(),
                TicketStatusRepository.getInstance(),
                SpaceRepository.getInstance()
            );
        }
        return AssemblaTicketStatusSynchronizer.instance;
    }


    /**
     * @param {AssemblaRepository} assembla_repository
     * @param {TicketStatusRepository} ticket_status_repository
     * @param {SpaceRepository} space_repository
     */
    constructor(
        assembla_repository,
        ticket_status_repository,
        space_repository
    ) {
        this.assembla_repository = assembla_repository;
        this.ticket_status_repository = ticket_status_repository;
        this.space_repository = space_repository;

        this.space_list = [];
    }


    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    async process(space_id) {

        this.space_list = await this.space_repository.search();

        const ticket_status_list = await this.assembla_repository
            .getTicketStatusesBySpaceId(space_id);
        for (const ticket_status of ticket_status_list) {
            console.log(`Processing Tickets Status Sync ${ticket_status.space_id} #${ticket_status.id} - ${ticket_status.name}`);
            await this.upsertTicketStatus({
                ...ticket_status,
                space_id,
            });
        }
        setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);
    }


    /**
     * @param {*} data
     * @returns {Promise}
     */
    async upsertTicketStatus(data) {
        const found = await this.ticket_status_repository.search({
            assembla_id: data.id,
        });


        const space = this.space_list.find((item) => item?.assembla?.id === data.space_id);

        const input = {
            name: data.name,
            list_order: data.list_order,
            state: data.state,
            space_id: space.id,
            assembla: {
                ...data,
            },
        };

        if (found.length === 1) {
            await this.ticket_status_repository.update(found[0].id, input);
        } else {
            await this.ticket_status_repository.create(input);
        }
    }


}

AssemblaTicketStatusSynchronizer.instance = null;

module.exports = {
    AssemblaTicketStatusSynchronizer,
};
