/* istanbul ignore file */

'use strict';

const {
    TicketTagRepository,
    AssemblaRepository,
    MongoDbRepository,
} = require('../../repositories');

class AssemblaTicketTagSynchronizer {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaTicketTagSynchronizer}
     */
    static getInstance() {
        if (AssemblaTicketTagSynchronizer.instance === null) {
            AssemblaTicketTagSynchronizer.instance = new AssemblaTicketTagSynchronizer(
                TicketTagRepository.getInstance(),
                AssemblaRepository.getInstance()
            );
        }
        return AssemblaTicketTagSynchronizer.instance;
    }

    /**
     * @param {TicketTagRepository} ticket_tag_repository
     * @param {AssemblaRepository} assembla_repository
     */
    constructor(
        ticket_tag_repository,
        assembla_repository
    ) {
        this.ticket_tag_repository = ticket_tag_repository;
        this.assembla_repository = assembla_repository;

        this.page = 1;
        this.per_page = 100;
        this.processing = false;
    }

    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    process(space_id) {
        this.processing = true;
        return this
            .worker(space_id)
            .finally(() => {
                setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);
                this.processing = false;
            });
    }


    /**
     *
     * @param {*} space_id
     * @param {*} page
     * @returns {Promise<Object|Error>}
     */
    async worker(space_id) {
        const result_list = await this
            .assembla_repository
            .getTags(
                space_id,
                this.page,
                this.per_page
            );
        for (const data of result_list) {
            console.log(`Processing TicketsTags Sync ${data.space_id} #${data.name} - ${data.color}`);
            await this.upsertTicketTag(data);
        }
        if (result_list.length < this.per_page) {
            return Promise.resolve();
        }
        this.page += 1;
        return this.worker(space_id);
    }


    /**
     * @param {*} data
     * @returns {Promise}
     */
    async upsertTicketTag(data) {
        const found = await this.ticket_tag_repository.search({
            assembla_id: data.id,
        });

        const input = {
            name: data.name,
            state: data.state,
            color: data.color,
            assembla: {
                ...data,
            },
        };

        if (found.length === 1) {
            await this.ticket_tag_repository.update(found[0].id, input);
        } else {
            await this.ticket_tag_repository.create(input);
        }
    }
}

AssemblaTicketTagSynchronizer.instance = null;

module.exports = {
    AssemblaTicketTagSynchronizer,
};
