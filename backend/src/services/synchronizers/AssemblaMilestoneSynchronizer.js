'use strict';

const {
    MilestoneRepository,
    SpaceRepository,
    AssemblaRepository,
} = require('../../repositories');

class AssemblaMilestoneSynchronizer {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaMilestoneSynchronizer}
     */
    static getInstance() {
        if (AssemblaMilestoneSynchronizer.instance === null) {
            AssemblaMilestoneSynchronizer.instance = new AssemblaMilestoneSynchronizer(
                MilestoneRepository.getInstance(),
                SpaceRepository.getInstance(),
                AssemblaRepository.getInstance()
            );
        }
        return AssemblaMilestoneSynchronizer.instance;
    }


    /**
     * @param {MilestoneRepository} milestone_repository
     * @param {AssemblaRepository} space_repository
     * @param {AssemblaRepository} assembla_repository
     */
    constructor(
        milestone_repository,
        space_repository,
        assembla_repository
    ) {
        this.milestone_repository = milestone_repository;
        this.assembla_repository = assembla_repository;
        this.space_repository = space_repository;

        this.page = 1;
        this.per_page = 100;
        this.processing = false;

        this.space_list = [];
    }


    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    async process(space_id) {
        this.processing = true;

        this.space_list = await this.space_repository.search({});

        return this
            .worker(space_id)
            .finally(() => {
                this.processing = false;
            });
    }


    /**
     *
     * @param {*} space_id
     * @param {*} page
     * @returns {Promise<Object|Error>}
     */
    async worker(space_id) {
        const result_list = await this
            .assembla_repository
            .getAllMilestones(
                space_id,
                this.page,
                this.per_page
            );
        for (const data of result_list) {
            console.log(`Processing Milestone Sync ${data.space_id} ${data.title}`);
            await this.upsertMilestone(data);
        }
        /* istanbul ignore next */
        if (result_list.length < this.per_page) {
            return Promise.resolve();
        }
        /* istanbul ignore next */
        this.page += 1;
        /* istanbul ignore next */
        return this.worker(space_id);
    }


    /**
     * @param {*} data
     * @returns {Promise}
     */
    async upsertMilestone(data) {
        const found = await this.milestone_repository.search({
            assembla_id: data.id,
        });

        const space = this.space_list.find((item) => item.assembla.id === data.space_id);

        const input = {
            start_date: data.start_date,
            due_date: data.due_date,
            title: data.title,
            description: data.description,
            space_id: space.id,
            created_at: data.created_at,
            created_by_user_id: data.created_by_user_id,
            is_completed: data.is_completed,
            completed_date: data.completed_date,
            assembla: {
                ...data,
            },
        };

        if (found.length === 1) {
            await this.milestone_repository.update(found[0].id, input);
        } else {
            await this.milestone_repository.create(input);
        }
    }
}

AssemblaMilestoneSynchronizer.instance = null;

module.exports = {
    AssemblaMilestoneSynchronizer,
};
