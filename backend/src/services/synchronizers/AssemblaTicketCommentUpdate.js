/* istanbul ignore file */

'use strict';

const {
    TicketRepository,
    AssemblaRepository,
    MongoDbRepository,
} = require('../../repositories');

class AssemblaTicketCommentUpdate {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {AssemblaTicketCommentUpdate}
     */
    static getInstance() {
        if (AssemblaTicketCommentUpdate.instance === null) {
            AssemblaTicketCommentUpdate.instance = new AssemblaTicketCommentUpdate(
                AssemblaRepository.getInstance(),
                TicketRepository.getInstance()
            );
        }
        return AssemblaTicketCommentUpdate.instance;
    }


    /**
     * @param {AssemblaRepository} assembla_repository
     * @param {TicketRepository} ticket_repository
     * @param {TicketTagRepository} ticket_tag_repository
     */
    constructor(
        assembla_repository,
        ticket_repository
    ) {
        this.assembla_repository = assembla_repository;
        this.ticket_repository = ticket_repository;
    }


    /**
     * @param {String} space_id
     * @returns {Promise<Object|error>}
     */
    async process() {

        const ticket_count = await this.ticket_repository.count({});

        for (let i = 0; i < ticket_count; i++) {
            const [
                ticket,
            ] = await this.ticket_repository.search({
                limit: 1,
                offset: i,
            });

            const week = 7 * 24 * 60 * 60 * 1000;

            if (ticket.comment_list_updated_at
                && new Date().getTime() - new Date(ticket.comment_list_updated_at).getTime() < week
            ) {
                console.log(`OK - ${ticket.summary}`);
                // eslint-disable-next-line no-continue
                continue;
            }

            if (ticket?.assembla?.space_id && ticket?.assembla?.number) {
                let ticket_comments = null;
                try {
                    ticket_comments = await this.assembla_repository
                        .getTicketComments(ticket.assembla.space_id, ticket.assembla.number);
                } catch (error) {
                    console.log(error);
                }

                if (ticket_comments) {
                    await this.ticket_repository.update(
                        ticket.id,
                        {
                            comment_list: ticket_comments,
                            comment_list_updated_at: new Date(),

                        }
                    );
                } else {
                    await this.ticket_repository.update(
                        ticket.id,
                        {
                            comment_list: [],
                            comment_list_updated_at: new Date(),
                        }
                    );
                }
            }
            console.log(ticket.summary);

        }

        setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);

    }


}

AssemblaTicketCommentUpdate.instance = null;

module.exports = {
    AssemblaTicketCommentUpdate,
};
