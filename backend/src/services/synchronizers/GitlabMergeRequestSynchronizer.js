/* istanbul ignore file */

'use strict';

const {
    GitlabRepository,
    MergeRequestRepository,
    MongoDbRepository,
} = require('../../repositories');

class GitlabMergeRequestSynchronizer {

    /* istanbul ignore next */
    /**
     *
     * @static
     *
     * @returns {GitlabMergeRequestSynchronizer}
     */
    static getInstance() {
        if (GitlabMergeRequestSynchronizer.instance === null) {
            GitlabMergeRequestSynchronizer.instance = new GitlabMergeRequestSynchronizer(
                GitlabRepository.getInstance(),
                MergeRequestRepository.getInstance()
            );
        }
        return GitlabMergeRequestSynchronizer.instance;
    }


    /**
     * @param {GitlabRepository} gitlab_repository
     * @param {MergeRequestRepository} merge_request_repository
     */
    constructor(
        gitlab_repository,
        merge_request_repository
    ) {
        this.gitlab_repository = gitlab_repository;
        this.merge_request_repository = merge_request_repository;

    }


    /**
     * @returns {Promise}
     */
    async process() {

        let count = 0;
        let mr_list = await this.gitlab_repository.getAllMergeRequests(1, 1);
        let page = 1;

        while (mr_list.length > 0) {
            mr_list = await this.gitlab_repository.getAllMergeRequests(page, 100);
            for (const mr of mr_list) {
                await this.upsertMergeRequest(mr);
                count++;
                console.log(`${count} - ${mr.id} - ${mr.web_url}`);
            }
            page++;
        }

        setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);

    }


    /**
     * @param {Object} data
     * @returns {Promise}
     */
    async upsertMergeRequest(data) {

        const found = await this.merge_request_repository.search({
            merge_request_id: data.id,
        });

        const input = {
            state: data.state,
            provider: 'gitlab',
            repository: data.references.full.split('!')[0],
            created_on: data.created_at,
            updated_at: data.updated_at,
            web_url: data.web_url,
            merge_request: {
                ...data,
            },
        };

        if (found.length === 1) {
            await this.merge_request_repository.update(found[0].id, input);
        } else {
            await this.merge_request_repository.create(input);
        }
    }

}

GitlabMergeRequestSynchronizer.instance = null;

module.exports = {
    GitlabMergeRequestSynchronizer,
};
