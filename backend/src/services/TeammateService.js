'use strict';

const {
    AbstractService,
} = require('./AbstractService');

const {
    UserRepository,
    SpaceUserAccessRepository,
} = require('../repositories');

class TeammateService extends AbstractService {


    /* istanbul ignore next */
    /**
     * @static
     * @returns {TeammateService}
     */
    static getInstance() {
        if (TeammateService.instance === null) {
            TeammateService.instance = new TeammateService(
                UserRepository.getInstance(),
                SpaceUserAccessRepository.getInstance()
            );
        }
        return TeammateService.instance;
    }


    /**
     * @param {UserRepository} user_repository
     * @param {SpaceUserAccessRepository} space_user_access_repository
     */
    constructor(
        user_repository,
        space_user_access_repository
    ) {
        super(user_repository);
        this.space_user_access_repository = space_user_access_repository;
        this.entity = 'teammate';
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Object}
     */
    async search(user, input) {
        const {
            space_id,
            user_id_list,
        } = input;

        const space_user_access_list = await this.space_user_access_repository.search({
            space_id,
        });

        const user_list = await this.repository.search({
            space_id,
            id_list: user_id_list,
        });

        const teammate_list = user_list.map((_user) => {
            const user_access = space_user_access_list
                .find((space_user_access) => space_user_access.user_id === _user.id);
            return {
                ..._user,
                user_access: user_access ? user_access : null,
            };
        });
        return teammate_list;
    }

}

TeammateService.instance = null;

module.exports = {
    TeammateService,
};
