'use strict';

const extractUrls = require('extract-urls');
const Url = require('url-parse');


class GitlabService {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {SpaceAccessProfileService}
     */
    static getInstance() {
        if (GitlabService.instance === null) {
            GitlabService.instance = new GitlabService();
        }
        return GitlabService.instance;
    }

    /**
     * @param {String} content
     * @return {Array<String>}
     */
    detectMergeRequestUrl(content) {
        const result = [];
        const url_list = extractUrls(content ? content : '');
        if (url_list === undefined) {
            return result;
        }

        url_list.forEach((web_url) => {

            const url_object = new Url(web_url, true);
            const path_name = url_object.pathname;

            if (url_object.host !== 'gitlab.com') {
                return;
            }

            let project = '';
            let mr_id = '';
            const project_match = path_name.match(/\/.*\/(.*)\/-\/merge_requests\//);
            const mr_id_match = path_name.match(/\/.*\/.*\/-\/merge_requests\/([0-9]*)/);

            if (
                project_match !== null
                && project_match !== undefined
                && project_match.length > 1
                && mr_id_match !== null
                && mr_id_match !== undefined
                && mr_id_match.length > 1
            ) {
                ([
                    ,
                    project,
                ] = project_match);
                ([
                    ,
                    mr_id,
                ] = mr_id_match);
            } else {
                return;
            }

            mr_id = parseInt(mr_id, 10);

            if (isNaN(mr_id)) {
                return;
            }

            if (result.find((element) => element.web_url === web_url) !== undefined) {
                return;
            }

            result.push({
                project,
                merge_request_id: mr_id,
                web_url,
            });

        });


        return result;
    }


    /**
     * @param {*} web_url
     * @returns {String}
     */
    normalizeMergeRequestUrl(web_url) {
        const removed_diffs = web_url
            .replace(/\/diffs\?.*/, '')
            .replace(/\/diffs$/, '')
            .replace(/\/$/, '');
        return removed_diffs;
    }


    /**
     * @param {*} web_url
     * @return {String|null}
     */
    extractProjectName(web_url) {
        const url_object = new Url(web_url, true);
        const path_name = url_object.pathname;

        if (url_object.host !== 'gitlab.com') {
            return null;
        }

        let project = '';
        const project_match = path_name.match(/\/.*\/(.*)\/-\/merge_requests\//);

        if (project_match !== null && project_match !== undefined && project_match.length > 1) {
            ([
                ,
                project,
            ] = project_match);
        } else {
            return null;
        }

        return project;
    }
}


GitlabService.instance = null;

module.exports = {
    GitlabService,
};
