'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    SpaceUserAccessService,
} = require('../services');


class SpaceUserAccessController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {SpaceUserAccessController}
     */
    static getInstance() {
        if (SpaceUserAccessController.instance === null) {
            SpaceUserAccessController.instance = new SpaceUserAccessController(
                SpaceUserAccessService.getInstance()
            );
        }
        return SpaceUserAccessController.instance;
    }

    /**
     * @param {*} service
     */
    constructor(service) {
        super(service);
        this.entity = 'space_user_access';
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationUpdate(request) {
        return joi
            .object()
            .keys({})
            .unknown(true)
            .validate(request);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationCreate(request) {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        space_id: joi.string().required(),
                        user_id: joi.string().required(),
                        access_profile_id: joi.string().required(),
                    })
                    .unknown(true),
            })
            .unknown(true)
            .validate(request);
    }

}

SpaceUserAccessController.instance = null;

module.exports = {
    SpaceUserAccessController,
};
