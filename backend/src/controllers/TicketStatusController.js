'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    TicketStatusService,
} = require('../services');


class TicketStatusController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {TicketStatusController}
     */
    static getInstance() {
        if (TicketStatusController.instance === null) {
            TicketStatusController.instance = new TicketStatusController(
                TicketStatusService.getInstance()
            );
        }
        return TicketStatusController.instance;
    }

    /**
     * @param {*} service
     */
    constructor(service) {
        super(service);
        this.entity = 'ticket_status';
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearch(request) {
        return joi
            .object()
            .keys({
                query: joi
                    .object()
                    .keys({
                        space_id: joi
                            .string()
                            .required(),
                        sort: joi
                            .array()
                            .items(
                                joi.string()
                            )
                            .default(['list_order']),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationCreate(request) {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        name: joi.string()
                            .required(),
                        list_order: joi.number()
                            .required(),
                        state: joi.number()
                            .required(),
                        space_id: joi.string()
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationUpdate(request) {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        name: joi.string()
                            .optional(),
                        list_order: joi.number()
                            .optional(),
                        state: joi.number()
                            .optional(),
                    })
                    .required(),
                params: joi.object()
                    .keys({
                        id: joi.string().required(),
                    }),
            })
            .unknown(true)
            .validate(request);
    }

}

TicketStatusController.instance = null;

module.exports = {
    TicketStatusController,
};
