'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    TicketService,
} = require('../services');


class TicketController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {TicketController}
     */
    static getInstance() {
        if (TicketController.instance === null) {
            TicketController.instance = new TicketController(
                TicketService.getInstance()
            );
        }
        return TicketController.instance;
    }

    /**
     * @param {TicketService} service
     */
    constructor(service) {
        super(service);
        this.entity = 'ticket';
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationCreate(request) {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        summary: joi.string()
                            .required(),
                        description: joi.string()
                            .required(),
                        priority: joi.number()
                            .required(),
                        milestone_id: joi.string()
                            .required(),
                        space_id: joi.string()
                            .required(),
                        status_id: joi.string(),
                        estimate: joi.number(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationUpdate(request) {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        summary: joi.string(),
                        description: joi.string(),
                        priority: joi.number(),
                        milestone_id: joi.string(),
                        space_id: joi.string(),
                        status_id: joi.string(),
                        estimate: joi.number(),
                    })
                    .required(),
                params: joi.object()
                    .keys({
                        id: joi.string().required(),
                    }),
            })
            .unknown(true)
            .validate(request);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearch(request) {
        return joi
            .object()
            .keys({
                query: joi
                    .object()
                    .keys({
                        id_list: joi
                            .array()
                            .items(
                                joi.string()
                            ),
                        milestone_id_list: joi
                            .array()
                            .items(
                                joi.string()
                            ),
                        space_id: joi.string(),
                        space_id_list: joi
                            .array()
                            .items(
                                joi.string()
                            ),
                        status_id: joi.string(),
                        status_id_list: joi
                            .array()
                            .items(
                                joi.string()
                            ),
                        assigned_to_user_id: joi.string(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }

}

TicketController.instance = null;

module.exports = {
    TicketController,
};
