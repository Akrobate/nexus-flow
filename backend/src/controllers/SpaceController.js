'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    SpaceService,
} = require('../services');


class SpaceController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {SpaceController}
     */
    static getInstance() {
        if (SpaceController.instance === null) {
            SpaceController.instance = new SpaceController(
                SpaceService.getInstance()
            );
        }
        return SpaceController.instance;
    }

    /**
     * @param {*} service
     */
    constructor(service) {
        super(service);
        this.entity = 'space';
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearch(request) {
        return joi
            .object()
            .keys({})
            .unknown(true)
            .validate(request);
    }

}

SpaceController.instance = null;

module.exports = {
    SpaceController,
};
