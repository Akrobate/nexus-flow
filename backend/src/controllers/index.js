'use strict';

const {
    TicketController,
} = require('./TicketController');
const {
    TicketStatusController,
} = require('./TicketStatusController');
const {
    UserController,
} = require('./UserController');
const {
    MilestoneController,
} = require('./MilestoneController');
const {
    SpaceController,
} = require('./SpaceController');
const {
    TicketTagController,
} = require('./TicketTagController');
const {
    TeammateController,
} = require('./TeammateController');
const {
    SpaceAccessProfileController,
} = require('./SpaceAccessProfileController');
const {
    SpaceUserAccessController,
} = require('./SpaceUserAccessController');
const {
    MergeRequestController,
} = require('./MergeRequestController');

module.exports = {
    TicketController,
    TicketStatusController,
    UserController,
    MilestoneController,
    SpaceController,
    TicketTagController,
    TeammateController,
    SpaceAccessProfileController,
    SpaceUserAccessController,
    MergeRequestController,
};
