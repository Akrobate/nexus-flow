'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    MilestoneService,
} = require('../services');


class MilestoneController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {MilestoneController}
     */
    static getInstance() {
        if (MilestoneController.instance === null) {
            MilestoneController.instance = new MilestoneController(
                MilestoneService.getInstance()
            );
        }
        return MilestoneController.instance;
    }

    /**
     * @param {MilestoneService} service
     */
    constructor(service) {
        super(service);
        this.entity = 'milestone';
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearch(request) {
        return joi
            .object()
            .keys({
                query: joi
                    .object()
                    .keys({
                        space_id: joi
                            .string()
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }

}

MilestoneController.instance = null;

module.exports = {
    MilestoneController,
};
