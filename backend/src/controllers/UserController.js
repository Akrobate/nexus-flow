'use strict';

const HTTP_CODE = require('http-status');
const joi = require('joi');

const {
    AbstractController,
} = require('./AbstractController');
const {
    UserService,
} = require('../services');


class UserController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {UserController}
     */
    static getInstance() {
        if (UserController.instance === null) {
            UserController.instance = new UserController(
                UserService.getInstance()
            );
        }
        return UserController.instance;
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async login(request, response) {
        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        login: joi.string()
                            .required(),
                        password: joi.string()
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
        this.checkValidationError(error);
        const result = await this.service.login(value.body);
        return response.status(HTTP_CODE.OK).send(result);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async register(request, response) {
        const {
            error,
            value,
        } = joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        login: joi.string()
                            .required(),
                        email: joi.string()
                            .required(),
                        password: joi.string()
                            .required(),
                        organization_name: joi.string()
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);

        this.checkValidationError(error);
        const result = await this.service.register(value.body);
        return response.status(HTTP_CODE.OK).send(result);
    }

}

UserController.instance = null;

module.exports = {
    UserController,
};
