'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    TicketTagService,
} = require('../services');


class TicketTagController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {TicketTagController}
     */
    static getInstance() {
        if (TicketTagController.instance === null) {
            TicketTagController.instance = new TicketTagController(
                TicketTagService.getInstance()
            );
        }
        return TicketTagController.instance;
    }

    /**
     * @param {*} service
     */
    constructor(service) {
        super(service);
        this.entity = 'ticket_tag';
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearch(request) {
        return joi
            .object()
            .keys({
                query: joi
                    .object()
                    .keys({
                        space_id: joi
                            .string()
                            .required(),
                    })
                    .required(),
            })
            .unknown(true)
            .validate(request);
    }

}

TicketTagController.instance = null;

module.exports = {
    TicketTagController,
};
