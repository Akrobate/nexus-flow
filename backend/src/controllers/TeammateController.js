'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    TeammateService,
} = require('../services');


class TeammateController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {TeammateController}
     */
    static getInstance() {
        if (TeammateController.instance === null) {
            TeammateController.instance = new TeammateController(
                TeammateService.getInstance()
            );
        }
        return TeammateController.instance;
    }

    /**
     * @param {*} service
     */
    constructor(service) {
        super(service);
        this.entity = 'teammate';
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearch(request) {
        return joi
            .object()
            .keys({})
            .unknown(true)
            .validate(request);
    }

}

TeammateController.instance = null;

module.exports = {
    TeammateController,
};
