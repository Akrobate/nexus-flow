'use strict';

const HTTP_CODE = require('http-status');

const {
    CustomError,
} = require('../CustomError');

class AbstractController {

    /**
     * @param {Object} error
     * @returns {void}
     */
    checkValidationError(error) {
        if (error) {
            throw new CustomError(CustomError.BAD_PARAMETER, error.message);
        }
    }


    /**
     * @param {ServiceInstance} service
     */
    constructor(service) {
        this.service = service;
        this.entity = 'entity';
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async read(request, response) {
        const {
            params,
        } = request;
        const {
            jwt_data: user,
        } = request;
        const result = await this.service.read(user, params.id);
        return response.status(HTTP_CODE.OK).send(result);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async create(request, response) {
        const {
            error,
            value,
        } = this.validationCreate(request);
        const {
            jwt_data: user,
        } = request;
        this.checkValidationError(error);
        const result = await this.service.create(user, value.body);
        return response.status(HTTP_CODE.OK).send(result);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async search(request, response) {
        const {
            jwt_data: user,
        } = request;
        const {
            error,
            value,
        } = this.validationSearch(request);

        this.checkValidationError(error);

        const entity_list = await this.service.search(user, value.query);
        return response.status(HTTP_CODE.OK).send({
            [`${this.entity}_list`]: entity_list,
        });
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async update(request, response) {
        const {
            jwt_data: user,
        } = request;
        const {
            error,
            value,
        } = this.validationUpdate(request);
        this.checkValidationError(error);
        const result = await this.service.update(user, value.params.id, value.body);
        return response.status(HTTP_CODE.OK).send(result);
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async delete(request, response) {
        const {
            params,
        } = request;
        const {
            jwt_data: user,
        } = request;
        const result = await this.service.delete(user, params.id);
        return response.status(HTTP_CODE.OK).send(result);
    }
}

module.exports = {
    AbstractController,
};
