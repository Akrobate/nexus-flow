'use strict';

const HTTP_CODE = require('http-status');
const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    MergeRequestService,
} = require('../services');


class MergeRequestController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {MergeRequestController}
     */
    static getInstance() {
        if (MergeRequestController.instance === null) {
            MergeRequestController.instance = new MergeRequestController(
                MergeRequestService.getInstance()
            );
        }
        return MergeRequestController.instance;
    }

    /**
     * @param {MilestoneService} service
     */
    constructor(service) {
        super(service);
        this.entity = 'merge_request';
    }

    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearchStatistics(request) {
        return joi
            .object()
            .keys({
                query: joi
                    .object()
                    .keys({
                        created_on_lower_boundary: joi
                            .string(),
                        created_on_lower_upper: joi
                            .string(),
                    }),
            })
            .unknown(true)
            .validate(request);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    async searchStatistics(request, response) {
        const {
            jwt_data: user,
        } = request;
        const {
            error,
            value,
        } = this.validationSearchStatistics(request);

        this.checkValidationError(error);

        const entity_list = await this.service.mergeRequestStatistics(user, value.query);
        return response.status(HTTP_CODE.OK).send({
            merge_request_statistic_list: entity_list,
        });
    }
}

MergeRequestController.instance = null;

module.exports = {
    MergeRequestController,
};
