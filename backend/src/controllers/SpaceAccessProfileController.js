'use strict';

const joi = require('joi');
const {
    AbstractController,
} = require('./AbstractController');
const {
    SpaceAccessProfileService,
} = require('../services');


class SpaceAccessProfileController extends AbstractController {

    /* istanbul ignore next */
    /**
     * @returns {SpaceAccessProfileController}
     */
    static getInstance() {
        if (SpaceAccessProfileController.instance === null) {
            SpaceAccessProfileController.instance = new SpaceAccessProfileController(
                SpaceAccessProfileService.getInstance()
            );
        }
        return SpaceAccessProfileController.instance;
    }

    /**
     * @param {*} service
     */
    constructor(service) {
        super(service);
        this.entity = 'space_access_profile';
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Promise<Object>}
     */
    validationSearch(request) {
        return joi
            .object()
            .keys({})
            .unknown(true)
            .validate(request);
    }

}

SpaceAccessProfileController.instance = null;

module.exports = {
    SpaceAccessProfileController,
};
