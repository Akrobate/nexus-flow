'use strict';

const {
    error_manager_middleware,
    not_found_error_middleware,
} = require('./HttpErrorManager');

module.exports = {
    error_manager_middleware,
    not_found_error_middleware,
};
