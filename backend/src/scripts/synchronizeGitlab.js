/* istanbul ignore file */

'use strict';

const synchronizers = require('../services/synchronizers/');

(async () => {
    await synchronizers
        .GitlabMergeRequestSynchronizer
        .getInstance()
        .process();
})();
