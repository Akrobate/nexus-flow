/* istanbul ignore file */

'use strict';

const {
    MongoDbRepository,
} = require('../repositories');
const {
    MergeRequestService,
} = require('../services');

const merge_request_service = MergeRequestService.getInstance();

const DEBUG = true;

(async () => {
    await merge_request_service.detectMergeRequestInTickets(DEBUG);
    setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);
})();
