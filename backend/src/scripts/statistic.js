/* istanbul ignore file */

'use strict';

const {
    MergeRequestService,
} = require('../services/MergeRequestService');

const merge_request_serivce = MergeRequestService.getInstance();

(async () => {
    const result = await merge_request_serivce.mergeRequestStatistics({}, {});
    console.log(result);
})();
