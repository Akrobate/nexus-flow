/* istanbul ignore file */

'use strict';

const {
    AssemblaSpaceSynchronizer,
    AssemblaTicketSynchronizer,
    AssemblaTicketStatusSynchronizer,
    AssemblaTicketTagSynchronizer,
    AssemblaMilestoneSynchronizer,
    AssemblaTicketTagUpdate,
    AssemblaTicketCommentUpdate,
    AssemblaUserSynchronizer,
} = require('../services/synchronizers/');

const {
    SpaceRepository,
    MongoDbRepository,
} = require('../repositories/');

const space_synchronizer = AssemblaSpaceSynchronizer.getInstance();
const assembla_ticket_synchronizer = AssemblaTicketSynchronizer.getInstance();
const assembla_ticket_tag_synchronizer = AssemblaTicketTagSynchronizer.getInstance();
const assembla_ticket_status_synchronizer = AssemblaTicketStatusSynchronizer.getInstance();
const assembla_milestone_synchronizer = AssemblaMilestoneSynchronizer.getInstance();
const assembla_ticket_tag_update = AssemblaTicketTagUpdate.getInstance();
const assembla_ticket_comment_update = AssemblaTicketCommentUpdate.getInstance();
const space_repository = SpaceRepository.getInstance();
// const user_synchronizer = AssemblaUserSynchronizer.getInstance();

// const space_id_developement = 'bXqO74QNir54k9acwqEsg8';
// const space_id_conception = 'cbFvqqSVir5Okwdmr6CpXy';


async function synchroniseFullSpace(space_id) {
    await assembla_milestone_synchronizer.process(space_id);
    await assembla_ticket_status_synchronizer.process(space_id);
    await assembla_ticket_tag_synchronizer.process(space_id);
    await assembla_ticket_synchronizer.process(space_id);
}


const FULL_SYNCHRONIZATION = false;

(async () => {

    await space_synchronizer.process();

    const SPACE_STATUS_ACTIVE = 1;


    //    await user_synchronizer.process(space_id_developement);

    const space_list = (await space_repository.search())
        .filter((item) => item.assembla.status === SPACE_STATUS_ACTIVE);

    for (const space of space_list) {
        const space_id = space.assembla.id;
        await synchroniseFullSpace(space_id);
    }

    if (FULL_SYNCHRONIZATION) {
        await assembla_ticket_tag_update.process();
        await assembla_ticket_comment_update.process();
    }

    setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);

})();
