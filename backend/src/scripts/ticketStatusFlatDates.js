/* istanbul ignore file */

'use strict';

const {
    MongoDbRepository,
} = require('../repositories');
const {
    TicketStatusService,
} = require('../services');

const ticket_status_service = TicketStatusService.getInstance();

(async () => {
    await ticket_status_service.updateFlatDatesStatuses();
    setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);
})();
