'use strict';

const {
    logger,
} = require('../logger');
const {
    MongoDbRepository,
} = require('../repositories');
const {
    TicketService,
} = require('../services');

const ticket_service = TicketService.getInstance();

const RESET_TICKET_LINKS = true;

(async () => {
    if (RESET_TICKET_LINKS) {
        logger.log('Reseting all existing links');
        await ticket_service.resetAllTicketsLinks();
    }
    await ticket_service.detectTicketsLinks();

    setTimeout(() => MongoDbRepository.getInstance().closeConnection(), 100);
})();

