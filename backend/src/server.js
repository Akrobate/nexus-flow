'use strict';

const cors = require('cors');

const server = require('express')();
const body_parser = require('body-parser');
const {
    error_manager_middleware,
    not_found_error_middleware,
} = require('./middlewares');

const initServerRoutes = require('./routes/');

server.use(cors());
server.use(body_parser.urlencoded({
    extended: true,
}));
server.use(body_parser.json());

initServerRoutes(server);

server.use(not_found_error_middleware);
server.use(error_manager_middleware);

module.exports = {
    server,
};
