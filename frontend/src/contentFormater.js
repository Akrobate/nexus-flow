import { DateTime, Interval } from 'luxon'

function formatDate(string_date) {
    return DateTime.fromISO(string_date)
        .toFormat("d/L/yy");
}


function formatDateTime(string_date) {
    return DateTime.fromISO(string_date)
        .toFormat("d/L/yy HH:mm");
}


function formatHumanizedDate(string_date) {
    const now = DateTime.now();
    
    const current_date = DateTime.fromISO(string_date);
    
    const interval = Interval.fromDateTimes(current_date, now);
    
    const diff = interval.toDuration(['days', 'hours', 'minutes', 'seconds']).toObject();

    let resultat = "Il y a ";

    if (diff.days) {
        resultat += `${Math.floor(diff.days)}j `;
    }
    if (diff.hours) {
        resultat += `${Math.floor(diff.hours)}h `;
    }
    if (diff.minutes) {
        resultat += `${Math.floor(diff.minutes)}m `;
    }
    if (!diff.days && !diff.hours && !diff.minutes) {
        resultat += "moins d'une minute";
    }
  
    return resultat.trim();
}

export {
    formatDate,
    formatDateTime,
    formatHumanizedDate,
}