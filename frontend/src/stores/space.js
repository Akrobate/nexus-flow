import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import {
    space_repository,
    user_repository,
} from '@/repositories'

export const useSpace = defineStore('space', () => {

    const space_list = ref([])
    const space_access_profile_list = ref([])
    const loaded = ref(false);
    const load_promise = ref(null);


    async function _load() {
        space_list.value = await space_repository.search()
        space_access_profile_list.value = await user_repository.getSpaceAccessProfiles()
        return 
    }

    async function load() {
        if (!loaded.value) {
            if (load_promise.value !== null) {
                return load_promise.value
            }
            try {
                load_promise.value = _load()
                await load_promise.value
                loaded.value = true
            } catch (error) {
                loaded.value = false
            }
            load_promise.value = null
        }
    }

    async function getSpaces() {
        await load()
        return space_list.value
    }

    async function getSpaceAccessProfiles() {
        await load()
        return space_access_profile_list.value
    }

    const getPrefixBySpaceId = computed(() =>
        (space_id) => space_list.value.find((item) => item.id === space_id).prefix
    )
    

    return {
        load,
        getSpaces,
        getSpaceAccessProfiles,
        getPrefixBySpaceId,
        space_list,
        space_access_profile_list,
    }
})
