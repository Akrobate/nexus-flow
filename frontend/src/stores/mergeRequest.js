import { defineStore } from 'pinia'
import {
    merge_request_repository,
} from '@/repositories'

export const useMergeRequest = defineStore('mergeRequest', () => {

    async function searchMergeRequestStatics(input) {

        const {
            created_on_lower_boundary,
        } = input
//= '2024-06-21'


        const all_statistics = await merge_request_repository.searchMergeRequestStatics({})

        const last_month_statistics = await merge_request_repository.searchMergeRequestStatics({
            created_on_lower_boundary,
        })

        return all_statistics.map((statistic) => {
            const last_month_statistic = last_month_statistics.find((_last_month_statistic) => {
                return _last_month_statistic.repository === statistic.repository
            })

            return {
                ...statistic,
                custmon_date_statistic: last_month_statistic,
            }

         })


    }

    return {
        searchMergeRequestStatics,
    }
})

