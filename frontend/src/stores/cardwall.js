import { ref } from 'vue'
import { defineStore } from 'pinia'
import {
    ticket_repository,
} from '@/repositories'

export const useCardwall = defineStore('cardwall', () => {

    const ticket_list = ref([])
    const ticket_status_list = ref([])
    const ticket_tag_list = ref([])
    const milestone_list = ref([])
    const space = ref(null)

    const selected_milestone = ref({})
    const selected_status = ref({})

    async function search() {
        const response = await ticket_repository.search({
            milestone_id_list: [
                selected_milestone.value.id,
            ],
            space_id: space.value.id,
        })
        ticket_list.value = response.slice(0,500)

        return response;
    }

    function setSpace(_space) {
        space.value = _space.value
    }

    function selectMilestone(item) {
        selected_milestone.value = item;
    }

    function selectStatus(item) {
        selected_status.value = item;
    }

    async function getStatuses() {
        const response = await ticket_repository.getStatuses({
            space_id: space.value.id,
        })
        ticket_status_list.value = response
        return response
    }

    async function getTags() {
        const response = await ticket_repository.getTags({
            space_id: space.value.id,
        })
        ticket_tag_list.value = response
        return response
    }
    
    async function getMilestones() {
        const response = await ticket_repository.getMilestones({
            space_id: space.value.id,
        })
        milestone_list.value = response
        return response
    }

    function generateCardwall() {

        console.log("###################################################################")
        console.log(ticket_list.value)
        console.log("###################################################################")


        console.log("*******************************************************************")
        console.log(ticket_status_list.value)
        console.log("*******************************************************************")

        for (const ticket_status of ticket_status_list.value) {
            ticket_status.ticket_list = ticket_list
                .value.filter((item) => item.status_id === ticket_status.id)
                .map((item) => ({
                    ...item,
                    prefix: space.value.prefix,
                }))
        }
    }

    return {
        ticket_list,
        ticket_status_list,
        ticket_tag_list,
        milestone_list,
        selected_milestone,

        search,
        getStatuses,
        setSpace,
        selectMilestone,
        selectStatus,
        generateCardwall,
        getMilestones,
        getTags,
    }
})
