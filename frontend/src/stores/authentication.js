import { ref } from 'vue'
import { defineStore } from 'pinia'
import {
    user_repository,
} from '@/repositories'

export const useAuthentication = defineStore('authentication', () => {

    const is_connected = ref(user_repository.isValidLocalStorageToken())
    const token = ref(user_repository.getTokenFromLocalStorageIfIsValid());

    async function login(login, password) {
        try {
            const response = await user_repository.login(login, password)
            user_repository.setTokenLocalStorage(response.token)
            is_connected.value = true;
            token.value = response.token;
        } catch (error) {
            is_connected.value = false;
            token.value = null;
            throw error;
        }
    }

    async function logout() {
        user_repository.removeTokenLocalStorage()
        token.value = null
        is_connected.value = false
    }


    function getUserData() {
        return user_repository.decodeToken(token.value)
    }


    function storedTokenIsValid() {
        return user_repository.isValidToken(token.value)
    }

    return {
        getUserData,
        storedTokenIsValid,
        login,
        logout,
        token,
    }

})
