import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import {
    user_repository,
} from '@/repositories'

export const useTeammate = defineStore('teammate', () => {

    const teammate_list = ref({})


    async function load(space_id, force = false) {
        if (!teammate_list.value[space_id] || force) {
            const response = await user_repository.getSpaceTeammates(space_id)
            teammate_list.value[space_id] = response
        }
    }

    async function getTeammates(space_id) {
        await load(space_id)
        return teammate_list.value[space_id]
    }

    const teammatesInSpaceId = computed(() => (space_id) => teammate_list.value[space_id])

    return {
        load,
        getTeammates,
        teammatesInSpaceId,
    }
})
