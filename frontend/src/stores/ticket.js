import { ref } from 'vue'
import { defineStore } from 'pinia'
import {
    ticket_repository,
} from '@/repositories'

export const useTicket = defineStore('ticket', () => {

    const ticket_modal = ref(false)
    const ticket = ref({})
    const space = ref(null)
    const loading = ref(false)

    function setSpace(_space) {
        space.value = _space.value
    }

   
    async function loadTicket(ticket_id) {
        loading.value = true
        ticket_modal.value = true
        const response = await ticket_repository.search({
            id_list: [ticket_id],
        })
        ticket.value = response[0]
        loading.value = false

        return response
    }


    async function update(ticket_id, input) {
        await ticket_repository.update(ticket_id, input)
    }

    return {
        loading,
        ticket,
        ticket_modal,
        setSpace,
        loadTicket,
        update,
    }
})
