import { defineStore, storeToRefs } from 'pinia'
import { useSpace } from './space'
import { useTag } from './tag'
import { useStatus } from './status'
import { useMilestone } from './milestone'
import { ref } from 'vue'

export const useApp = defineStore('app', () => {

    const space_store = useSpace()
    const tag_store = useTag()
    const milestone_store = useMilestone()
    const status_store = useStatus()

    const { space_list } = storeToRefs(space_store)

    const selected_space = ref(null)
    
    const ready = ref(false)

    async function load() {
        await space_store.load()
        const selected_space_id = getSelectedSpaceId()
        const _selected_space = space_list.value.find((item) =>  item.id === selected_space_id)
        if (_selected_space !== undefined) {
            selectSpace(_selected_space)

            await tag_store.load(selected_space_id)
            await status_store.load(selected_space_id)
            await milestone_store.load(selected_space_id)
        }
        ready.value = true
    }

    function selectSpace(space) {
        selected_space.value = space
        _setSelectedSpaceId(selected_space.value.id)
    }

    function getSelectedSpaceId() {
        return localStorage.selected_space_id ? localStorage.selected_space_id : null
    }

    function _setSelectedSpaceId(space_id) {
        return localStorage.selected_space_id = space_id
    }

    return {
        selectSpace,
        load,
        ready,
        selected_space,
        space_list
    }
})
