import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useSnackBar = defineStore('snackBar', () => {

    const DEFAULT_TIMEOUT = 2000

    const DEFAULT_ERROR_COLOR = 'error'
    const DEFAULT_SUCCESS_COLOR = 'success'
    const DEFAULT_WARN_COLOR = 'warning'
    const DEFAULT_INFO_COLOR = 'primary'

    const color = ref('')
    const active = ref(false)
    const text = ref('')
    const has_close_button = ref(false)
    const timeout = ref(10000)

    function processDefaultsParams(input) {
        const default_params = {
            timeout: DEFAULT_TIMEOUT,
            has_close_button: false,
            color: DEFAULT_INFO_COLOR,
            text: '',
        }
        
        if (input.constructor == Object) {
            return {
                ...default_params,
                ...input,
            }
        }
        return {
            ...default_params,
            text: input,
        }
    }

    function trigger(data) {
        text.value = data.text
        timeout.value = data.timeout
        has_close_button.value = data.has_close_button
        color.value = data.color
        active.value = true
    }

    function triggerSuccess(data) {
        trigger({
            ...processDefaultsParams(data),
            color: DEFAULT_SUCCESS_COLOR,
        })
    }

    function triggerError(data) {
        trigger({
            ...processDefaultsParams(data),
            color: DEFAULT_ERROR_COLOR,
        })
    }


    function triggerInfo(data) {
        trigger({
            ...processDefaultsParams(data),
            color: DEFAULT_INFO_COLOR,
        })
    }


    function triggerWarning(data) {
        trigger({
            ...processDefaultsParams(data),
            color: DEFAULT_WARN_COLOR,
        })
    }


    return {
        color,
        active,
        text,
        has_close_button,
        timeout,
        triggerSuccess,
        triggerError,
        triggerInfo,
        triggerWarning,
    }

})
