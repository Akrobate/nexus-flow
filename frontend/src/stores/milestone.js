import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import {
    ticket_repository,
} from '@/repositories'

export const useMilestone = defineStore('milestone', () => {

    const space_milestone_list = ref({})

    async function load(space_id) {
        if (!space_milestone_list.value[space_id]) {
            const response = await ticket_repository.getMilestones({
                space_id: space_id,
            })
            space_milestone_list.value[space_id] = response
        }
    }


    async function getMilestones(space_id) {
        await load(space_id)
        return space_milestone_list.value[space_id]
    }

    const getMilestoneById = computed(() =>
        (space_id, milestone_id) => space_milestone_list.value[space_id]
            ?
            space_milestone_list.value[space_id].find(
                (item) => item.id === milestone_id
            )
            :
            {}
    )

    return {
        getMilestones,
        getMilestoneById,
        load,
    }
})

