import { defineStore } from 'pinia'
import {
    ticket_repository,
} from '@/repositories'

export const useDashboard = defineStore('dashboard', () => {

    function searchTickets(input) {
        return ticket_repository.search(input)
    }

    return {
        searchTickets,
    }
})

