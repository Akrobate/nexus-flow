import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import {
    ticket_repository,
} from '@/repositories'

export const useTag = defineStore('tag', () => {

    const space_tag_list = ref({})


    async function load(space_id) {
        if (!space_tag_list.value[space_id]) {
            const response = await ticket_repository.getTags({
                space_id,
            })
            space_tag_list.value[space_id] = response
        }
    }

    async function getTags(space_id) {
        await load(space_id)
        return space_tag_list.value[space_id]
    }

    const tagsInSpaceId = computed(() => (space_id) => space_tag_list.value[space_id])

    const tagsInSpaceIdByIdList = computed(() => 
        (space_id, id_list) => {
            return (space_tag_list.value[space_id] ? space_tag_list.value[space_id] : [])
                .filter((item) => id_list.includes(item.id))
        }
    )

    return {
        load,
        getTags,
        tagsInSpaceId,
        tagsInSpaceIdByIdList,
    }
})
