'use strict'

import { defineStore } from 'pinia'
import {
    user_repository,
} from '@/repositories'

export const useUser = defineStore('user', () => {


    function createSpaceUserAccess(input) {
        return user_repository.createSpaceUserAccess(input)
    }

    function updateSpaceUserAccess(id, input) {
        return user_repository.updateSpaceUserAccess(id, input)
    }

    function deleteSpaceUserAccess(id) {
        return user_repository.deleteSpaceUserAccess(id)
    }

    return {
        createSpaceUserAccess,
        updateSpaceUserAccess,
        deleteSpaceUserAccess,
    }

})
