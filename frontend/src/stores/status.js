import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import {
    ticket_repository,
} from '@/repositories'

export const useStatus = defineStore('status', () => {

    const space_status_list = ref({})


    async function load(space_id) {
        if (!space_status_list.value[space_id]) {
            const response = await ticket_repository.getStatuses({
                space_id,
            })
            space_status_list.value[space_id] = response
        }
    }

    async function getStatuses(space_id) {
        await load(space_id)
        return space_status_list.value[space_id]
    }

    const get_status_by_space_id = computed(() => (space_id) => space_status_list.value[space_id])
    const get_status_by_space_id_status_id = computed(() => 
        (space_id, status_id) => space_status_list.value[space_id]
            .find((item) => item.id === status_id)
    )

    return {
        load,
        getStatuses,
        get_status_by_space_id,
        get_status_by_space_id_status_id,
    }
})
