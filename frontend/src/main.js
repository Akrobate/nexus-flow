import { registerPlugins } from '@/plugins'
import App from './App.vue'
import { createApp } from 'vue'
import axios from 'axios'
import { getInterceptorFunctions } from '@/repositories/ApiInterceptors'

const app = createApp(App)

registerPlugins(app)

app.mount('#app')

const {
    responseUnauthorizedRedirect,
    responseValidVoidInterceptor,
    requestAuthenticate,
} = getInterceptorFunctions();

axios.interceptors.response.use(responseValidVoidInterceptor, responseUnauthorizedRedirect)
axios.interceptors.request.use(requestAuthenticate)
  