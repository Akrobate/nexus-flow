import router from '@/router'
import http_status from 'http-status'

import { storeToRefs } from 'pinia'
import { useAuthentication } from '@/stores/authentication'

function getInterceptorFunctions() {
  const authentication_store = useAuthentication()
  
  const {
      token,
  } = storeToRefs(authentication_store)
  
  const responseValidVoidInterceptor = (response) => response
  
  const responseUnauthorizedRedirect = (error) => {
    if (error.response.status === http_status.UNAUTHORIZED) {
      router.push({ name: 'login' })
    }
    return error
  }
  
  const requestAuthenticate = async (config) => {
    if (token.value) {
      config.headers = {
        Authorization: `Bearer ${token.value}`
      }
    }
    return config
  };
  
  return {
    responseValidVoidInterceptor,
    responseUnauthorizedRedirect,
    requestAuthenticate,
  }
}

export {
  getInterceptorFunctions
}