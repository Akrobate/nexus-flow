import axios from 'axios'
import api_configuration from '@/configurations/api'

class TicketRepository {

    async search(input) {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/tickets`,
            {
                params: input,
            }
        )
        return response.data.ticket_list
    }


    async update(id, input) {
        const response = await axios.patch(
            `${api_configuration.url_api}/api/v1/tickets/${id}`,
            input
        )
        return response.data.ticket_list
    }

    async getStatuses(input) {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/ticket-statuses`,
            {
                params: input,
            }
        )
        return response.data.ticket_status_list
    }


    async getTags(input) {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/ticket-tags`,
            {
                params: input,
            }
        )
        return response.data.ticket_tag_list
    }

    async getMilestones(input) {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/milestones`,
            {
                params: input,
            }
        )
        return response.data.milestone_list
    }
}

const ticket_repository = new TicketRepository()

export {
    ticket_repository,
}
