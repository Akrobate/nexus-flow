'use strict'

import {
    user_repository,
} from './UserRepository'
import {
    ticket_repository,
} from './TicketRepository'
import {
    space_repository,
} from './SpaceRepository'
import {
    merge_request_repository,
} from './MergeRequestRepository'

export {
    user_repository,
    ticket_repository,
    space_repository,
    merge_request_repository,
}