import axios from 'axios'
import api_configuration from '@/configurations/api'
import { jwtDecode } from 'jwt-decode'

class UserRepository {

    async login(login, password) {
        const response = await axios.post(`${api_configuration.url_api}/api/v1/login`,
            {
                login,
                password,
            }
        )
        return response.data
    }


    async register({
        email,
        login,
        password,
        first_name,
        last_name
    }) {
        const response = await axios
            .post(`${api_configuration.url_api}/api/v1/users/register`,
                {
                    email,
                    password,
                    login,
                    first_name,
                    last_name
                }
            )
        return response.data
    }

    async getUser(user_id) {
        const response = await axios.get(`${api_configuration.url_api}/api/v1/users/${user_id}`)
        return response.data
    }


    async getSpaceTeammates(space_id) {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/teammates`,
            {
                params: {
                    space_id,
                },
            }
        )
        return response.data.teammate_list
    }

    async getSpaceAccessProfiles() {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/space-access-profiles`,
        )
        return response.data.space_access_profile_list
    }


    async createSpaceUserAccess(input) {
        const response = await axios.post(
            `${api_configuration.url_api}/api/v1/space-user-accesses`,
            input
        )
        return response.data
    }

    async updateSpaceUserAccess(id, input) {
        const response = await axios.patch(
            `${api_configuration.url_api}/api/v1/space-user-accesses/${id}`,
            input
        )
        return response.data
    }

    async deleteSpaceUserAccess(id) {
        const response = await axios.delete(
            `${api_configuration.url_api}/api/v1/space-user-accesses/${id}`
        )
        return response.data
    }

    getTokenLocalStorage() {
        return localStorage.token ? localStorage.token : null
    }

    setTokenLocalStorage(token) {
        localStorage.token = token
    }

    removeTokenLocalStorage() {
        localStorage.removeItem('token')
    }

    isValidLocalStorageToken() {
        const local_token = this.getTokenLocalStorage()
        return this.isValidToken(local_token)
    }

    getTokenFromLocalStorageIfIsValid() {
        const local_token = this.getTokenLocalStorage()
        if (this.isValidToken(local_token)) {
            return local_token
        }
        return null
    }

    isValidToken(token) {
        if (token == null || token == undefined || token === 'undefined')
            return false
        const now_timestamp = Math.floor(Date.now() / 1000)
        const decoded_token = this.decodeToken(token)
        return (decoded_token.exp > now_timestamp)
    }

    decodeToken(token) {
        return jwtDecode(token)
    }

}

const user_repository = new UserRepository()

export {
  user_repository,
}
