import axios from 'axios'
import api_configuration from '@/configurations/api'

class SpaceRepository {

    async search(input) {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/spaces`,
            {
                params: input,
            }
        )
        return response.data.space_list
    }
}

const space_repository = new SpaceRepository()

export {
    space_repository,
}
