import axios from 'axios'
import api_configuration from '@/configurations/api'

class MergeRequestRepository {

    async searchMergeRequestStatics(input) {
        const response = await axios.get(
            `${api_configuration.url_api}/api/v1/merge-request-statistics`,
            {
                params: input,
            }
        )
        return response.data.merge_request_statistic_list
    }
}

const merge_request_repository = new MergeRequestRepository()

export {
    merge_request_repository,
}
