import { createRouter, createWebHistory } from 'vue-router'

import AppLayout from '@/layouts/AppLayout.vue'
import PublicLayout from '@/layouts/PublicLayout.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('@/pages/LoginPage.vue'),
      meta: {
        layout: PublicLayout,
      }
    },
    {
      path: '/',
      name: 'home',
      component: () => import('@/pages/DashboardPage.vue'),
      meta: {
        layout: AppLayout,
      }
    },
    {
      path: '/cardwall',
      name: 'cardwall',
      component: () => import('@/pages/CardwallPage.vue'),
      meta: {
        layout: AppLayout,
      }
    },
    {
      path: '/milestones',
      name: 'milestones',
      component: () => import('@/pages/MilestonePage.vue'),
      meta: {
        layout: AppLayout,
      }
    },
    {
      path: '/admin/spaces/access',
      name: 'admin-space-access',
      component: () => import('@/pages/AdminSpaceAccessPage.vue'),
      meta: {
        layout: AppLayout,
      }
    },
    {
      path: '/merge-requests',
      name: 'merge-requests-home',
      component: () => import('@/pages/MergeRequestHome.vue'),
      meta: {
        layout: AppLayout,
      }
    },
  ],
})

export default router
